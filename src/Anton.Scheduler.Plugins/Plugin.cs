﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Scheduler;
using Ridder.Scheduler.Jobs;

namespace Anton.Scheduler.Plugins
{
    public class Plugin : IPlugin
    {
        public void Configure()
        {
           
        }
    }

    public class JobConfig : IConfigureJobs
    {
        public void Configure(IJobConfiguration jobManager)
        {
            jobManager.CreateIfNotExist(new ScheduledJob()
            {
                Key = "ImportCreditSafeMonitoringEvents",
                Description = "Importeren van CreditSafe krediet limiet informatie voor Ridder iQ relaties",
                Type = typeof(Jobs.ImportCreditSafeMonitoringEvents),
                CronExpression = "0 0/1 * 1/1 * ? *",
                Interval = 0,
                RunOnStart = false,
            });

            jobManager.CreateIfNotExist(new ScheduledJob()
            {
                Key = "CopySafetyReportsToIntercompany",
                Description = "Kopieer veiligheidsmelding naar administratie van intercompany waar veiligheidsmelding is uitgevoerd",
                Type = typeof(Jobs.CopySafetyReportsToIntercompany),
                CronExpression = "0 0 0/1 1/1 * ? *",
                Interval = 0,
                RunOnStart = false,
            });

            jobManager.CreateIfNotExist(new ScheduledJob()
            {
                Key = "ImportEmployeesFromAfas",
                Description = "Importeer werknemers vanuit AFAS",
                Type = typeof(Jobs.ImportEmployeesFromAfas),
                CronExpression = "0 0 22 1/1 * ? *",
                Interval = 0,
                RunOnStart = false,
            });

            jobManager.CreateIfNotExist(new ScheduledJob()
            {
                Key = "ExportLeaveHoursToAfas",
                Description = "Exporteer verlofuren naar AFAS",
                Type = typeof(Jobs.ExportLeaveHoursToAfas),
                CronExpression = "0 0 23 1/1 * ? *",
                Interval = 0,
                RunOnStart = false,
            });

            jobManager.CreateIfNotExist(new ScheduledJob()
            {
                Key = "ImportShippingDetailsAsWipFromBfw",
                Description = "Importeer BFW vrachtbriefregels als OHW",
                Type = typeof(Jobs.ImportBfwShippingDetailsAsWip),
                CronExpression = "0 0 0 ? * * 2099",
                Interval = 0,
                RunOnStart = false,
            });

            jobManager.CreateIfNotExist(new ScheduledJob()
            {
                Key = "ImportIntranetNews",
                Description = "Importeer Anton Intranet nieuws berichten als Bisharp RSS feed berichten.",
                Type = typeof(Jobs.ImportIntranetNews),
                CronExpression = "0 0 0 ? * * 2099",
                Interval = 0,
                RunOnStart = false,
            });

            jobManager.CreateIfNotExist(new ScheduledJob()
            {
                Key = "ImportBfwBuigstaten",
                Description = "Importeer BFW buigstaten",
                Type = typeof(Jobs.ImportBfwBuigstaten),
                CronExpression = "0 0 0 ? * * 2099",
                Interval = 0,
                RunOnStart = false,
            });
        }
    }
}
