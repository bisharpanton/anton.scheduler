﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    class AfasHrAbsence
    {
        [JsonProperty("EmId")]
        public string EmployeeId { get; set; }
        [JsonProperty("ViAt")]
        public string TypeAbsence { get; set; }
        [JsonProperty("DaBe")]
        public DateTime Start { get; set; }
        [JsonProperty("DaEn")]
        public DateTime End { get; set; }
        [JsonProperty("ViLr")]
        public string ReasonAbsence { get; set; }
        [JsonProperty("EnSe")]
        public int EmploymentType { get; set; }
        [JsonProperty("DuRa")]
        public int TimeInMinutes { get; set; }
        [JsonProperty("Re")]
        public string Memo { get; set; }
        [JsonProperty("LeDt")]
        public bool SpecificTimes { get; set; }
        [JsonProperty("DuBe")]
        public int? LeaveHoursStartTime { get; set; }
    }
}
