﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    class AfasLeaveCorrection
    {
        public string Medewerker { get; set; }
        public string Achternaam__init_voorvoegsel__voornaam_ { get; set; }
        public int Boekjaar { get; set; }
        public int Verlofperiode { get; set; }
        public string Type_verlof { get; set; }
        public int Dienstverband { get; set; }
        public string Aantal_uren { get; set; }
        public string Omschrijving { get; set; }
        public DateTime Datum_boeking { get; set; }
        public string OrganisationUnitCode { get; set; }
        public string CodeTypeVerlof { get; set; }

        [JsonIgnore]
        public string UniqueKey => $"{Medewerker}_{Datum_boeking.Date:yyyyMMdd}";
    }
}
