﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.AfasModels
{
    public class AfasTimeTable
    {
        public string Medewerker { get; set; }
        public string Naam { get; set; }
        public DateTime Begindatum_rooster { get; set; }
        public DateTime? Einddatum_rooster { get; set; }
        public double? Aantal_dagen_per_week { get; set; }
        public double? Aantal_uren_per_week { get; set; }
        public double? Aantal_uren_per_dag { get; set; }
        public int Volgnummer_rooster { get; set; }
        public string Type_rooster { get; set; }
        public double? Uren_Maandag { get; set; }
        public double? Uren_Dinsdag { get; set; }
        public double? Uren_Woensdag { get; set; }
        public double? Uren_Donderdag { get; set; }
        public double? Uren_Vrijdag { get; set; }
        public double? Uren_Zaterdag { get; set; }
        public double? Uren_Zondag { get; set; }
        public string OrganisationUnitCode { get; set; }
    }
}
