﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    public class RootobjectHrAbsenceCorrection
    {
        public Hrabscorrection HrAbsCorrection { get; set; }
    }

    public class Hrabscorrection
    {
        public Element Element { get; set; }
    }

    public class Element
    {
        public HrAbsCorrection Fields { get; set; }
    }

    /*
    public class Fields
    {
        public string EmId { get; set; }
        public int EnSe { get; set; }
        public string RgDa { get; set; }
        public string ViAt { get; set; }
        public int HhMm { get; set; }
    }*/
}
