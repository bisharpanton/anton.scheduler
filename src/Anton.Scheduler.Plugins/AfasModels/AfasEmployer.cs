﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.AfasModels
{
    public class AfasEmployer
    {
        public string EmployerId { get; set; }
        public string Name { get; set; }
        public string OrganisationId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string DimAx1 { get; set; }
        public string DimAx2 { get; set; }
        public string DimAx3 { get; set; }
        public string DimAx4 { get; set; }
        public string DimAx5 { get; set; }
        public int UnitId { get; set; }
    }
}
