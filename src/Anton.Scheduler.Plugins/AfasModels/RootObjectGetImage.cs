﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    class RootObjectGetImage
    {
        [JsonProperty("filedata")]
        public string FileData { get; set; }

        [JsonProperty("mimetype")]
        public string MimeType { get; set; }
    }
}
