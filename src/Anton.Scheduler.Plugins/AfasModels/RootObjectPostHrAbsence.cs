﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    class RootObjectPostHrAbsence
    {
        public Hrabsence HrAbsence { get; set; }

        public class Hrabsence
        {
            public Element Element { get; set; }
        }

        public class Element
        {
            [JsonProperty("Fields")]
            public AfasHrAbsence AfasHrAbsence { get; set; }
        }
    }
}
