﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    public class HrAbsCorrection
    {
        [JsonProperty("EmId")]
        public string EmployeeId { get; set; }
        [JsonProperty("EnSe")]
        public int EmploymentType { get; set; }
        [JsonProperty("RgDa")]
        public DateTime Date { get; set; }
        [JsonProperty("ViAt")]
        public string TypeAbsence { get; set; }
        [JsonProperty("HhMm")]
        public int TimeInMinutes { get; set; }
    }
}
