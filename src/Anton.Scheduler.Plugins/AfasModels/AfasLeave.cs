﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    class AfasLeave
    {
        public int LeaveId { get; set; }
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public string BSN { get; set; }
        public bool Export { get; set; }
        public int Hours { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [JsonProperty("DvId")]
        public int EmploymentType { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string LeaveCode { get; set; }
        public string LeaveDescr { get; set; }
        public string ReasonCode { get; set; }
        public string ReasonDescr { get; set; }
        public string OrganisationUnitCode { get; set; }
        public int? Samengestelde_verlofboeking { get; set; }

        [JsonIgnore]
        public string UniqueKey => $"{EmployeeId}_{StartDate.Date:yyyyMMdd}_{LeaveCode}_{Math.Abs(TimeSpan.FromMinutes(Hours).TotalMinutes)}";
    }
}
