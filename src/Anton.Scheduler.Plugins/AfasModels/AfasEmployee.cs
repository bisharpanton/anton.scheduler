﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.Models;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.AfasModels
{
    public class AfasEmployee
    {
        public string EmployeeId { get; set; }
        public string PersonId { get; set; }
        public string EmployerId { get; set; }
        public string BSN { get; set; }
        public string BirthName { get; set; }
        public string Initials { get; set; }
        public string PrefixBirthName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string PrefixPartner { get; set; }
        public string BirthNamePartner { get; set; }
        public string NameUse { get; set; }
        public string MobilePrivate { get; set; }
        public string PhonePrivate { get; set; }
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string AddNumber { get; set; }
        public string ZIPCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime? EmploymentStart { get; set; }
        public DateTime? EmploymentEnd { get; set; }
        public double HourPerWeek { get; set; }
        public string EmploymentType { get; set; }
        public string EmploymentTypeDesc { get; set; }
        public double FTE { get; set; }
        public string OrgUnit { get; set; }
        public string OrgUnitDesc { get; set; }
        public string FunctionId { get; set; }
        public string FunctionDesc { get; set; }
        public DateTime? DateDeceased { get; set; }
        public string Mail { get; set; }
        public string FirstName { get; set; }
        public string EmailPrivate { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public string CaoCode { get; set; }
        public string CaoDescription { get; set; }
        public string OrganisationUnitCode { get; set; }
        public string OrganisationUnitDescription { get; set; }
        public DateTime? ImageDateChanged { get; set; }
        public int? ImageId { get; set; }
        public int? PeriodTable { get; set; }
        public string TypeTimeTable { get; set; }
        public int EmploymentTypeId { get; set; }
        public DateTime? ContractEndDate { get; set; }

        [JsonIgnore]
        public string NamePrefix => DetermineNamePrefix();

        public string LastName => DetermineLastName();
        public string Recordlabel => $"{FirstName} {(NamePrefix + " " + LastName).Trim()}";

        private string DetermineNamePrefix()
        {
            if (!Gender.Equals("V"))
            {
                //Bij mannen gewoon geboortenaam
                return PrefixBirthName;
            }
            else if (NameUse.Equals("0") || NameUse.Equals("3"))
            {
                //0 = Geboortenaam, 3 = Geboortenaam en daarna naam partner
                return PrefixBirthName;
            }
            else if (NameUse.Equals("1") || NameUse.Equals("2"))
            {
                return PrefixPartner;
            }
            else
            {
                //Hier komt ie niet als het goed is
                return PrefixBirthName;
            }
        }

        private string DetermineLastName()
        {
            if (!Gender.Equals("V"))
            {
                //Bij mannen gewoon geboortenaam
                return BirthName;
            }
            else if (NameUse.Equals("0"))
            {
                //0 = Geboortenaam
                return BirthName;
            }
            else if (NameUse.Equals("1"))
            {
                //1 = Geboortenaam partner + geboortenaam
                var totalLastName = $"{PrefixBirthName} {BirthName}".Trim();
                return $"{BirthNamePartner} - {totalLastName}";
            }
            else if (NameUse.Equals("2"))
            {
                //2 = Geboortenaam partner
                return BirthNamePartner;
            }
            else if (NameUse.Equals("3"))
            {
                //Geboortenaam + geboortenaam partner
                var totalLastName = $"{PrefixPartner} {BirthNamePartner}".Trim();
                return $"{BirthName} - {totalLastName}";
            }
            else
            {
                //Hier komt ie niet als het goed is
                return BirthName;
            }
        }

        public bool AfasEmployeeEqualsRidderEmployee(Employee employeeRidder)
        {
            //Pak voor een vrouw de partner namen
            var namesEquals = (FirstName ?? string.Empty).Trim().Equals(employeeRidder.Person.FIRSTNAME.Trim())
                                && (NamePrefix ?? string.Empty).Trim().Equals(employeeRidder.Person.NAMEPREFIX.Trim())
                                && (LastName ?? string.Empty).Trim().Equals(employeeRidder.Person.LASTNAME.Trim());

            var generalFieldsEquals = (BSN ?? string.Empty).Trim().Equals(employeeRidder.EmployeeAvgData?.NATIONALIDNUMBER.Trim())
                                      && (Initials ?? string.Empty).Trim().Equals(employeeRidder.Person.INITIALS.Trim())
                                      && DateOfBirth.ToString().Trim().Equals(employeeRidder.EmployeePrivateData?.DATEOFBIRTH.ToString().Trim())
                                      && (MobilePrivate ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.CELLPHONE.Trim())
                                      && (PhonePrivate ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.PHONE1.Trim())
                                      && (Street ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.Address?.STREET.Trim())
                                      && HouseNumber.ToString().Trim().Equals(employeeRidder.EmployeePrivateData?.Address?.HOUSENUMBER2.Trim())
                                      && (AddNumber ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.Address?.ADDITIONHOUSENUMBER.Trim())
                                      && (ZIPCode ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.Address?.ZIPCODE.Trim())
                                      && (City ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.Address?.CITY.Trim())
                                      && (Country ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.Address?.Country?.CODE.Trim())
                                      && EmploymentStart.HasValue && employeeRidder.EMPLOYMENTSTARTDATE.HasValue //Datum in dienst alleen overnemen als deze nog leeg is in Ridder
                                      && ((!EmploymentEnd.HasValue && !employeeRidder.EMPLOYMENTTERMINATIONDATE.HasValue)
                                            || (EmploymentEnd.HasValue && employeeRidder.EMPLOYMENTTERMINATIONDATE.HasValue && EmploymentEnd.Value.Date == employeeRidder.EMPLOYMENTTERMINATIONDATE.Value.Date.AddDays(-1))) //Datum uit dienst in Ridder zetten we één dag verder ivm uren op die laatste dag
                                      && Math.Abs(HourPerWeek - employeeRidder.UURWERKWEEK.Value) < 0.01
                                      && (FunctionId ?? string.Empty).Trim().Trim().Equals(employeeRidder.Function?.CODE.Trim(), StringComparison.OrdinalIgnoreCase)
                                      && (Mail ?? string.Empty).Trim().Equals(employeeRidder.EMAIL.Trim())
                                      && (EmailPrivate ?? string.Empty).Trim().Equals(employeeRidder.EmployeePrivateData?.EMAIL.Trim())
                                      && (Mobile ?? string.Empty).Trim().Equals(employeeRidder.MOBILE.Trim())
                                      && (Phone ?? string.Empty).Trim().Equals(employeeRidder.PHONE.Trim())
                                      && (CaoCode ?? string.Empty).Trim().Equals(employeeRidder.Cao?.AFASCAOCODE.Trim())
                                      && (ImageDateChanged ?? DateTime.MinValue).Equals(employeeRidder.AFASDATEPICTURECHANGED ?? DateTime.MinValue)
                                      && ((!ContractEndDate.HasValue && !employeeRidder.EINDECONTRACTDATUM.HasValue)
                                            || (ContractEndDate.HasValue && employeeRidder.EINDECONTRACTDATUM.HasValue && ContractEndDate.Value.Date == employeeRidder.EINDECONTRACTDATUM.Value.Date));



            var genderEquals = Gender.Equals("M") && employeeRidder.Person.GENDER == Models.Gender.Man
                               || Gender.Equals("V") && employeeRidder.Person.GENDER == Models.Gender.Vrouw
                               || !Gender.Equals("M") && !Gender.Equals("V") &&
                               employeeRidder.Person.GENDER == Models.Gender.Onbekend;

            var deceasedEquals = employeeRidder.EmployeePrivateData == null
                                 || (!DateDeceased.HasValue && !employeeRidder.EmployeePrivateData.DECEASED
                                     || DateDeceased.HasValue && employeeRidder.EmployeePrivateData.DECEASED);

            return namesEquals && generalFieldsEquals && genderEquals && deceasedEquals;
        }
    }
}
