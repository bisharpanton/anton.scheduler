﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.BfwModels;
using Anton.Scheduler.Plugins.FrameworkModels;
using Anton.Scheduler.Plugins.Models;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Scheduler.Jobs;

namespace Anton.Scheduler.Plugins.Jobs
{
    internal class ImportBfwShippingDetailsAsWip : RidderJob
    {
        public SdkSession _sdkSession { get; set; }
        private readonly RidderLogger _ridderLogger;
        private const string SchedulerTask = "ImportShippingDetailsAsWipFromBfw";

        public ImportBfwShippingDetailsAsWip(SdkSession sdkSession) : base(sdkSession)
        {
            _sdkSession = sdkSession;
            _ridderLogger = new RidderLogger(sdkSession);
        }

        protected override void ExecuteImpl(IJobExecutionContext context)
        {
            //DB
            //22-9-2022

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, "Start plugin importeren BFW vrachtbriefregels als OHW.");

            //TESTEN BFW koppeling
            //var shipmentsToImport2 = new List<int>() { 1, 2, 3 };
            //var shippingHeaderInfo2 = BfwHelper.GetShippingHeaderInfo(_ridderLogger, shipmentsToImport2, SchedulerTask);

            var sdkSessionSpanberg = GlobalFunctions.GetSdkSession(_ridderLogger, _sdkSession, SchedulerTask, "Spanberg");

            if (sdkSessionSpanberg == null)
            {
                return;
            }

            var dateLastImport = _sdkSession.Find<CrmSetting>().First().LASTIMPORTBFWWIPDETAILS ?? DateTime.MinValue;

            var shipmentsToImport = DetermineShipmentsToImport(sdkSessionSpanberg, dateLastImport);

            if (!shipmentsToImport.Any())
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, "Geen BFW vrachtbrieven gevonden om te importeren.");

                UpdateDateLastImport();
                return;
            }

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"{shipmentsToImport.Count} vrachtbrieven gevonden om te importeren.");

            var shippingHeaderInfo = BfwHelper.GetShippingHeaderInfo(_ridderLogger, shipmentsToImport, SchedulerTask);

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Vrachtbrief info correct opgehaald uit BFW.");

            var bfwWerkNrs = shippingHeaderInfo.Select(x => x.WerkNr).Distinct().ToList();
            var orders = sdkSessionSpanberg.Find<Order>($"ORDERNUMBER IN ({string.Join(",", bfwWerkNrs)}) OR OLDBFWORDERNUMBER IN ({string.Join(",", bfwWerkNrs)})").ToList();

            var bfwExternalItemCodes = shippingHeaderInfo.SelectMany(x => x.StockTransactions).Select(x => x.Item.ExternalId).Distinct().ToList();
            var items = sdkSessionSpanberg.Find<Models.Item>($"CODE IN ('{string.Join("','", bfwExternalItemCodes)}')").ToList();

            FilterShippingHeadersAndItemsNotFound(shippingHeaderInfo, orders, items);

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"{shippingHeaderInfo.SelectMany(x => x.StockTransactions).Count()} vrachtbriefregels gevonden om aan te maken in Ridder iQ.");

            CreateWipDetailsItemInRidder(sdkSessionSpanberg, shippingHeaderInfo, items);

            UpdateDateLastImport();

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, "Plugin importeren BFW vrachtbriefregels afgerond.");
        }

        private void UpdateDateLastImport()
        {
            var crmSetting = _sdkSession.Find<CrmSetting>().First();
            crmSetting.LASTIMPORTBFWWIPDETAILS = DateTime.Now;
            _sdkSession.Update(crmSetting);
        }

        private void FilterShippingHeadersAndItemsNotFound(List<ShippingHeader> shippingHeaderInfo, List<Order> orders, List<Models.Item> items)
        {
            var ridderItemCodes = items.Select(x => x.CODE).Distinct().ToList();

            var shippingHeadersToRemove = new List<ShippingHeader>();

            foreach (var shippingHeader in shippingHeaderInfo)
            {
                //Controleer eerst of ordernummer overeen komt, anders nog zoeken op OLDBFWORDERNUMBER
                var order = orders.Any(x => x.ORDERNUMBER == shippingHeader.WerkNr)
                    ? orders.First(x => x.ORDERNUMBER == shippingHeader.WerkNr)
                    : orders.FirstOrDefault(x => x.OLDBFWORDERNUMBER == shippingHeader.WerkNr);

                if (order == null)
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Importeren BFW vrachtbrief {shippingHeader.ShipmentId} met werknr {shippingHeader.WerkNr} geannuleerd. Order {shippingHeader.WerkNr} niet gevonden in Ridder iQ.");
                    shippingHeadersToRemove.Add(shippingHeader);
                    continue;
                }

                shippingHeader.FoundRidderOrderId = order.Id;

                if(order.FINANCIALDONE)
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Importeren BFW vrachtbrief {shippingHeader.ShipmentId} met werknr {shippingHeader.WerkNr} geannuleerd. Order {shippingHeader.WerkNr} is al financieel gereed.");
                    shippingHeadersToRemove.Add(shippingHeader);
                    continue;
                }

                if (shippingHeader.StockTransactions.Any(x => !ridderItemCodes.Contains(x.Item.ExternalId)))
                {
                    var missingItemCodes = shippingHeader.StockTransactions.Where(x => !ridderItemCodes.Contains(x.Item.ExternalId))
                        .Select(x => x.Item.ExternalId).Distinct().ToList();

                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Importeren BFW vrachtbrief {shippingHeader.ShipmentId} met werknr {shippingHeader.WerkNr} geannuleerd. Artikel(en) {string.Join(", ", missingItemCodes)} niet gevonden in Ridder iQ.");
                    shippingHeadersToRemove.Add(shippingHeader);
                    continue;
                }
            }

            shippingHeadersToRemove.ForEach(x => shippingHeaderInfo.Remove(x));
        }

        private void CreateWipDetailsItemInRidder(SdkSession sdkSessionSpanberg, List<ShippingHeader> shippingHeaderInfo, List<Models.Item> items)
        {
            var allStockTransactions = shippingHeaderInfo.SelectMany(x => x.StockTransactions).ToList();

            var batches = GlobalFunctions.SplitList<StockTransactions>(allStockTransactions, 100);

            for (int i = 0; i < batches.Count; i++)
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start aanmaken OHW regels ({i * 100} / {allStockTransactions.Count}).");

                var rsOrderWipDetailItem = sdkSessionSpanberg.GetRecordsetColumns("R_ORDERWIPDETAILITEM", "", "PK_R_ORDERWIPDETAILITEM IS NULL", "");
                rsOrderWipDetailItem.UseDataChanges = true;

                foreach (var stockTransaction in batches[i])
                {
                    var shippingHeader = shippingHeaderInfo.First(x => x.ShipmentId == stockTransaction.GlobalId);

                    rsOrderWipDetailItem.AddNew();
                    rsOrderWipDetailItem.SetFieldValue("FK_ORDER", shippingHeader.FoundRidderOrderId);
                    rsOrderWipDetailItem.SetFieldValue("FK_ITEM", items.First(x => x.CODE.Equals(stockTransaction.Item.ExternalId)).Id);
                    rsOrderWipDetailItem.SetFieldValue("QUANTITY", Convert.ToDouble(stockTransaction.Amount) / 1000.0);
                    rsOrderWipDetailItem.SetFieldValue("DATEINWIP", shippingHeader.Leverdatum);

                    rsOrderWipDetailItem.SetFieldValue("BFWSHIPMENTID", shippingHeader.ShipmentId);
                }

                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Opslaan OHW regels...");
                var updateResult = rsOrderWipDetailItem.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Aanmaken OHW regels is mislukt, oorzaak {updateResult.First(x => x.HasError).GetResult()}");
                }
            }
            
            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"OHW regels correct aangemaakt.");
        }

        private List<int> DetermineShipmentsToImport(SdkSession sdkSessionSpanberg, DateTime dateLastImport)
        {
            var alreadyImportedShipmentIds = sdkSessionSpanberg.GetRecordsetColumns("R_ORDERWIPDETAILITEM", "BFWSHIPMENTID",
                $"BFWSHIPMENTID <> '' AND DATECREATED >= '{DateTime.Now.AddYears(-2):yyyyMMdd}'").DataTable.AsEnumerable()
                .Select(x => int.Parse(x.Field<string>("BFWSHIPMENTID"))).Distinct().ToList();
            
            var bfwShipmentIds = BfwHelper.GetShipmentIds(_ridderLogger, dateLastImport, SchedulerTask);

            if(!bfwShipmentIds.Any())
            {
                return new List<int>();
            }

            return bfwShipmentIds.Except(alreadyImportedShipmentIds).ToList();
        }


    }
}
