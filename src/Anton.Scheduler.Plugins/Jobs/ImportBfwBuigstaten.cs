﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.BfwModels;
using Anton.Scheduler.Plugins.FrameworkModels;
using Anton.Scheduler.Plugins.Models;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Client.SDK.SDKEventsAndActions.SDKItems;
using Ridder.Scheduler.Jobs;

namespace Anton.Scheduler.Plugins.Jobs
{
    internal class ImportBfwBuigstaten : RidderJob
    {
        public SdkSession _sdkSession { get; set; }
        private readonly RidderLogger _ridderLogger;
        private const string SchedulerTask = "ImportBfwBuigstaten";

        public ImportBfwBuigstaten(SdkSession sdkSession) : base(sdkSession)
        {
            _sdkSession = sdkSession;
            _ridderLogger = new RidderLogger(sdkSession);
        }

        protected override void ExecuteImpl(IJobExecutionContext context)
        {
            //DB
            //12-12-2024

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, "Start plugin importeren BFW buigstaten.");

            var sdkSessionSpanberg = GlobalFunctions.GetSdkSession(_ridderLogger, _sdkSession, SchedulerTask, "Spanberg");

            if (sdkSessionSpanberg == null)
            {
                return;
            }

            var orders = GetActualOrders(sdkSessionSpanberg);

            if(!orders.Any())
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, "Geen actuele orders gevonden.");
                return;
            }

            var ridderFasen = GetRidderFasen(sdkSessionSpanberg, orders);
            var bfwFasen = BfwHelper.GetFasen(_ridderLogger, SchedulerTask, orders);

            List<Fase> ridderFasenToDelete = new List<Fase>();
            UpdateRidderFasenInfo(sdkSessionSpanberg, orders, ridderFasen, bfwFasen, ref ridderFasenToDelete);

            //Haal de ridder fasen opnieuw op omdat deze nieuw zijn aangemaakt
            ridderFasen = GetRidderFasen(sdkSessionSpanberg, orders);

            var ridderBuigstaten = GetRidderBuigstaten(sdkSessionSpanberg, orders, ridderFasen);
            var bfwBuigstaten = BfwHelper.GetBuigstaten(_ridderLogger, SchedulerTask, orders);

            UpdateRidderBuigstatenInfo(sdkSessionSpanberg, orders, ridderFasen, ridderBuigstaten, bfwBuigstaten);
            
            var ridderBuigstaatRegels = GetRidderBuigstaatregels(sdkSessionSpanberg, ridderBuigstaten);
            var bfwBuigstaatregels = BfwHelper.GetBuigstaatregels(_ridderLogger, SchedulerTask, orders, bfwBuigstaten);

            var items = GetRidderItems(sdkSessionSpanberg, bfwBuigstaatregels);
            ridderBuigstaatRegels.ForEach(x => x.RidderItem = items.FirstOrDefault(y => y.Id == x.RidderItemId));
            bfwBuigstaatregels.Where(x => x.BfwItem != null).ToList().ForEach(x => x.RidderItem = items.FirstOrDefault(y => y.CODE == (x.BfwItem?.ExternalId ?? string.Empty)));

            UpdateRidderBuigstaatregelInfo(sdkSessionSpanberg, ridderBuigstaatRegels, bfwBuigstaatregels, orders, items);


            if(ridderFasenToDelete.Any())
            {
                DeleteFasen(sdkSessionSpanberg, ridderFasenToDelete);
            }

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, "Plugin importeren BFW buigstaten afgerond.");
        }

        private void UpdateRidderFasenInfo(SdkSession sdkSessionSpanberg, List<Order> orders, List<Fase> ridderFasen, List<Fase> bfwFasen, ref List<Fase> ridderFasenToDelete)
        {
            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start vergelijk fasen.");

            var ridderFasenUniqueKeys = ridderFasen.Select(x => x.UniqueKey).ToList();
            var bfwFasenUniqueKeys = bfwFasen.Select(x => x.UniqueKey).ToList();

            var fasenToInsert = bfwFasenUniqueKeys.Except(ridderFasenUniqueKeys).ToList();
            var fasenToDelete = ridderFasenUniqueKeys.Except(bfwFasenUniqueKeys).ToList();
            var fasenToUpdate = ridderFasen
                .Where(x => bfwFasenUniqueKeys.Contains(x.UniqueKey)
                                            && bfwFasen.Any(y => y.UniqueKey.Equals(x.UniqueKey) && !y.Description.Trim().Trim().Equals(x.Description))).ToList();

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Vergelijk fasen: {fasenToInsert.Count} aanmaken, {fasenToDelete.Count} te verwijderen, {fasenToUpdate.Count} bij te werken.");

            if (fasenToInsert.Count == 0 && fasenToDelete.Count == 0 && fasenToUpdate.Count == 0)
            {
                return;
            }

            var result1 = InsertFasen(sdkSessionSpanberg, bfwFasen.Where(x => fasenToInsert.Contains(x.UniqueKey)).ToList(), orders);
            //Verwijderen fasen doen we na bijwerken buigstaten en buigstaatregels
            

            var bfwFasenToUpdate = bfwFasen.Where(x => fasenToUpdate.Select(y => y.UniqueKey).Contains(x.UniqueKey)).ToList();
            var result3 = UpdateRidderFasen(sdkSessionSpanberg, fasenToUpdate, bfwFasenToUpdate, orders);

            if (!result1 || !result3)
            {
                return;
            }

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Fasen in Ridder bijgewerkt.");
        }


        private void UpdateRidderBuigstatenInfo(SdkSession sdkSessionSpanberg, List<Order> orders, List<Fase> ridderFasen, List<Buigstaat> ridderBuigstaten, List<Buigstaat> bfwBuigstaten)
        {
            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start vergelijk buigstaten.");

            var ridderBuigstatenUniqueKeys = ridderBuigstaten.Select(x => x.UniqueKey).ToList();
            var bfwBuigstatenUniqueKeys = bfwBuigstaten.Select(x => x.UniqueKey).ToList();

            var buigstatenToInsert = bfwBuigstatenUniqueKeys.Except(ridderBuigstatenUniqueKeys).ToList();
            var buigstatenToDelete = ridderBuigstatenUniqueKeys.Except(bfwBuigstatenUniqueKeys).ToList();
            var buigstatenToUpdate = ridderBuigstaten
                .Where(x => bfwBuigstatenUniqueKeys.Contains(x.UniqueKey)
                                            && bfwBuigstaten.Any(y => y.UniqueKey.Equals(x.UniqueKey)
                                                                            && (
                                                                                    !y.BfwFase.Trim().Equals(x.RidderFase.Code)
                                                                                    || !y.DeliveryDate.Equals(x.DeliveryDate)
                                                                                    || !y.BfwShipmentId.Equals(x.BfwShipmentId)
                                                                                    || !y.Element.Trim().Trim().Equals(x.Element)
                                                                                    || !y.Description.Trim().Trim().Equals(x.Description)
                                                                                    || !y.DrawingNumber.Trim().Equals(x.DrawingNumber)
                                                                                    || !y.BfwInvoiceId.Equals(x.BfwInvoiceId)
                                                                                    || !y.BfwInvoiceComplete.Equals(x.BfwInvoiceComplete)
                                                                                    || !y.BfwInvoiceDate.Equals(x.BfwInvoiceDate)))
                                            ).ToList();

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Vergelijk buigstaten: {buigstatenToInsert.Count} aanmaken, {buigstatenToDelete.Count} te verwijderen, {buigstatenToUpdate.Count} bij te werken.");

            if (buigstatenToInsert.Count == 0 && buigstatenToDelete.Count == 0 && buigstatenToUpdate.Count == 0)
            {
                return;
            }

            var result1 = InsertBuigstaten(sdkSessionSpanberg, bfwBuigstaten.Where(x => buigstatenToInsert.Contains(x.UniqueKey)).ToList(), orders, ridderFasen);
            var result2 = DeleteBuigstaten(sdkSessionSpanberg, ridderBuigstaten.Where(x => buigstatenToDelete.Contains(x.UniqueKey)).ToList());

            var bfwBuigstatenToUpdate = bfwBuigstaten.Where(x => buigstatenToUpdate.Select(y => y.UniqueKey).Contains(x.UniqueKey)).ToList();
            var result3 = UpdateRidderBuigstaten(sdkSessionSpanberg, buigstatenToUpdate, bfwBuigstatenToUpdate, orders, ridderFasen);

            if(!result1 || !result2 || !result3)
            {
                return;
            }

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Buigstaten in Ridder bijgewerkt.");
        }

        private void UpdateRidderBuigstaatregelInfo(SdkSession sdkSessionSpanberg, List<Buigstaatregel> ridderBuigstaatregels, List<Buigstaatregel> bfwBuigstaatregels, List<Order> orders, List<Item> items)
        {
            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start vergelijk buigstaatregels.");

            var ridderBuigstaatregelsUniqueKeys = ridderBuigstaatregels.Select(x => x.UniqueKey).ToList();
            var bfwBuigstaatregelsUniqueKeys = bfwBuigstaatregels.Select(x => x.UniqueKey).ToList();

            var ridderItemCodes = items.Select(x => x.CODE).ToList();

            var buigstaatregelsToInsert = bfwBuigstaatregelsUniqueKeys.Except(ridderBuigstaatregelsUniqueKeys).ToList();
            var buigstaatregelsToDelete = ridderBuigstaatregelsUniqueKeys.Except(bfwBuigstaatregelsUniqueKeys).ToList();
            var buigstaatregelsToUpdate = ridderBuigstaatregels
                .Where(x => bfwBuigstaatregelsUniqueKeys.Contains(x.UniqueKey)
                                            && bfwBuigstaatregels.Any(y => y.UniqueKey.Equals(x.UniqueKey)
                                                                            && (
                                                                                    (ridderItemCodes.Contains(y.BfwItem?.ExternalId.Trim()) && !(y.BfwItem?.ExternalId ?? string.Empty).Equals(x.RidderItem?.CODE ?? string.Empty))
                                                                                    || (!ridderItemCodes.Contains(y.BfwItem?.ExternalId.Trim()) && x.RidderItem != null)
                                                                                    || !y.Quantity.Equals(x.Quantity)
                                                                                    || !y.BfwKnipMin.Equals(x.BfwKnipMin)
                                                                                    || !y.BfwKnipMax.Equals(x.BfwKnipMax)
                                                                                    || !y.Diameter.Equals(x.Diameter)
                                                                                    || !y.Length.Equals(x.Length)
                                                                                    || !y.Text.Trim().Equals(x.Text)
                                                                                    || !y.Weight.Equals(x.Weight)
                                                                                    || !y.TotalWeight.Equals(x.TotalWeight)
                                                                                    || !y.Width.Equals(x.Width)))
                                            ).ToList();

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Vergelijk buigstaatregels: {buigstaatregelsToInsert.Count} aanmaken, {buigstaatregelsToDelete.Count} te verwijderen, {buigstaatregelsToUpdate.Count} bij te werken.");

            if(buigstaatregelsToInsert.Count == 0 && buigstaatregelsToDelete.Count == 0 && buigstaatregelsToUpdate.Count == 0)
            {
                return;
            }

            //Haal de ridder buigstaten opnieuw op omdat deze nieuw zijn aangemaakt
            var ridderBuigstaten = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAAT", "PK_U_BUIGSTAAT, FK_ORDER, BUIGSTAATNUMMER",
                $"FK_ORDER IN ({string.Join(",", orders.Select(x => x.Id))})").DataTable.AsEnumerable().ToList();

            var result1 = InsertBuigstaatregels(sdkSessionSpanberg, bfwBuigstaatregels.Where(x => buigstaatregelsToInsert.Contains(x.UniqueKey)).ToList(), orders, items, ridderBuigstaten);
            var result2 = DeleteBuigstaatregels(sdkSessionSpanberg, ridderBuigstaatregels.Where(x => buigstaatregelsToDelete.Contains(x.UniqueKey)).ToList());

            var bfwBuigstaatregelsToUpdate = bfwBuigstaatregels.Where(x => buigstaatregelsToUpdate.Select(y => y.UniqueKey).Contains(x.UniqueKey)).ToList();
            var result3 = UpdateRidderBuigstaatregels(sdkSessionSpanberg, buigstaatregelsToUpdate, bfwBuigstaatregelsToUpdate, orders, items, ridderBuigstaten);

            if (!result1 || !result2 || !result3)
            {
                return;
            }

            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Buigstaatregels in Ridder bijgewerkt.");
        }

        private bool UpdateRidderFasen(SdkSession sdkSessionSpanberg, List<Fase> fasenToUpdate, List<Fase> bfwFasen, List<Order> orders)
        {
            if (!fasenToUpdate.Any())
            {
                return true;
            }

            var faseIds = fasenToUpdate.Select(x => x.Id).ToList();

            var rsBfwFasen = sdkSessionSpanberg.GetRecordsetColumns("U_BFWFASE", "", $"PK_U_BFWFASE IN ({string.Join(",", faseIds)})");
            rsBfwFasen.UseDataChanges = false;
            rsBfwFasen.UpdateWhenMoveRecord = false;

            rsBfwFasen.MoveFirst();
            while (!rsBfwFasen.EOF)
            {
                //Order en code zullen niet wijzigen. Anders wordt de fase verwijderd en opnieuw aangemaakt.
                var orderNumber = orders.First(x => x.Id == (int)rsBfwFasen.GetField("FK_ORDER").Value).ORDERNUMBER;

                var bfwFase = bfwFasen.First(x => x.BfwWerkNr == orderNumber && x.Code.Equals(rsBfwFasen.GetField("CODE").Value.ToString()));

                rsBfwFasen.SetFieldValue("DESCRIPTION", bfwFase.Description);

                rsBfwFasen.MoveNext();
            }

            var updateResult2 = rsBfwFasen.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"BFW Fasen bijwerken is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                return false;
            }

            return true;
        }

        private bool UpdateRidderBuigstaten(SdkSession sdkSessionSpanberg, List<Buigstaat> buigstatenToUpdate, List<Buigstaat> bfwBuigstaten, List<Order> orders, List<Fase> ridderFasen)
        {
            if (!buigstatenToUpdate.Any())
            {
                return true;
            }

            var buigstatenIds = buigstatenToUpdate.Select(x => x.Id).ToList();

            var rsBuigstaten = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAAT", "", $"PK_U_BUIGSTAAT IN ({string.Join(",", buigstatenIds)})");
            rsBuigstaten.UseDataChanges = false;
            rsBuigstaten.UpdateWhenMoveRecord = false;

            rsBuigstaten.MoveFirst();
            while (!rsBuigstaten.EOF)
            {
                //Order en buigstaatnr zullen niet wijzigen. Anders wordt de buigstaat verwijderd en opnieuw aangemaakt.
                var orderNumber = orders.First(x => x.Id == (int)rsBuigstaten.GetField("FK_ORDER").Value).ORDERNUMBER;
                var bfwBuigstaat = bfwBuigstaten.First(x => x.UniqueKey.Equals($"{orderNumber}_{rsBuigstaten.GetField("BUIGSTAATNUMMER").Value}"));

                var bfwFase = ridderFasen.First(x => x.RidderOrderId == (int)rsBuigstaten.GetField("FK_ORDER").Value && x.Code.Equals(bfwBuigstaat.BfwFase));

                rsBuigstaten.SetFieldValue("FK_BFWFASE", bfwFase.Id);

                rsBuigstaten.SetFieldValue("ELEMENT", bfwBuigstaat.Element);
                rsBuigstaten.SetFieldValue("ELEMENT_1", new string(bfwBuigstaat.Element.Where(char.IsDigit).ToArray()).Trim());
                rsBuigstaten.SetFieldValue("ELEMENT_2", new string(bfwBuigstaat.Element.Where(char.IsLetter).ToArray()).Trim());

                rsBuigstaten.SetFieldValue("DESCRIPTION", bfwBuigstaat.Description);
                rsBuigstaten.SetFieldValue("BFWDELIVERYDATE", bfwBuigstaat.DeliveryDate);
                rsBuigstaten.SetFieldValue("DRAWINGNUMBER", bfwBuigstaat.DrawingNumber);
                rsBuigstaten.SetFieldValue("BFWSHIPMENTID", bfwBuigstaat.BfwShipmentId);

                rsBuigstaten.SetFieldValue("BFWINVOICEID", bfwBuigstaat.BfwInvoiceId);
                rsBuigstaten.SetFieldValue("BFWINVOICECOMPLETE", bfwBuigstaat.BfwInvoiceComplete);
                rsBuigstaten.SetFieldValue("BFWFACTDATUM", bfwBuigstaat.BfwInvoiceDate);

                rsBuigstaten.MoveNext();
            }

            var updateResult2 = rsBuigstaten.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Buigstaten bijwerken is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                return false;
            }

            return true;
        }

        private bool UpdateRidderBuigstaatregels(SdkSession sdkSessionSpanberg, List<Buigstaatregel> buigstaatregelsToUpdate, List<Buigstaatregel> bfwBuigstaatregels, List<Order> orders, 
            List<Item> items, List<DataRow> ridderBuigstaten)
        {
            if (!buigstaatregelsToUpdate.Any())
            {
                return true;
            }

            var batches = GlobalFunctions.SplitList<Buigstaatregel>(buigstaatregelsToUpdate, 1000);

            for (int i = 0; i < batches.Count; i++)
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start bijwerken {buigstaatregelsToUpdate.Count} buigstaten (batch {i + 1} / {batches.Count})");
                var buigstatenThisBatch = batches[i];


                var buigstaatregelIds = buigstatenThisBatch.Select(x => x.Id).ToList();

                var rsBuigstaatregels = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAATREGEL", "", $"PK_U_BUIGSTAATREGEL IN ({string.Join(",", buigstaatregelIds)})");
                rsBuigstaatregels.UseDataChanges = false;
                rsBuigstaatregels.UpdateWhenMoveRecord = false;

                rsBuigstaatregels.MoveFirst();
                while (!rsBuigstaatregels.EOF)
                {
                    //Order, buigstaatnr en regelnr, zullen niet wijzigen. Anders wordt de buigstaat verwijderd en opnieuw aangemaakt.
                    var ridderBuigstaat = ridderBuigstaten.FirstOrDefault(x => x.Field<int>("PK_U_BUIGSTAAT") == (int)rsBuigstaatregels.GetField("FK_BUIGSTAAT").Value);

                    if (ridderBuigstaat == null)
                    {
                        _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Geen ridder buigstaat id gevonden met id {(int)rsBuigstaatregels.GetField("FK_BUIGSTAAT").Value}, bijwerken mislukt.");
                        rsBuigstaatregels.MoveNext();
                        continue;
                    }

                    var orderNumber = orders.First(x => x.Id == ridderBuigstaat.Field<int>("FK_ORDER")).ORDERNUMBER;
                    var currentUniqueKey = $"{orderNumber}_{ridderBuigstaat.Field<int>("BUIGSTAATNUMMER")}_{(int)rsBuigstaatregels.GetField("LINENUMBER").Value}";

                    var bfwBuigstaatregel = bfwBuigstaatregels.FirstOrDefault(x => x.UniqueKey.Equals(currentUniqueKey));

                    if (bfwBuigstaatregel == null)
                    {
                        _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Geen corresponderen bfw buigstaatregel gevonden met '{currentUniqueKey}', bijwerken mislukt.");
                        rsBuigstaatregels.MoveNext();
                        continue;
                    }

                    rsBuigstaatregels.SetFieldValue("QUANTITY", bfwBuigstaatregel.Quantity);
                    rsBuigstaatregels.SetFieldValue("BFWKNIPMIN", bfwBuigstaatregel.BfwKnipMin);
                    rsBuigstaatregels.SetFieldValue("BFWKNIPMAX", bfwBuigstaatregel.BfwKnipMax);
                    rsBuigstaatregels.SetFieldValue("DIAMETER", bfwBuigstaatregel.Diameter);
                    rsBuigstaatregels.SetFieldValue("LENGTH", bfwBuigstaatregel.Length);
                    rsBuigstaatregels.SetFieldValue("TEXT", bfwBuigstaatregel.Text);
                    rsBuigstaatregels.SetFieldValue("WEIGHT", bfwBuigstaatregel.Weight);
                    rsBuigstaatregels.SetFieldValue("TOTALWEIGHT", bfwBuigstaatregel.TotalWeight);
                    rsBuigstaatregels.SetFieldValue("WIDTH", bfwBuigstaatregel.Width);


                    var codeMatIsGewijzigd = !rsBuigstaatregels.GetField("BFWCODEMAT").Value.ToString().Equals(bfwBuigstaatregel.BfwCodeMat);
                    if(codeMatIsGewijzigd)
                    {
                        rsBuigstaatregels.SetFieldValue("BFWCODEMAT", bfwBuigstaatregel.BfwCodeMat);

                        var ridderItem = bfwBuigstaatregel.BfwItem != null
                            ? items.FirstOrDefault(x => x.CODE.Equals(bfwBuigstaatregel.BfwItem.ExternalId, StringComparison.OrdinalIgnoreCase))
                            : null;

                        if (ridderItem == null)
                        {
                            /*
                            if(bfwBuigstaatregel.BfwItem == null)
                            {
                                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Geen artikel gevonden met bfw item code '{bfwBuigstaatregel.BfwCodeMat}' bij buigstaatregel {bfwBuigstaatregel.UniqueKey}.");
                            }
                            else
                            {
                                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Geen artikel gevonden in Ridder voor BFW artikel '{bfwBuigstaatregel.BfwItem.CodeMaterial}' - '{bfwBuigstaatregel.BfwItem.ExternalId}'.");
                            }*/
                        }
                        else
                        {
                            rsBuigstaatregels.SetFieldValue("FK_ITEM", ridderItem.Id);
                        }
                    }

                    rsBuigstaatregels.MoveNext();
                }

                var updateResult2 = rsBuigstaatregels.Update2();

                if (updateResult2.Any(x => x.HasError))
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Buigstaatregels bijwerken mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                    return false;
                }
            }

            return true;
        }

        private bool DeleteFasen(SdkSession sdkSessionSpanberg, List<Fase> fasenToDelete)
        {
            if (!fasenToDelete.Any())
            {
                return true;
            }

            var faseIds = fasenToDelete.Select(x => x.Id).ToList();

            var rsBfwFasen = sdkSessionSpanberg.GetRecordsetColumns("U_BFWFASE", "", $"PK_U_BFWFASE IN ({string.Join(",", faseIds)})");
            rsBfwFasen.UseDataChanges = false;
            rsBfwFasen.UpdateWhenMoveRecord = false;

            rsBfwFasen.MoveFirst();
            while (!rsBfwFasen.EOF)
            {
                rsBfwFasen.Delete();
                rsBfwFasen.MoveNext();
            }

            var updateResult2 = rsBfwFasen.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Verwijderen fasen mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                return false;
            }

            return true;
        }

        private bool DeleteBuigstaten(SdkSession sdkSessionSpanberg, List<Buigstaat> buigstatenToDelete)
        {
            if(!buigstatenToDelete.Any())
            {
                return true;
            }

            var buigstatenIds = buigstatenToDelete.Select(x => x.Id).ToList();

            var rsBuigstaten = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAAT", "", $"PK_U_BUIGSTAAT IN ({string.Join(",", buigstatenIds)})");
            rsBuigstaten.UseDataChanges = false;
            rsBuigstaten.UpdateWhenMoveRecord = false;

            rsBuigstaten.MoveFirst();
            while(!rsBuigstaten.EOF)
            {
                rsBuigstaten.Delete();
                rsBuigstaten.MoveNext();
            }

            var updateResult2 = rsBuigstaten.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Verwijderen buigstaten mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                return false;
            }

            return true;
        }

        private bool DeleteBuigstaatregels(SdkSession sdkSessionSpanberg, List<Buigstaatregel> buigstaatregelsToDelete)
        {
            if (!buigstaatregelsToDelete.Any())
            {
                return true;
            }

            var buigstaatregelIds = buigstaatregelsToDelete.Select(x => x.Id).ToList();

            var rsBuigstaatregels = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAATREGEL", "", $"PK_U_BUIGSTAATREGEL IN ({string.Join(",", buigstaatregelIds)})");
            rsBuigstaatregels.UseDataChanges = false;
            rsBuigstaatregels.UpdateWhenMoveRecord = false;

            rsBuigstaatregels.MoveFirst();
            while (!rsBuigstaatregels.EOF)
            {
                rsBuigstaatregels.Delete();
                rsBuigstaatregels.MoveNext();
            }

            var updateResult2 = rsBuigstaatregels.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Verwijderen buigstaatregels mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                return false;
            }

            return true;
        }

        private bool InsertFasen(SdkSession sdkSessionSpanberg, List<Fase> fasenToInsert, List<Order> orders)
        {
            if (!fasenToInsert.Any())
            {
                return true;
            }

            var batches = GlobalFunctions.SplitList<Fase>(fasenToInsert, 1000);

            for (int i = 0; i < batches.Count; i++)
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start aanmaken {fasenToInsert.Count} nieuwe fasen (batch {i + 1} / {batches.Count})");
                var fasenThisBatch = batches[i];

                var rsBfwFasen = sdkSessionSpanberg.GetRecordsetColumns("U_BFWFASE", "", "PK_U_BFWFASE = NULL");
                rsBfwFasen.UseDataChanges = false;
                rsBfwFasen.UpdateWhenMoveRecord = false;

                foreach (var fase in fasenThisBatch)
                {
                    var orderId = orders.First(x => x.ORDERNUMBER == fase.BfwWerkNr).Id;

                    rsBfwFasen.AddNew();
                    rsBfwFasen.SetFieldValue("FK_ORDER", orderId);
                    rsBfwFasen.SetFieldValue("CODE", fase.Code);
                    rsBfwFasen.SetFieldValue("DESCRIPTION", fase.Description);
                }

                var updateResult2 = rsBfwFasen.Update2();

                if (updateResult2.Any(x => x.HasError))
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Aanmaken nieuwe fasen is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                    return false;
                }
            }


            return true;
        }

        private bool InsertBuigstaten(SdkSession sdkSessionSpanberg, List<Buigstaat> buigstatenToInsert, List<Order> orders, List<Fase> ridderFasen)
        {
            if(!buigstatenToInsert.Any())
            {
                return true;
            }

            var batches = GlobalFunctions.SplitList<Buigstaat>(buigstatenToInsert, 1000);

            for (int i = 0; i < batches.Count; i++)
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start aanmaken {buigstatenToInsert.Count} nieuwe buigstaten (batch {i + 1} / {batches.Count})");
                var buigstatenThisBatch = batches[i];

                var rsBuigstaten = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAAT", "", "PK_U_BUIGSTAAT = NULL");
                rsBuigstaten.UseDataChanges = false;
                rsBuigstaten.UpdateWhenMoveRecord = false;

                foreach (var buigstaat in buigstatenThisBatch)
                {
                    var orderId = orders.First(x => x.ORDERNUMBER == buigstaat.BfwWerkNr).Id;
                    var bfwFase = ridderFasen.First(x => x.RidderOrderId == orderId && x.Code.Equals(buigstaat.BfwFase));

                    rsBuigstaten.AddNew();
                    rsBuigstaten.SetFieldValue("FK_ORDER", orderId);
                    rsBuigstaten.SetFieldValue("BUIGSTAATNUMMER", buigstaat.Buigstaatnummer);
                    rsBuigstaten.SetFieldValue("FK_BFWFASE", bfwFase.Id);

                    rsBuigstaten.SetFieldValue("ELEMENT", buigstaat.Element);
                    rsBuigstaten.SetFieldValue("DESCRIPTION", buigstaat.Description);
                    rsBuigstaten.SetFieldValue("BFWDELIVERYDATE", buigstaat.DeliveryDate);
                    rsBuigstaten.SetFieldValue("DRAWINGNUMBER", buigstaat.DrawingNumber);
                    rsBuigstaten.SetFieldValue("BFWSHIPMENTID", buigstaat.BfwShipmentId);

                    rsBuigstaten.SetFieldValue("BFWINVOICEID", buigstaat.BfwInvoiceId);
                    rsBuigstaten.SetFieldValue("BFWINVOICECOMPLETE", buigstaat.BfwInvoiceComplete);
                    rsBuigstaten.SetFieldValue("BFWFACTDATUM", buigstaat.BfwInvoiceDate);
                }

                var updateResult2 = rsBuigstaten.Update2();

                if (updateResult2.Any(x => x.HasError))
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Aanmaken nieuwe buigstaten is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                    return false;
                }
            }


            return true;
        }

        private bool InsertBuigstaatregels(SdkSession sdkSessionSpanberg, List<Buigstaatregel> buigstaatregelsToInsert, List<Order> orders, List<Item> items, List<DataRow> ridderBuigstaten)
        {
            if (!buigstaatregelsToInsert.Any())
            {
                return true;
            }

            var batches = GlobalFunctions.SplitList<Buigstaatregel>(buigstaatregelsToInsert, 1000);



            for (int i = 0; i < batches.Count; i++)
            {
                _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start aanmaken {buigstaatregelsToInsert.Count} nieuwe buigstaatregels (batch {i + 1} / {batches.Count})");
                var buigstaatregelsThisBatch = batches[i];


                var rsBuigstaatregels = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAATREGEL", "", "PK_U_BUIGSTAATREGEL = NULL");
                rsBuigstaatregels.UseDataChanges = false;
                rsBuigstaatregels.UpdateWhenMoveRecord = false;

                foreach (var buigstaatregel in buigstaatregelsThisBatch)
                {
                    var orderId = orders.FirstOrDefault(x => x.ORDERNUMBER == buigstaatregel.Buigstaat.BfwWerkNr)?.Id ?? 0;

                    if(orderId == 0)
                    {
                        _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Bepaling order id voor buigstaatregel {buigstaatregel.UniqueKey} is mislukt. Aanmaken wordt overgeslagen.");
                        continue;
                    }

                    var ridderBuigstaat = ridderBuigstaten.FirstOrDefault(x => x.Field<int>("FK_ORDER") == orderId && x.Field<int>("BUIGSTAATNUMMER") == buigstaatregel.Buigstaat.Buigstaatnummer);

                    if(ridderBuigstaat == null)
                    {
                        _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Bepaling buigstaat id voor buigstaatregel {buigstaatregel.UniqueKey} is mislukt. Aanmaken wordt overgeslagen.");
                        continue;
                    }

                    rsBuigstaatregels.AddNew();
                    rsBuigstaatregels.SetFieldValue("FK_BUIGSTAAT", ridderBuigstaat.Field<int>("PK_U_BUIGSTAAT"));
                    rsBuigstaatregels.SetFieldValue("LINENUMBER", buigstaatregel.LineNumber);

                    rsBuigstaatregels.SetFieldValue("BFWCODEMAT", buigstaatregel.BfwCodeMat);
                    rsBuigstaatregels.SetFieldValue("QUANTITY", buigstaatregel.Quantity);
                    rsBuigstaatregels.SetFieldValue("BFWKNIPMIN", buigstaatregel.BfwKnipMin);
                    rsBuigstaatregels.SetFieldValue("BFWKNIPMAX", buigstaatregel.BfwKnipMax);
                    rsBuigstaatregels.SetFieldValue("DIAMETER", buigstaatregel.Diameter);
                    rsBuigstaatregels.SetFieldValue("LENGTH", buigstaatregel.Length);
                    rsBuigstaatregels.SetFieldValue("TEXT", buigstaatregel.Text);
                    rsBuigstaatregels.SetFieldValue("WEIGHT", buigstaatregel.Weight);
                    rsBuigstaatregels.SetFieldValue("TOTALWEIGHT", buigstaatregel.TotalWeight);
                    rsBuigstaatregels.SetFieldValue("WIDTH", buigstaatregel.Width);

                    var ridderItem = buigstaatregel.BfwItem != null
                        ? items.FirstOrDefault(x => x.CODE.Equals(buigstaatregel?.BfwItem.ExternalId ?? string.Empty, StringComparison.OrdinalIgnoreCase))
                        : null;

                    if (ridderItem == null)
                    {
                        /*
                        if (buigstaatregel.BfwItem == null)
                        {
                            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Geen artikel gevonden met bfw item code '{buigstaatregel.BfwCodeMat}' bij buigstaatregel {buigstaatregel.UniqueKey}.");
                        }
                        else
                        {
                            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Geen artikel gevonden in Ridder voor BFW artikel '{buigstaatregel.BfwItem.CodeMaterial}' - '{buigstaatregel.BfwItem.ExternalId}'.");
                        }*/
                    }
                    else
                    {
                        rsBuigstaatregels.SetFieldValue("FK_ITEM", ridderItem.Id);
                    }
                }

                var updateResult2 = rsBuigstaatregels.Update2();

                if (updateResult2.Any(x => x.HasError))
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Aanmaken nieuwe buigstaten is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                    return false;
                }
            }


            return true;
        }

        /*
        private List<DataRow> GetAndCreateBfwFases(SdkSession sdkSessionSpanberg, List<Buigstaat> buigstaten, List<Order> orders)
        {
            var rsBfwFases = sdkSessionSpanberg.GetRecordsetColumns("U_BFWFASE", "", $"FK_ORDER IN ({string.Join(",", orders.Select(x => x.Id))})", "");
            var ridderBfwFasesUniqueKeys = rsBfwFases.DataTable.AsEnumerable().Select(x => $"{orders.First(y => y.Id == x.Field<int>("FK_ORDER")).ORDERNUMBER}_{x.Field<string>("CODE")}").Distinct().ToList();
            var bfwFasesUniqueKeys = buigstaten.Select(x => $"{x.BfwWerkNr}_{x.BfwFase}").Distinct().ToList();

            var bfwFasesToCreate = bfwFasesUniqueKeys.Except(ridderBfwFasesUniqueKeys).ToList();

            if (bfwFasesToCreate.Any())
            {
                foreach (var bfwFase in bfwFasesToCreate)
                {
                    var orderNumber = bfwFase.Split('_')[0];
                    var faseCode = bfwFase.Split('_')[1];

                    var orderId = orders.First(x => x.ORDERNUMBER.ToString().Equals(orderNumber)).Id;

                    rsBfwFases.AddNew();
                    rsBfwFases.SetFieldValue("FK_ORDER", orderId);
                    rsBfwFases.SetFieldValue("CODE", faseCode);
                }

                var updateResult = rsBfwFases.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Aanmaken nieuwe BFW fases is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
                    return null;
                }
            }

            return rsBfwFases.DataTable.AsEnumerable().ToList();
        }*/

        private List<Fase> GetRidderFasen(SdkSession sdkSessionSpanberg, List<Order> orders)
        {
            return sdkSessionSpanberg.GetRecordsetColumns("U_BFWFASE", "", $"FK_ORDER IN ({string.Join(",", orders.Select(x => x.Id))})")
                .DataTable.AsEnumerable().Select(x => new Fase()
                {
                    Id = x.Field<int>("PK_U_BFWFASE"),
                    RidderOrderId = x.Field<int>("FK_ORDER"),
                    RidderOrder = orders.First(y => y.Id == x.Field<int>("FK_ORDER")),
                    Code = x.Field<string>("CODE"),
                    Description = x.Field<string>("DESCRIPTION"),
                    UniqueKey = $"{orders.First(y => y.Id == x.Field<int>("FK_ORDER")).ORDERNUMBER}_{x.Field<string>("CODE")}",
                }).ToList();
        }

        private List<Buigstaat> GetRidderBuigstaten(SdkSession sdkSessionSpanberg, List<Order> orders, List<Fase> ridderFasen)
        {
            var buigstaten = sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAAT", "", $"FK_ORDER IN ({string.Join(",", orders.Select(x => x.Id))})")
                .DataTable.AsEnumerable().Select(x => new Buigstaat()
                {
                    Id = x.Field<int>("PK_U_BUIGSTAAT"),
                    RidderOrder = orders.First(y => y.Id == x.Field<int>("FK_ORDER")),
                    Buigstaatnummer = x.Field<int>("BUIGSTAATNUMMER"),
                    RidderFaseId = x.Field<int>("FK_BFWFASE"),
                    Element = x.Field<string>("ELEMENT"),
                    Description = x.Field<string>("DESCRIPTION"),
                    DeliveryDate = x.Field<DateTime?>("BFWDELIVERYDATE"),
                    DrawingNumber = x.Field<string>("DRAWINGNUMBER"),
                    BfwShipmentId = x.Field<int>("BFWSHIPMENTID"),
                    BfwInvoiceId = x.Field<int>("BFWINVOICEID"),
                    BfwInvoiceComplete = x.Field<bool>("BFWINVOICECOMPLETE"),
                    BfwInvoiceDate = x.Field<DateTime?>("BFWFACTDATUM"),
                    UniqueKey = $"{orders.First(y => y.Id == x.Field<int>("FK_ORDER")).ORDERNUMBER}_{x.Field<int>("BUIGSTAATNUMMER")}",
                }).ToList();

            if(!buigstaten.Any())
            {
                return new List<Buigstaat>();
            }

            buigstaten.ForEach(x => x.RidderFase = ridderFasen.First(y => y.Id == x.RidderFaseId));

            return buigstaten;
        }

        private List<Buigstaatregel> GetRidderBuigstaatregels(SdkSession sdkSessionSpanberg, List<Buigstaat> ridderBuigstaten)
        {
            _ridderLogger.Log(SchedulerTask, "Spanberg", SchedulerLogLevel.Info, "Start ophalen Ridder buigstaatregels.");

            if (!ridderBuigstaten.Any())
            {
                return new List<Buigstaatregel>();
            }

            return sdkSessionSpanberg.GetRecordsetColumns("U_BUIGSTAATREGEL", "", $"FK_BUIGSTAAT IN ({string.Join(",", ridderBuigstaten.Select(x => x.Id))})")
                .DataTable.AsEnumerable().Select(x => new Buigstaatregel()
                {
                    Id = x.Field<int>("PK_U_BUIGSTAATREGEL"),
                    Buigstaat = ridderBuigstaten.First(y => y.Id == x.Field<int>("FK_BUIGSTAAT")),
                    RidderItemId = x.Field<int?>("FK_ITEM") ?? 0,
                    BfwCodeMat = x.Field<string>("BFWCODEMAT"),
                    BfwKnipMin = x.Field<int>("BFWKNIPMIN"),
                    BfwKnipMax = x.Field<int>("BFWKNIPMAX"),
                    Diameter = x.Field<double>("DIAMETER"),
                    Length = x.Field<int>("LENGTH"),
                    LineNumber = x.Field<int>("LINENUMBER"),
                    Quantity = x.Field<int>("QUANTITY"),
                    Text = x.Field<string>("TEXT"),
                    TotalWeight = x.Field<double>("TOTALWEIGHT"),
                    Weight = x.Field<double>("WEIGHT"),
                    Width = x.Field<int>("WIDTH"),
                    UniqueKey = $"{ridderBuigstaten.First(y => y.Id == x.Field<int>("FK_BUIGSTAAT")).UniqueKey}_{x.Field<int>("LINENUMBER")}",   //Ordernummer_Buigstaatnr_Regelnr
                }).ToList();
        }



        private List<Item> GetRidderItems(SdkSession sdkSessionSpanberg, List<Buigstaatregel> bfwBuigstaatregels)
        {
            if(!bfwBuigstaatregels.Any())
            {
                return new List<Item>();
            }

            var ridderItemCodes = bfwBuigstaatregels.Where(x => x.BfwItem != null).Select(x => x.BfwItem.ExternalId).Distinct().ToList();

            if(!ridderItemCodes.Any())
            {
                return new List<Item>();
            }

            return sdkSessionSpanberg.GetRecordsetColumns("R_ITEM", "CODE", $"CODE IN ('{string.Join("','", ridderItemCodes)}')")
                .DataTable.AsEnumerable().Select(x => new Item()
                {
                    Id = x.Field<int>("PK_R_ITEM"),
                    CODE = x.Field<string>("CODE"),
                }).ToList();
        }

        private List<Order> GetActualOrders(SdkSession sdkSessionSpanberg)
        {
            var wfStateNew = new Guid("6205d9d2-72d7-49df-8b4b-6e5c1c3d4927");
            var wfStateOnderhanden = new Guid("e83694f3-b6a8-4c2f-8c65-20107edf22bc");
            var wfStateAfgesloten = new Guid("eb0e5af0-52b1-4d0e-b203-4a4fb2f089bf");

            return sdkSessionSpanberg.GetRecordsetColumns("R_ORDER", "ORDERNUMBER", $"FK_WORKFLOWSTATE IN ('{wfStateNew}', '{wfStateOnderhanden}', '{wfStateAfgesloten}')")
                .DataTable.AsEnumerable().Select(x => new Order()
                {
                    Id = x.Field<int>("PK_R_ORDER"),
                    ORDERNUMBER = x.Field<int>("ORDERNUMBER"),
                }).ToList();
        }

        
    }
}
