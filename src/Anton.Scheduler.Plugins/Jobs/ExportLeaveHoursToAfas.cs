﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.AfasModels;
using Anton.Scheduler.Plugins.FrameworkModels;
using Anton.Scheduler.Plugins.Models;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Scheduler.Jobs;

namespace Anton.Scheduler.Plugins.Jobs
{
    class ExportLeaveHoursToAfas : RidderJob
    {
        public SdkSession _sdkSession { get; set; }
        private readonly RidderLogger _ridderLogger;
        private const string AfasTvtCode = "T";
        private const string AfasDiscontinuiteitCode = "D";
        private const string SchedulerTask = "ExportLeaveHoursToAfas";
        private const long MinimalTime = 3000000000; //Minimaal 5 min

        public ExportLeaveHoursToAfas(SdkSession sdkSession) : base(sdkSession)
        {
            _sdkSession = sdkSession;
            _ridderLogger = new RidderLogger(sdkSession);
        }

        protected override void ExecuteImpl(IJobExecutionContext context)
        {
            //DB
            //22-12-2021

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start plugin exporteren verlofuren naar AFAS");

            var sqlServerInstanceName = ConfigurationManager.AppSettings["SqlServerInstanceName"].ToString();
            var afasStartUrl = ConfigurationManager.AppSettings["AfasStartUrl"].ToString();
            var afasToken = ConfigurationManager.AppSettings["AfasToken"].ToString();
            var leaveHoursMonthsBack = ConfigurationManager.AppSettings["LeaveHoursMonthsBack"].ToString();
            var leaveHoursMonthsForward = ConfigurationManager.AppSettings["LeaveHoursMonthsForward"].ToString();

            var allCompanies = SqlHelper.DetermineRidderAdministrationsWithAfasCompanyName(_ridderLogger, sqlServerInstanceName);

            foreach (var company in allCompanies)
            {
                var ridderAdministrationName = company.Equals("OV & Loos Wapeningscentrale")
                    ? "Loos Wapeningscentrale"
                    : company;

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AfasCompanyFilter"].ToString()) 
                        && !ConfigurationManager.AppSettings["AfasCompanyFilter"].ToString().Equals(company))
                {
                    continue;
                }

                ProcessExportLeaveHoursPerCompany(SchedulerTask, ridderAdministrationName, afasStartUrl, afasToken, leaveHoursMonthsBack, leaveHoursMonthsForward);
            }

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Exporteren verlofuren naar AFAS afgerond.");
        }

        private void ProcessExportLeaveHoursPerCompany(string schedulerTask, string company, string afasStartUrl, string afasToken, string leaveHoursMonthsBack, string leaveHoursMonthsForward)
        {
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"Start export verlofuren {company}.");

            var sdkSessionCompany = GlobalFunctions.GetSdkSession(_ridderLogger, _sdkSession, schedulerTask, company);

            if (sdkSessionCompany == null)
            {
                return;
            }

            var crmSetting = sdkSessionCompany.Find<CrmSetting>().FirstOrDefault();

            if (string.IsNullOrEmpty(crmSetting?.AFASCOMPANYNAME))
            {
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, "Geen AFAS organisatie eenheid codes gevonden. Verlof uren export stopt.");
                return;
            }

            var afasOrganisationUnitCodes = crmSetting?.AFASCOMPANYNAME;

            var workactivitiesWithAfasAbsenceCode = sdkSessionCompany.Find<Workactivity>(x => !string.IsNullOrEmpty(x.AFASABSENCECODE)).ToList();

            if (!workactivitiesWithAfasAbsenceCode.Any())
            {
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"Geen bewerkingen met AFAS verlofcode gevonden.");
                return;
            }

            int iLeaveHoursMonthsBack;
            int iLeaveHoursMonthsForward;
            try
            {
                iLeaveHoursMonthsBack = int.Parse(leaveHoursMonthsBack);
                iLeaveHoursMonthsForward = int.Parse(leaveHoursMonthsForward);
            }
            catch
            {
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, "Omzetten app settings months back/forward mislukt.");
                return;
            }

            var startInterval = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(iLeaveHoursMonthsBack * -1);
            var endInterval = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(iLeaveHoursMonthsForward).AddDays(-1);

            
            //Export verlofuren
            var employeesAfas = AfasHelper.GetEmployeesFromAfas(_ridderLogger, afasStartUrl, schedulerTask, company, afasOrganisationUnitCodes, afasToken);
            var afasLeaves = AfasHelper.GetLeavesFromAfas(_ridderLogger, afasStartUrl, schedulerTask, company, afasOrganisationUnitCodes, afasToken, workactivitiesWithAfasAbsenceCode,
                startInterval, endInterval, employeesAfas);


            var ridderEmployees = RidderHelper.GetEmployees(sdkSessionCompany, employeesAfas);
            var ridderLeaves = RidderHelper.GetLeaves(_ridderLogger, sdkSessionCompany, workactivitiesWithAfasAbsenceCode, startInterval, endInterval, ridderEmployees, MinimalTime);

            if(!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString()))
            {
                employeesAfas = employeesAfas.Where(x => x.EmployeeId.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();
                afasLeaves = afasLeaves.Where(x => x.EmployeeId.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();

                ridderEmployees = ridderEmployees.Where(x => x.CODE.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();
                ridderLeaves = ridderLeaves.Where(x => x.EmployeeCode.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();
            }

            //Gewijzigde records vallen zowel in te verwijderen als toe te voegen
            var afasUniqueKeys = afasLeaves.Select(x => x.UniqueKey).ToList();
            var ridderUniqueKeys = ridderLeaves.Select(x => x.UniqueKey).ToList();

            var uniqueKeysToDelete = afasUniqueKeys.Except(ridderUniqueKeys).ToList();
            var uniqueKeysToCreate = ridderUniqueKeys.Except(afasUniqueKeys).ToList();

            var afasLeavesToDelete = afasLeaves.Where(x => uniqueKeysToDelete.Contains(x.UniqueKey)).ToList();
            var ridderLeavesToCreate = ridderLeaves.Where(x => uniqueKeysToCreate.Contains(x.UniqueKey)).ToList();

            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{afasLeavesToDelete.Count} AFAS verlofboekingen gevonden om te verwijderen.");
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{ridderLeavesToCreate.Count} Ridder verlofboekingen gevonden om te exporteren naar AFAS.");

            AfasHelper.DeleteLeaves(_ridderLogger, afasStartUrl, schedulerTask, company, afasToken, afasLeavesToDelete);
            
            AfasHelper.CreateLeaves(_ridderLogger, afasStartUrl, schedulerTask, company, afasToken, ridderLeavesToCreate, employeesAfas);


            //Export TvT overuren
            var afasLeaveCorrectionsTvT = AfasHelper.GetLeaveCorrectionsFromAfas(_ridderLogger, afasStartUrl, schedulerTask, company, afasOrganisationUnitCodes, afasToken, startInterval, endInterval, AfasTvtCode, employeesAfas);
            var ridderTvtOvertimes = RidderHelper.GetTvtOverTimes(_ridderLogger, sdkSessionCompany, startInterval, endInterval, ridderEmployees);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString()))
            {
                afasLeaveCorrectionsTvT = afasLeaveCorrectionsTvT.Where(x => x.Medewerker.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();
                ridderTvtOvertimes = ridderTvtOvertimes.Where(x => x.EmployeeCode.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();
            }

            var afasUniqueKeys2 = afasLeaveCorrectionsTvT.Select(x => x.UniqueKey).ToList();
            var ridderUniqueKeys2 = ridderTvtOvertimes.Select(x => x.UniqueKey).ToList();

            var uniqueKeysToCreate2 = ridderUniqueKeys2.Except(afasUniqueKeys2).ToList();
            var leaveCorrectionsTvTToCorrectInAfas = CompareExisitingLeaveCorrections(company, afasLeaveCorrectionsTvT, ridderTvtOvertimes, false);

            if(afasLeaveCorrectionsTvT == null)
            {
                return;
            }

            var ridderTvtOvertimesToCreate = ridderTvtOvertimes.Where(x => uniqueKeysToCreate2.Contains(x.UniqueKey)).ToList();

            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{ridderTvtOvertimesToCreate.Count} TvT overuren gevonden om aan te maken in AFAS.");
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{leaveCorrectionsTvTToCorrectInAfas.Count} AFAS verlofcorrecties TvT gevonden om te corrigeren.");

            var allLeaveCorrectionsTvTToCreateInAfas = ridderTvtOvertimesToCreate.Concat(leaveCorrectionsTvTToCorrectInAfas).ToList();

            AfasHelper.CreateLeaveCorrections(_ridderLogger, afasStartUrl, schedulerTask, company, afasToken, allLeaveCorrectionsTvTToCreateInAfas, AfasTvtCode, employeesAfas, false);

            //Exporteer discontinuïteit opbouw
            var afasLeaveCorrectionsDiscontinuity = AfasHelper.GetLeaveCorrectionsFromAfas(_ridderLogger, afasStartUrl, schedulerTask, company, afasOrganisationUnitCodes, afasToken, startInterval, endInterval, 
                AfasDiscontinuiteitCode, employeesAfas);
            var ridderDiscontinuityOvertimes = RidderHelper.GetDiscontinuiteitOverTimes(_ridderLogger, sdkSessionCompany, startInterval, endInterval, ridderEmployees);

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString()))
            {
                afasLeaveCorrectionsDiscontinuity = afasLeaveCorrectionsDiscontinuity.Where(x => x.Medewerker.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();
                ridderDiscontinuityOvertimes = ridderDiscontinuityOvertimes.Where(x => x.EmployeeCode.Equals(ConfigurationManager.AppSettings["AfasEmployeeFilter"].ToString())).ToList();
            }

            var afasUniqueKeys3 = afasLeaveCorrectionsDiscontinuity.Select(x => x.UniqueKey).ToList();
            var ridderUniqueKeys3 = ridderDiscontinuityOvertimes.Select(x => x.UniqueKey).ToList();

            var uniqueKeysToCreate3 = ridderUniqueKeys3.Except(afasUniqueKeys3).ToList();
            var leaveCorrectionsDiscontinuityToCorrectInAfas = CompareExisitingLeaveCorrections(company, afasLeaveCorrectionsDiscontinuity, ridderDiscontinuityOvertimes, true);

            if (leaveCorrectionsDiscontinuityToCorrectInAfas == null)
            {
                return;
            }

            var ridderDiscontinuityOvertimesToCreate = ridderDiscontinuityOvertimes.Where(x => uniqueKeysToCreate3.Contains(x.UniqueKey)).ToList();

            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{ridderDiscontinuityOvertimesToCreate.Count} discontinuïteit overuren gevonden om aan te maken in AFAS.");
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{leaveCorrectionsDiscontinuityToCorrectInAfas.Count} AFAS verlofcorrecties discontinuïteit gevonden om te corrigeren.");

            var allLeaveCorrectionsDiscontinuityToCreateInAfas = ridderDiscontinuityOvertimesToCreate.Concat(leaveCorrectionsDiscontinuityToCorrectInAfas).ToList();

            AfasHelper.CreateLeaveCorrections(_ridderLogger, afasStartUrl, schedulerTask, company, afasToken, allLeaveCorrectionsDiscontinuityToCreateInAfas, AfasDiscontinuiteitCode, employeesAfas, true);


            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"Export verlofuren {company} afgerond.");
        }

        private List<ProjectTime> CompareExisitingLeaveCorrections(string company, List<AfasLeaveCorrection> afasLeaveCorrections, List<ProjectTime> ridderTvtOvertimes, bool isDiscontinuity)
        {
            //We kunnen geen verlof correcties verwijderen in AFAS dus we gaan deze corrigeren bij afwijkingen
            var result = new List<ProjectTime>();

            var afasLeaveCorrectionsGroupedByEmployeeDate = afasLeaveCorrections.GroupBy(x => x.UniqueKey).ToList();

            foreach (var afasLeaveCorrectionsThisEmployeeDate in afasLeaveCorrectionsGroupedByEmployeeDate)
            {
                if(isDiscontinuity && afasLeaveCorrectionsThisEmployeeDate.First().Datum_boeking.Date == new DateTime(2022,1,1))
                {
                    continue; //Op 1-1-2022 zijn de discontinuiteit startsaldo's ingericht, deze niet corrigeren
                }


                var totalMinutesAfas = 0;

                foreach (var afasLeaveCorrection in afasLeaveCorrectionsThisEmployeeDate)
                {
                    var minutes = DetermineTotalMinutesOfAfasLeaveCorrection(company, afasLeaveCorrection);

                    if (minutes == null)
                    {
                        return null;
                    }

                    totalMinutesAfas += minutes.Value;
                }

                var ridderOverTimesThisEmployeeThisDate = ridderTvtOvertimes
                    .Where(x => x.EmployeeCode.Equals(afasLeaveCorrectionsThisEmployeeDate.First().Medewerker) && x.DATE.Date.Equals(afasLeaveCorrectionsThisEmployeeDate.First().Datum_boeking.Date)).ToList();

                var totalMinutesInRidder = isDiscontinuity
                    ? ridderOverTimesThisEmployeeThisDate.Sum(x => x.TimeBuildUpDiscontinuityInMinutes)
                    : ridderOverTimesThisEmployeeThisDate.Sum(x => x.TimeTvtInMinutes);

                if (totalMinutesAfas != totalMinutesInRidder)
                {
                    var timeToCorrect = totalMinutesInRidder - totalMinutesAfas;

                    result.Add(new ProjectTime()
                    {
                        EmployeeCode = afasLeaveCorrectionsThisEmployeeDate.First().Medewerker,
                        DATE = afasLeaveCorrectionsThisEmployeeDate.First().Datum_boeking,
                        TIMEOFINLIEU = isDiscontinuity ? 0 : TimeSpan.FromMinutes(Convert.ToDouble(timeToCorrect)).Ticks,
                        BUILDUPDISCONTINUITY = !isDiscontinuity ? 0 : TimeSpan.FromMinutes(Convert.ToDouble(timeToCorrect)).Ticks,
                    });
                }
            }


            return result;
        }

        private int? DetermineTotalMinutesOfAfasLeaveCorrection(string company, AfasLeaveCorrection afasLeaveCorrection)
        {
            var hours = afasLeaveCorrection.Aantal_uren.Split(':')[0];
            var minutes = afasLeaveCorrection.Aantal_uren.Split(':')[1];

            if (!int.TryParse(hours, out int iHours))
            {
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Bepaling aantal uur verlofcorrectie {afasLeaveCorrection.Medewerker} - {afasLeaveCorrection.Datum_boeking:dd-MM-yyyy} - {afasLeaveCorrection.Aantal_uren} is mislukt.");
                return null;
            }

            if (!int.TryParse(minutes, out int iMinutes))
            {
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Bepaling aantal minuten verlofcorrectie {afasLeaveCorrection.Medewerker} - {afasLeaveCorrection.Datum_boeking:dd-MM-yyyy} - {afasLeaveCorrection.Aantal_uren} is mislukt.");
                return null;
            }

            return iHours < 0
                ? (iHours * 60) - iMinutes
                : (iHours * 60) + iMinutes;
        }
    }
}
