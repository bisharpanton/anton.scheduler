﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.FrameworkModels;
using Anton.Scheduler.Plugins.Models;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Scheduler.Jobs;
using Ridder.Scheduler.Logging;

namespace Anton.Scheduler.Plugins.Jobs
{
    class CopySafetyReportsToIntercompany : RidderJob
    {
        public SdkSession _sdkSession { get; set; }
        private readonly RidderLogger _ridderLogger;
        private readonly string _tempDirectory = Path.GetTempPath();
        private readonly Guid _wfStateDone = new Guid("946b3c25-11ea-4d0a-98b9-8894c42e971e");
        private const string SchedulerTask = "CopySafetyReportsToIntercompany";

        public CopySafetyReportsToIntercompany(SdkSession sdkSession) : base(sdkSession)
        {
            _sdkSession = sdkSession;
            _ridderLogger = new RidderLogger(sdkSession);
        }

        protected override void ExecuteImpl(IJobExecutionContext context)
        {
            //DB
            //19-01-2021
            //Zoek in de Anton Groep in de view 'C_ALLSAFETYREPORTS' naar veiligheidsmeldingen die gekopieerd moeten worden in de andere administraties
            //In dat geval maak ze daar aan
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start scheduler plugin CopySafetyReportsToIntercompany");

            var allSafetyReportsToCopy = _sdkSession.Find<AllSafetyReport>(x =>
                x.INTERCOMPANYRELATION != IntercompanyRelation.N_v_t_ && !x.COPIEDTOINTERCOMPANYRELATION)
                .Where(x => !GetEnumDescription(x.INTERCOMPANYRELATION).Equals(x.COMPANY))
                .ToList();

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"{allSafetyReportsToCopy.Count} veiligheidsmeldingen gevonden om te kopiëren.");

            var allSafetyReportsToLinkBackRespons = _sdkSession.Find<AllSafetyReport>(x =>
                !string.IsNullOrEmpty(x.EXTERNALREPORTER) && x.FK_WORKFLOWSTATE.Value.Equals(_wfStateDone) && !x.RESPONSLINKEDBACK).ToList();

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"{allSafetyReportsToLinkBackRespons.Count} veiligheidsmeldingen gevonden om respons terug te kopiëren.");

            if (!allSafetyReportsToCopy.Any() && !allSafetyReportsToLinkBackRespons.Any())
            {
                return;
            }

            //First, create the safety reports in the intercompany administrations
            ProcessCreatingSafetyReportsIntercompanies(allSafetyReportsToCopy);

            //Second, update safety reports, set bool copied true
            UpdateSafetyReports(allSafetyReportsToCopy);

            //Thirth, link respons back to original safety report
            LinkBackResponsToOriginalSafetyReport(allSafetyReportsToLinkBackRespons);

            //Fourth, update respons safety report 
            UpdateResponsSafetyReport(allSafetyReportsToLinkBackRespons);

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Einde scheduler plugin CopySafetyReportsToIntercompany");
        }

        private void UpdateResponsSafetyReport(List<AllSafetyReport> allSafetyReportsToLinkBackRespons)
        {
            if (!allSafetyReportsToLinkBackRespons.Any())
            {
                return;
            }

            var companies = allSafetyReportsToLinkBackRespons.Select(x => x.COMPANY).Distinct().ToList();

            if (!companies.Any())
            {
                return;
            }

            foreach (var company in companies)
            {
                var allSafetyReportsToUpdateThisCompany =
                    allSafetyReportsToLinkBackRespons.Where(x => x.COMPANY.Equals(company)).ToList();

                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                    $"{allSafetyReportsToUpdateThisCompany.Count} veiligheidsmeldingen gevonden in '{company}' waarvan de respons succesvol is gekopieerd.");

                var configuration = new SdkConfiguration()
                {
                    CompanyName = company,
                    UserName = _sdkSession.Configuration.UserName,
                    Password = _sdkSession.Configuration.Password,
                    PersistSession = _sdkSession.Configuration.PersistSession,
                    RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
                };

                var sdkSessionCompany = company.Equals("Anton Groep")
                    ? _sdkSession
                    : new SdkSession(configuration) { AutoLogin = false };

                try
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Inloggen in '{company}'...");

                    sdkSessionCompany.Login();

                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Ingelogd in '{company}'");
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error,
                        $"Inloggen mislukt in '{company}', oorzaak: {e}.");

                    continue;
                }

                var safetyReportIdsToUpdate =
                    allSafetyReportsToUpdateThisCompany.Select(x => x.SAFETYREPORTID ?? 0).ToList();
                var safetyReportsThisCompany = sdkSessionCompany
                    .Find<SafetyReport>(x => safetyReportIdsToUpdate.Contains(x.Id)).ToList();

                safetyReportsThisCompany.ForEach(x => x.RESPONSLINKEDBACK = true);

                try
                {
                    sdkSessionCompany.UpdateBulk(safetyReportsThisCompany);
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error,
                        $"Bijwerken veiligheidsmeldingen respons in '{company}' is mislukt, oorzaak: {e}.");
                }

                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                    $"Bijwerken veiligheidsmeldingen respons in '{company}' afgerond.");
            }
        }

        private void LinkBackResponsToOriginalSafetyReport(List<AllSafetyReport> allSafetyReportsToLinkBackRespons)
        {
            LinkDocumentsToSafetyReports(allSafetyReportsToLinkBackRespons);

            if (!allSafetyReportsToLinkBackRespons.Any())
            {
                return;
            }

            var companies = allSafetyReportsToLinkBackRespons
                .Where(x => x.EXTERNALKEY.Split('_').Length == 2)
                .Select(x => x.EXTERNALKEY.Split('_').First()).Distinct().ToList();

            if (!companies.Any())
            {
                return;
            }

            foreach (var company in companies)
            {
                var allSafetyReportsToLinkBackResponsThisCompany =
                    allSafetyReportsToLinkBackRespons
                        .Where(x => x.EXTERNALKEY.Split('_').Length == 2 &&
                                    x.EXTERNALKEY.Split('_')[0].Equals(company)).ToList();

                var configuration = new SdkConfiguration()
                {
                    CompanyName = company,
                    UserName = _sdkSession.Configuration.UserName,
                    Password = _sdkSession.Configuration.Password,
                    PersistSession = _sdkSession.Configuration.PersistSession,
                    RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
                };

                var sdkSessionCompany = company.Equals("Anton Groep")
                    ? _sdkSession
                    : new SdkSession(configuration) { AutoLogin = false };

                try
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Inloggen in '{company}'...");

                    sdkSessionCompany.Login();

                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Ingelogd in '{company}'");
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error,
                        $"Inloggen mislukt in '{company}', oorzaak: {e}.");

                    continue;
                }

                var safetyReportIdsToUpdate =
                    allSafetyReportsToLinkBackResponsThisCompany
                        .Where(x => x.EXTERNALKEY.Split('_').Length == 2)
                        .Select(x => x.EXTERNALKEY.Split('_')[1]).Distinct().ToList();

                var safetyReportsThisCompany = sdkSessionCompany
                    .Find<SafetyReport>($"PK_C_VEILIGHEIDSMELDINGEN IN ({string.Join(",", safetyReportIdsToUpdate)})").ToList();

                foreach (var safetyReport in safetyReportsThisCompany)
                {
                    var intercompanySafetyReport = allSafetyReportsToLinkBackResponsThisCompany
                        .First(x => x.EXTERNALKEY.Split('_')[1].Equals(safetyReport.Id.ToString()));

                    //TODO: CAUSE, MEASURES en LONGTERMEASURES concatten? Dirk Pepping gevraagd.
                    safetyReport.RESPONSINTERCOMPANY = intercompanySafetyReport.MEMO;

                    safetyReport.CAUSE = intercompanySafetyReport.CAUSE;
                    safetyReport.MEASURES = intercompanySafetyReport.MEASURES;
                    safetyReport.LONGTERMMEASURES = intercompanySafetyReport.LONGTERMMEASURES;

                    CopyDocumentsBackAtRespons(sdkSessionCompany, safetyReport, intercompanySafetyReport.Documents);
                }

                try
                {
                    sdkSessionCompany.UpdateBulk(safetyReportsThisCompany);
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error,
                        $"Kopiëren respons in '{company}' is mislukt, oorzaak: {e}.");
                }

                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                    $"Kopiëren respons in '{company}' afgerond.");
            }
        }

        private void CopyDocumentsBackAtRespons(SdkSession sdkSessionCompany, SafetyReport safetyReport,
            List<Document> documents)
        {
            if (documents == null)
            {
                return;
            }

            var documentsToLinkBack = documents.Where(x => string.IsNullOrEmpty(x.EXTERNALKEY)).ToList();

            if (!documentsToLinkBack.Any())
            {
                return;
            }

            foreach (var document in documentsToLinkBack)
            {
                AddDocumentCore(sdkSessionCompany, document, safetyReport);
            }
        }

        private void UpdateSafetyReports(List<AllSafetyReport> allSafetyReportsToCopy)
        {
            if (!allSafetyReportsToCopy.Any())
            {
                return;
            }

            var allSafetyReportsCopied = allSafetyReportsToCopy.Where(x => x.SuccesfulCopied).ToList();

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"{allSafetyReportsCopied.Count} veiligheidsmeldingen gevonden die succesvol zijn gekopieerd naar intercompanies.");

            if (!allSafetyReportsCopied.Any())
            {
                return;
            }

            var companies = allSafetyReportsCopied.Select(x => x.COMPANY).Distinct().ToList();

            if (!companies.Any())
            {
                return;
            }

            foreach (var company in companies)
            {
                var allSafetyReportsToUpdateThisCompany =
                    allSafetyReportsCopied.Where(x => x.COMPANY.Equals(company)).ToList();

                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                    $"{allSafetyReportsToUpdateThisCompany.Count} veiligheidsmeldingen gevonden in '{company}' die succesvol zijn gekopieerd.");

                var configuration = new SdkConfiguration()
                {
                    CompanyName = company,
                    UserName = _sdkSession.Configuration.UserName,
                    Password = _sdkSession.Configuration.Password,
                    PersistSession = _sdkSession.Configuration.PersistSession,
                    RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
                };

                var sdkSessionCompany = company.Equals("Anton Groep")
                    ? _sdkSession
                    : new SdkSession(configuration) { AutoLogin = false };

                try
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Inloggen in '{company}'...");

                    sdkSessionCompany.Login();

                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Ingelogd in '{company}'");
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error,
                        $"Inloggen mislukt in '{company}', oorzaak: {e}.");

                    continue;
                }

                var safetyReportIdsToUpdate =
                    allSafetyReportsToUpdateThisCompany.Select(x => x.SAFETYREPORTID ?? 0).ToList();
                var safetyReportsThisCompany = sdkSessionCompany
                    .Find<SafetyReport>(x => safetyReportIdsToUpdate.Contains(x.Id)).ToList();

                safetyReportsThisCompany.ForEach(x => x.COPIEDTOINTERCOMPANYRELATION = true);

                try
                {
                    sdkSessionCompany.UpdateBulk(safetyReportsThisCompany);
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error,
                        $"Bijwerken veiligheidsmeldingen in '{company}' is mislukt, oorzaak: {e}.");
                }

                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                    $"Bijwerken veiligheidsmeldingen in '{company}' afgerond.");
            }
        }


        private void ProcessCreatingSafetyReportsIntercompanies(List<AllSafetyReport> allSafetyReportsToCopy)
        {
            if (!allSafetyReportsToCopy.Any())
            {
                return;
            }

            LinkDocumentsToSafetyReports(allSafetyReportsToCopy);

            var intercompanies = allSafetyReportsToCopy.Select(x => x.INTERCOMPANYRELATION).Distinct().ToList();

            foreach (var intercompanyRelation in intercompanies)
            {
                var allSafetyReportsToCreateThisCompany =
                    allSafetyReportsToCopy.Where(x => x.INTERCOMPANYRELATION == intercompanyRelation).ToList();

                var correctCompanyName = GetChoiceText(intercompanyRelation);

                if (string.IsNullOrEmpty(correctCompanyName))
                {
                    continue;
                }

                _ridderLogger.Log(SchedulerTask, correctCompanyName, SchedulerLogLevel.Info,
                    $"{allSafetyReportsToCreateThisCompany.Count} veiligheidsmeldingen gevonden om aan te maken in '{correctCompanyName}'.");


                var configuration = new SdkConfiguration()
                {
                    CompanyName = correctCompanyName,
                    UserName = _sdkSession.Configuration.UserName,
                    Password = _sdkSession.Configuration.Password,
                    PersistSession = _sdkSession.Configuration.PersistSession,
                    RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
                };

                var sdkSessionCompany = intercompanyRelation == IntercompanyRelation.Anton_Groep
                    ? _sdkSession
                    : new SdkSession(configuration) { AutoLogin = false };

                try
                {
                    _ridderLogger.Log(SchedulerTask, correctCompanyName, SchedulerLogLevel.Info,
                        $"Inloggen in '{correctCompanyName}'...");

                    sdkSessionCompany.Login();

                    _ridderLogger.Log(SchedulerTask, correctCompanyName, SchedulerLogLevel.Info,
                        $"Ingelogd in '{correctCompanyName}'");
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, correctCompanyName, SchedulerLogLevel.Error,
                        $"Inloggen mislukt in '{correctCompanyName}', oorzaak: {e}.");

                    continue;
                }

                CreateSafetyReportsInIntercompany(sdkSessionCompany, allSafetyReportsToCreateThisCompany);

                _ridderLogger.Log(SchedulerTask, correctCompanyName, SchedulerLogLevel.Info,
                    $"Aanmaken veiligheidsmeldingen in '{correctCompanyName}' afgerond.");
            }
        }

        private void LinkDocumentsToSafetyReports(List<AllSafetyReport> allSafetyReports)
        {
            var companies = allSafetyReports.Select(x => x.COMPANY).Distinct().ToList();

            if (!companies.Any())
            {
                return;
            }

            foreach (var company in companies)
            {
                var allSafetyReportsThisCompany =
                    allSafetyReports.Where(x => x.COMPANY.Equals(company)).ToList();

                var configuration = new SdkConfiguration()
                {
                    CompanyName = company,
                    UserName = _sdkSession.Configuration.UserName,
                    Password = _sdkSession.Configuration.Password,
                    PersistSession = _sdkSession.Configuration.PersistSession,
                    RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
                };

                var sdkSessionCompany = company.Equals("Anton Groep")
                    ? _sdkSession
                    : new SdkSession(configuration) { AutoLogin = false };

                try
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Inloggen in '{company}'...");

                    sdkSessionCompany.Login();

                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                        $"Ingelogd in '{company}'");
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error,
                        $"Inloggen mislukt in '{company}', oorzaak: {e}.");

                    continue;
                }

                var safetyReportIds = allSafetyReportsThisCompany.Select(x => x.SAFETYREPORTID).ToList();
                var safetyReportDocuments = sdkSessionCompany.Find<SafetyReportDocument>(x => safetyReportIds.Contains(x.FK_VEILIGHEIDSMELDINGEN)).ToList();
                var allDocumentIds = safetyReportDocuments.Select(x => x.FK_DOCUMENT).ToList();
                var documents = sdkSessionCompany.Find<Document>(x => allDocumentIds.Contains(x.Id)).ToList();

                foreach (var allSafetyReport in allSafetyReportsThisCompany)
                {
                    var documentIdsThisSafetyReport = safetyReportDocuments
                        .Where(x => x.FK_VEILIGHEIDSMELDINGEN == allSafetyReport.SAFETYREPORTID)
                        .Select(x => x.FK_DOCUMENT).ToList();

                    if (!documentIdsThisSafetyReport.Any())
                    {
                        continue;
                    }

                    allSafetyReport.Documents = documents.Where(x => documentIdsThisSafetyReport.Contains(x.Id)).ToList();
                }
            }
        }

        private void CreateSafetyReportsInIntercompany(SdkSession sdkSessionCompany, List<AllSafetyReport> allSafetyReportsToCreateThisCompany)
        {
            var safetyReportsToCreate = new List<SafetyReport>();

            foreach (var allSafetyReport in allSafetyReportsToCreateThisCompany)
            {
                var bronMeldingId = GetOrCreateBronMelding(sdkSessionCompany, allSafetyReport.BRONMELDING);
                var soortMeldingId = GetOrCreateSoortMelding(sdkSessionCompany, allSafetyReport.SOORTMELDING);

                if (soortMeldingId == 0 || bronMeldingId == 0)
                {
                    allSafetyReport.SuccesfulCopied = false;
                    continue;
                }

                safetyReportsToCreate.Add(new SafetyReport()
                {
                    INCIDENTREPORTING = allSafetyReport.MELDING,
                    FK_BRONMELDINGEN = bronMeldingId,
                    DESCRIPTION = allSafetyReport.DESCRIPTION,
                    FK_MELDINGSOORTEN = soortMeldingId,
                    DATUMMELDING = allSafetyReport.DATUMMELDING,
                    MEMO = allSafetyReport.MEMO,
                    CAUSE = allSafetyReport.CAUSE,
                    MEASURES = allSafetyReport.MEASURES,
                    LONGTERMMEASURES = allSafetyReport.LONGTERMMEASURES,
                    CLASSIFICATION = Classification.N_,
                    SAFETYAWARENESS = Veiligheidsbewustzijn.N_,
                    INTERCOMPANYRELATION = IntercompanyRelation.N_v_t_,
                    EXTERNALREPORTER = $"{allSafetyReport.COMPANY} | {allSafetyReport.MELDER}",
                    EXTERNALKEY = $"{allSafetyReport.COMPANY}_{allSafetyReport.SAFETYREPORTID}",
                    ISOUTSTANDING = true,
                });

                allSafetyReport.SuccesfulCopied = true;
            }

            try
            {
                sdkSessionCompany.InsertBulk(safetyReportsToCreate);
            }
            catch (Exception e)
            {
                _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                    $"Aanmaken veiligheidsmeldingen in {sdkSessionCompany.Configuration.CompanyName} mislukt, oorzaak: {e}");

                allSafetyReportsToCreateThisCompany.ForEach(x => x.SuccesfulCopied = false);
            }

            CopyDocuments(sdkSessionCompany, safetyReportsToCreate, allSafetyReportsToCreateThisCompany);
        }

        private void CopyDocuments(SdkSession sdkSessionCompany, List<SafetyReport> createdSafetyReports,
            List<AllSafetyReport> allSafetyReportsToCreateThisCompany)
        {
            foreach (var createdSafetyReport in createdSafetyReports)
            {
                if (string.IsNullOrEmpty(createdSafetyReport.EXTERNALKEY))
                {
                    _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                        $"Oorspronkelijke veiligheidsmelding niet gevonden bij kopiëren documenten voor aangemaakte veiligheidsmelding met id {createdSafetyReport.Id}. External key is leeg.");

                    continue;
                }

                var externalKeyInfo = createdSafetyReport.EXTERNALKEY.Split('_');

                if (externalKeyInfo.Length < 2)
                {
                    _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                        $"Oorspronkelijke veiligheidsmelding niet gevonden bij kopiëren documenten voor aangemaakte veiligheidsmelding met id {createdSafetyReport.Id}. External key bevat geen 2 elementen.");

                    continue;
                }

                var originalCompany = externalKeyInfo[0];
                var originalSafetyReportId = externalKeyInfo[1];

                var originalSafetyReport = allSafetyReportsToCreateThisCompany
                    .FirstOrDefault(x => x.COMPANY.Equals(originalCompany) && x.SAFETYREPORTID.ToString().Equals(originalSafetyReportId));

                if (originalSafetyReport == null)
                {
                    _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                        $"Oorspronkelijke veiligheidsmelding niet gevonden bij kopiëren documenten voor aangemaakte veiligheidsmelding met id {createdSafetyReport.Id}");

                    continue;
                }

                if (originalSafetyReport.Documents == null)
                {
                    continue;
                }

                foreach (var originalDocument in originalSafetyReport.Documents)
                {
                    AddDocumentCore(sdkSessionCompany, originalDocument, createdSafetyReport);
                }
            }

            MarkCreatedDocuments(sdkSessionCompany, createdSafetyReports);
        }

        private void AddDocumentCore(SdkSession sdkSessionCompany, Document originalDocument, SafetyReport createdSafetyReport)
        {
            if (originalDocument.STORAGESYSTEM.Equals("Database"))
            {
                var fileName = $"{originalDocument.DESCRIPTION}{originalDocument.EXTENSION}";
                var totalFileName = Path.Combine(_tempDirectory, fileName);
                File.WriteAllBytes(totalFileName, originalDocument.DOCUMENTDATA);

                var result = sdkSessionCompany.Sdk.EventsAndActions.CRM.Actions.AddDocument(totalFileName,
                    originalDocument.REVISION,
                    false, "C_VEILIGHEIDSMELDINGEN", createdSafetyReport.Id, originalDocument.DESCRIPTION);

                if (result.HasError)
                {
                    _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                        $"Toevoegen document met document id {originalDocument.Id} mislukt, oorzaak: {result.GetResult()}");

                    return;
                }

                File.Delete(totalFileName);
            }
            else
            {
                var result = sdkSessionCompany.Sdk.EventsAndActions.CRM.Actions.AddDocument(originalDocument.DOCUMENTLOCATION,
                    originalDocument.REVISION, true, "C_VEILIGHEIDSMELDINGEN", createdSafetyReport.Id,
                    originalDocument.DESCRIPTION);

                if (result.HasError)
                {
                    _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                        $"Toevoegen document met document id {originalDocument.Id} mislukt, oorzaak: {result.GetResult()}");

                    return;
                }
            }
        }

        private void MarkCreatedDocuments(SdkSession sdkSessionCompany, List<SafetyReport> createdSafetyReports)
        {
            var createdSafetyReportIds = createdSafetyReports.Select(x => x.Id).ToList();

            var safetyReportDocuments = sdkSessionCompany
                .Find<SafetyReportDocument>(x => createdSafetyReportIds.Contains(x.FK_VEILIGHEIDSMELDINGEN)).ToList();

            var documentIds = safetyReportDocuments.Select(x => x.FK_DOCUMENT).ToList();
            var documents = sdkSessionCompany
                .Find<Document>(x => documentIds.Contains(x.Id)).ToList();

            documents.ForEach(x => x.EXTERNALKEY = "Imported from intercompany");

            sdkSessionCompany.UpdateBulk(documents);
        }

        private int? GetOrCreateBronMelding(SdkSession sdkSessionCompany, string bronmeldingOmschrijving)
        {
            if(string.IsNullOrEmpty(bronmeldingOmschrijving))
            {
                return null;
            }

            var bronMelding = sdkSessionCompany.Find<BronMelding>(x => x.DESCRIPTION.Equals(bronmeldingOmschrijving))
                .FirstOrDefault();

            if (bronMelding != null)
            {
                return bronMelding.Id;
            }

            var newBronMelding = new BronMelding() { DESCRIPTION = bronmeldingOmschrijving };

            try
            {
                sdkSessionCompany.Insert(newBronMelding);
            }
            catch (Exception e)
            {
                _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                    $"Aanmaken nieuwe bron melding mislukt, oorzaak: {e}");

                return 0;
            }

            return newBronMelding.Id;
        }

        private int GetOrCreateSoortMelding(SdkSession sdkSessionCompany, string soortmeldingOmschrijving)
        {
            var soortMelding = sdkSessionCompany.Find<SoortMelding>(x => x.DESCRIPTION.Equals(soortmeldingOmschrijving))
                .FirstOrDefault();

            if (soortMelding != null)
            {
                return soortMelding.Id;
            }

            var newSoortMelding = new SoortMelding() { DESCRIPTION = soortmeldingOmschrijving };

            try
            {
                sdkSessionCompany.Insert(newSoortMelding);
            }
            catch (Exception e)
            {
                _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                    $"Aanmaken nieuwe soort melding mislukt, oorzaak: {e}");

                return 0;
            }

            return newSoortMelding.Id;
        }

        private string GetChoiceText(IntercompanyRelation intercompanyRelation)
        {
            var choiceTypeId = new Guid("06f64738-17bb-4d09-814a-c458ed132b22");

            var choiceValue = _sdkSession
                .Find<ChoiceValue>(x => x.FK_CHOICETYPE == choiceTypeId && x.CHOICENUMBER == (int)intercompanyRelation)
                .FirstOrDefault();

            if (choiceValue == null)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                    $"Bepalen juiste keuze waarde id niet gelukt voor {intercompanyRelation}");

                return string.Empty;
            }

            return choiceValue.CHOICETEXT;
        }
        
        public string GetEnumDescription(Enum enumValue)
        {
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();
        }
    }
}
