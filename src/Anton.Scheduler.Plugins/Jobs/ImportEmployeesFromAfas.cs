﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.AfasModels;
using Anton.Scheduler.Plugins.FrameworkModels;
using Anton.Scheduler.Plugins.Models;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Scheduler.Jobs;

namespace Anton.Scheduler.Plugins.Jobs
{
    class ImportEmployeesFromAfas : RidderJob
    {
        public SdkSession _sdkSession { get; set; }
        private readonly RidderLogger _ridderLogger;
        private const string SchedulerTask = "ImportEmployeesFromAfas";

        public ImportEmployeesFromAfas(SdkSession sdkSession) : base(sdkSession)
        {
            _sdkSession = sdkSession;
            _ridderLogger = new RidderLogger(sdkSession);
        }

        protected override void ExecuteImpl(IJobExecutionContext context)
        {
            //DB
            //23-11-2021

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start plugin importeren AFAS werknemers");

            var sqlServerInstanceName = ConfigurationManager.AppSettings["SqlServerInstanceName"].ToString();
            var afasStartUrl = ConfigurationManager.AppSettings["AfasStartUrl"].ToString();
            var afasToken = ConfigurationManager.AppSettings["AfasToken"].ToString();
            var defaultWageGroup = ConfigurationManager.AppSettings["DefaultWageGroup"].ToString();

            var allCompanies = SqlHelper.DetermineRidderAdministrationsWithAfasCompanyName(_ridderLogger, sqlServerInstanceName);

            foreach (var company in allCompanies)
            {
                var ridderAdministrationName = company.Equals("OV & Loos Wapeningscentrale")
                    ? "Loos Wapeningscentrale"
                    : company;

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AfasCompanyFilter"].ToString())
                    && !ConfigurationManager.AppSettings["AfasCompanyFilter"].ToString().Equals(company))
                {
                    continue;
                }

                ProcessImportEmployeesPerCompany(SchedulerTask, ridderAdministrationName, afasStartUrl, afasToken, defaultWageGroup);
            }

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Importeren werknemers uit AFAS afgerond.");
        }

        private void ProcessImportEmployeesPerCompany(string schedulerTask, string company, string afasStartUrl, string afasToken, string defaultWageGroup)
        {
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"Start import werknemers {company}.");

            var sdkSessionCompany = GlobalFunctions.GetSdkSession(_ridderLogger, _sdkSession, schedulerTask, company);

            if (sdkSessionCompany == null)
            {
                return;
            }

            var crmSetting = sdkSessionCompany.Find<CrmSetting>().FirstOrDefault();

            if (string.IsNullOrEmpty(crmSetting?.AFASCOMPANYNAME))
            {
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, "Geen AFAS organisatie eenheid codes gevonden. Werknemer import stopt.");
                return;
            }

            var afasOrganisationUnitCodes = crmSetting?.AFASCOMPANYNAME;

            var employeesAfas = AfasHelper.GetEmployeesFromAfas(_ridderLogger, afasStartUrl, schedulerTask, company, afasOrganisationUnitCodes, afasToken);
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{employeesAfas.Count} werknemers gevonden in AFAS bij organisatie codes {afasOrganisationUnitCodes}.");

            var employeesRidder = sdkSessionCompany.Find<Employee>().ToList();

            RidderHelper.LinkEmployeeAvgAndPrivateData(sdkSessionCompany, employeesRidder);

            var employeesAfasToCreate = GetEmployeesToCreateInRidder(employeesAfas, employeesRidder);
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{employeesAfasToCreate.Count} werknemers gevonden om toe te voegen in Ridder iQ.");

            var employeesAfasToUpdateInRidder = GetEmployeesToUpdateInRidder(employeesAfas, employeesRidder);
            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{employeesAfasToUpdateInRidder.Count} werknemers gevonden om bij te werken in Ridder iQ.");

            RidderHelper.CreateEmployeesInRidder(_ridderLogger, schedulerTask, company, sdkSessionCompany, employeesAfasToCreate, defaultWageGroup, afasStartUrl, afasToken);
            RidderHelper.UpdateEmployeesInRidder(_ridderLogger, schedulerTask, company, sdkSessionCompany, employeesAfasToUpdateInRidder, afasStartUrl, afasToken);

            if(crmSetting.IMPORTTIMETABLESFROMAFAS)
            {
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"Start import roosters werknemers {company}.");

                //Reload de info omdat in UpdateEmployeesInRidder de info is bijgewerkt
                employeesRidder = sdkSessionCompany.Find<Employee>().ToList();

                var timeTablesAfas = AfasHelper.GetTimeTablesFromAfas(_ridderLogger, afasStartUrl, schedulerTask, company, afasOrganisationUnitCodes, afasToken);
                timeTablesAfas = DetermineActualTimeTablesAfas(timeTablesAfas);
                var timeTablesRidder = RidderHelper.GetActualTimeTableRidder(sdkSessionCompany, employeesRidder);

                var employeesToUpdateTimeTable = DetermineEmployeesToUpdateTimeTable(_ridderLogger, company, sdkSessionCompany, employeesAfas, employeesRidder, timeTablesAfas, timeTablesRidder);
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"{employeesToUpdateTimeTable.Count} werknemers gevonden om het rooster bij te werken in Ridder iQ.");

                RidderHelper.CreateNewTimeTables(_ridderLogger, SchedulerTask, company, sdkSessionCompany, employeesToUpdateTimeTable, timeTablesAfas);
            }


            _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info, $"Import werknemers {company} afgerond.");
        }

        private List<AfasTimeTable> DetermineActualTimeTablesAfas(List<AfasTimeTable> timeTablesAfas)
        {
            var result = new List<AfasTimeTable>();

            var employees = timeTablesAfas.Select(x => x.Medewerker).Distinct().ToList();

            foreach (var employee in employees)
            {
                var timeTablesAfasThisEmployee = timeTablesAfas.Where(x => x.Medewerker.Equals(employee)).ToList();
                
                if(!timeTablesAfasThisEmployee.Any())
                {
                    continue;
                }

                var timeTablesAfasThisEmployeeWithDateFilter = timeTablesAfasThisEmployee.Where(x => x.Begindatum_rooster.Date <= DateTime.Now.Date).ToList();

                if(!timeTablesAfasThisEmployeeWithDateFilter.Any())
                {
                    //Bij toekomstig in dienst tredende werknemers is er 1 rooster in de toekomst en pak die dan maar gewoon
                    var firstTimeTable = timeTablesAfasThisEmployee.OrderBy(x => x.Begindatum_rooster).First();
                    result.Add(firstTimeTable);
                    continue;
                }

                var actualTimeTable = timeTablesAfasThisEmployeeWithDateFilter.OrderByDescending(x => x.Begindatum_rooster).First();

                result.Add(actualTimeTable);
            }

            return result;
        }

        private List<Employee> DetermineEmployeesToUpdateTimeTable(RidderLogger ridderLogger, string company, SdkSession sdkSession, List<AfasEmployee> afasEmployees,
            List<Employee> employeesRidder, List<AfasTimeTable> timeTablesAfas, List<TimeTableOffSet> timeTablesRidder)
        {
            var result = new List<Employee>();

            var positionIdDirecteur = sdkSession.Find<Function>("DESCRIPTION = 'Directeur'").FirstOrDefault()?.Id ?? 0;

            var ridderEmployeesExistingInAfas = employeesRidder.Where(x => afasEmployees.Select(y => y.EmployeeId).Contains(x.CODE)).ToList();

            foreach (var employee in ridderEmployeesExistingInAfas)
            {
                if(employee.FK_POSITION == positionIdDirecteur)
                {
                    continue; //Import roosters overslaan bij directeurtjes
                }


                var afasTimeTable = timeTablesAfas.FirstOrDefault(x => x.Medewerker.Equals(employee.CODE));

                if(afasTimeTable == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Bij werknemer {employee.Person.RecordLabel} is geen AFAS rooster gevonden. Overnemen AFAS rooster niet mogelijk.");
                    continue;
                }

                var ridderTimeTable = timeTablesRidder.FirstOrDefault(x => x.FK_EMPLOYEE == employee.Id);

                if (ridderTimeTable == null)
                {
                    if (afasTimeTable.Type_rooster.Equals("Uren per dag, wisselende weken"))
                    {
                        ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Werknemer {employee.Person.RecordLabel} heeft geen rooster, maar heeft een afwisselend rooster in AFAS. Maak dit rooster handmatig aan in Ridder.");
                        continue;
                    }

                    //Er is nog geen rooster. Maak deze aan
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Debug, $"Er is nog geen rooster bij werknemer {employee.Person.RecordLabel}.");
                    result.Add(employee);
                    continue;
                }


                if (afasTimeTable.Type_rooster.Equals("Uren per week"))
                {
                    //Flex rooster moet gekoppeld worden
                    if(!ridderTimeTable.TimeTable.CODE.Equals("FLEX"))
                    {
                        ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Debug, $"Flex rooster moet gekoppeld worden bij werknemer {employee.Person.RecordLabel}.");
                        result.Add(employee);
                    }

                    continue;
                }

                if (afasTimeTable.Type_rooster.Equals("Uren per dag, wisselende weken"))
                {
                    //Bv Amarins en Niels Buiter hebben een 2-wekelijks rooster, check of er in Ridder een 14-daagse rooster is. 
                    if (ridderTimeTable.TimeTable.TimeTableWorkdays.Count != 14)
                    {
                        ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Werknemer {employee.Person.RecordLabel} heeft een wisselend rooster in AFAS. In Ridder moet een 14-daags rooster handmatig gekoppeld worden.");
                    }

                    continue;
                }

                if (afasTimeTable.Uren_Maandag == null && afasTimeTable.Uren_Dinsdag == null && afasTimeTable.Uren_Woensdag == null && afasTimeTable.Uren_Donderdag == null &&
                        afasTimeTable.Uren_Vrijdag == null && afasTimeTable.Uren_Zaterdag == null && afasTimeTable.Uren_Zondag == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Bij werknemer {employee.Person.RecordLabel} zijn in AFAS de werkdagen niet ingevuld. Overnemen van Ridder rooster niet mogelijk.");
                    continue;
                }


                var allWorkdayOverTimesThisEmployeeNotNorm = ridderTimeTable.TimeTable.TimeTableWorkdays.SelectMany(x => x.Workday.WorkdayOverTimes)
                    .Where(x => !x.TimeCode.CODE.Equals("NORM")).ToList();
                if (!employee.Cao.DESCRIPTION.Equals("Basis CAO") && allWorkdayOverTimesThisEmployeeNotNorm.Any(x => x.TimeCode.FK_CAO != employee.FK_CAO))
                {
                    //Rooster is van een afwijkende CAO. Wijzig rooster
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Debug, $"Rooster is van een afwijkende CAO. Wijzig rooster bij werknemer {employee.Person.RecordLabel}.");
                    result.Add(employee);
                    continue;
                }

                if(ridderTimeTable.TimeTable.TimeTableWorkdays.Count != 7)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Debug, $"Bij werknemer {employee.Person.RecordLabel} bestaat Ridder rooster niet uit 7 werkdagen, importeer AFAS rooster.");
                    result.Add(employee);
                    continue;
                }

                var urenMaWijkenAf = Math.Abs((afasTimeTable.Uren_Maandag ?? 0.0) - ridderTimeTable.TimeTable.TimeTableWorkdays[0].Workday.DefaultHoursInHours) > 0.01;
                var urenDiWijkenAf = Math.Abs((afasTimeTable.Uren_Dinsdag ?? 0.0) - ridderTimeTable.TimeTable.TimeTableWorkdays[1].Workday.DefaultHoursInHours) > 0.01;
                var urenWoWijkenAf = Math.Abs((afasTimeTable.Uren_Woensdag ?? 0.0) - ridderTimeTable.TimeTable.TimeTableWorkdays[2].Workday.DefaultHoursInHours) > 0.01;
                var urenDoWijkenAf = Math.Abs((afasTimeTable.Uren_Donderdag ?? 0.0) - ridderTimeTable.TimeTable.TimeTableWorkdays[3].Workday.DefaultHoursInHours) > 0.01;
                var urenVrijWijkenAf = Math.Abs((afasTimeTable.Uren_Vrijdag ?? 0.0) - ridderTimeTable.TimeTable.TimeTableWorkdays[4].Workday.DefaultHoursInHours) > 0.01;
                var urenZaWijkenAf = Math.Abs((afasTimeTable.Uren_Zaterdag ?? 0.0) - ridderTimeTable.TimeTable.TimeTableWorkdays[5].Workday.DefaultHoursInHours) > 0.01;
                var urenZonWijkenAf = Math.Abs((afasTimeTable.Uren_Zondag ?? 0.0) - ridderTimeTable.TimeTable.TimeTableWorkdays[6].Workday.DefaultHoursInHours) > 0.01;

                if(urenMaWijkenAf || urenDiWijkenAf || urenWoWijkenAf || urenDoWijkenAf || urenVrijWijkenAf || urenZaWijkenAf || urenZonWijkenAf)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Debug, $"Rooster wijkt af bij werknemer {employee.Person.RecordLabel}.");
                    result.Add(employee);
                }
            }

            return result;
        }

        private List<AfasEmployee> GetEmployeesToCreateInRidder(List<AfasEmployee> employeesAfas, List<Employee> employeesRidder)
        {
            return employeesAfas
                .Where(x => !employeesRidder.Select(y => y.CODE).Contains(x.EmployeeId)).ToList();
        }

        private List<AfasEmployee> GetEmployeesToUpdateInRidder(List<AfasEmployee> employeesAfas, List<Employee> employeesRidder)
        {
            var result = new List<AfasEmployee>();

            var employeesAfasExistingInRidder = employeesAfas
                .Where(x => employeesRidder.Select(y => y.CODE).Contains(x.EmployeeId)).ToList();

            foreach (var afasEmployee in employeesAfasExistingInRidder)
            {
                var employeeRidder = employeesRidder.First(x => x.CODE.Equals(afasEmployee.EmployeeId));

                if (!afasEmployee.AfasEmployeeEqualsRidderEmployee(employeeRidder))
                {
                    result.Add(afasEmployee);
                }
            }

            return result;
        }
    }
}
