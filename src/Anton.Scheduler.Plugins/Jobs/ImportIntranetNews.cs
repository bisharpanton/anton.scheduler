﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Serialization;
using ADODB;
using Anton.Scheduler.Plugins.AfasModels;
using Anton.Scheduler.Plugins.CreditSafeModels;
using Anton.Scheduler.Plugins.FrameworkModels;
using Anton.Scheduler.Plugins.IntranetModels;
using Anton.Scheduler.Plugins.Models;
using Anton.Scheduler.Plugins.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Scheduler.Jobs;
using static Anton.Scheduler.Plugins.IntranetModels.RootObjectGetIntranetNews;

namespace Anton.Scheduler.Plugins.Jobs
{
    class ImportIntranetNews : RidderJob
    {
        public SdkSession _sdkSession { get; set; }
        private readonly RidderLogger _ridderLogger;
        private const string DefaultCompany = "Anton Groep";
        private const string SchedulerTask = "ImportIntranetNews";
        private const int RelationIdAntonGroepInBisharp = 4842;
        private const string AntonGroepBisharpCompanyNumber = "000001";

        public ImportIntranetNews(SdkSession sdkSession) : base(sdkSession)
        {
            _sdkSession = sdkSession;
            _ridderLogger = new RidderLogger(sdkSession);
        }

        protected override void ExecuteImpl(IJobExecutionContext context)
        {
            //DB
            //11-04-2023
            //Dreamlab beheert nu de wordpress omgeving en heeft url klaargzet om wordpress berichten op te vragen.
            //Importeer deze naar de Bisharp Relation news feed

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start scheduler plugin ImportIntranetNews");

            /*
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Job is uitgezet");
            return;*/

            var getIntranetNewsItemsDaysBack = GetAppSettingDaysBack();

            if (!getIntranetNewsItemsDaysBack.HasValue)
            {
                return;
            }

            var errorMessage = string.Empty;
            var token = GetTokenFromDreamlabApi(ref errorMessage);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error, errorMessage);
                return;
            }


            var intranetItems = GetAllIntranetItems(getIntranetNewsItemsDaysBack.Value, token, ref errorMessage);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return;
            }

            var sdkSessionBisharp = LoginIntoBisharp();

            if (sdkSessionBisharp == null)
            {
                return;
            }

            var antonGroepNewsFeedItems = GetAntonGroepNewsFeedItems(sdkSessionBisharp);

            SetOlderNewsFeedItemsArchived(sdkSessionBisharp, antonGroepNewsFeedItems, getIntranetNewsItemsDaysBack.Value);

            var rebuildRssFeed = false;
            CompareNewsItemsAndUpdateTable(sdkSessionBisharp, intranetItems, 
                antonGroepNewsFeedItems.Where(x => x.Date.Date >= DateTime.Now.AddDays(getIntranetNewsItemsDaysBack.Value * -1).Date).ToList(), ref rebuildRssFeed);
            
            /* We gebruiken de OptiSigns Rss Feed viewer niet meer
            if(rebuildRssFeed)
            {
                TriggerRebuildRssFeedAtBisharpPortal();
            }
            */

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Einde scheduler plugin ImportIntranetNews");
        }

        private string GetTokenFromDreamlabApi(ref string errorMessage)
        {
            var url = "https://intranet.antongroep.nl/wp-json/api/v1/token";
            

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response;

                var bodyFormUrlEncoded = new List<KeyValuePair<string, string>>();
                bodyFormUrlEncoded.Add(new KeyValuePair<string, string>("username", ConfigurationManager.AppSettings["IntranetApiUsername"].ToString()));
                bodyFormUrlEncoded.Add(new KeyValuePair<string, string>("password", ConfigurationManager.AppSettings["IntranetApiPassword"].ToString()));

                try
                {
                    var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(bodyFormUrlEncoded) };
                    response = client.SendAsync(req).GetAwaiter().GetResult();
                }
                catch (Exception e)
                {
                    errorMessage = $"Dreamlab API niet bereikbaar, error: {e}";
                    return string.Empty;
                }

                if (!response.IsSuccessStatusCode)
                {
                    var message = response.Content.ReadAsStringAsync().Result;

                    errorMessage = $"Verbinden Dreamlab API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} - {message} (URL: {url})";

                    return null;
                }

                try
                {
                    var json = response.Content.ReadAsStringAsync().Result;

                    dynamic result = JObject.Parse(json);

                    return result.jwt_token;
                }
                catch (Exception e)
                {
                    errorMessage = $"Converteren naar token is mislukt, error: {e}";
                    return string.Empty;
                }
            }
        }

        /*
        private void TriggerRebuildRssFeedAtBisharpPortal()
        {
            var urlBisharpPortal = ConfigurationManager.AppSettings["UrlBisharpPortal"].ToString();

            var url = new Uri($"{urlBisharpPortal}/api/TriggerBuildUpRssFeed/{AntonGroepBisharpCompanyNumber}");

            HttpClient client = new HttpClient();

            var response = client.PostAsync(url, null).Result;

            if (!response.IsSuccessStatusCode)
            {
                _ridderLogger.Log(SchedulerTask, DefaultCompany, SchedulerLogLevel.Error,
                    $"Triggeren Rebuild RSS feed is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}");

                return;
            }
        }*/


        private void CompareNewsItemsAndUpdateTable(SdkSession sdkSessionBisharp, List<BisharpRelationNewsFeed> intranetNewsItems, List<BisharpRelationNewsFeed> antonGroepNewsFeedItems, 
            ref bool rebuildRssFeed)
        {
            var recordsToDelete = antonGroepNewsFeedItems
                .Where(x => !intranetNewsItems.Any(y => y.Date.Date == x.Date.Date
                                                         && y.Title.Equals(x.Title)
                                                         && y.Content.Equals(x.Content)
                                                         && y.ImageUrl.Equals(x.ImageUrl)
                                                         && y.ExternalNewsUrl.Equals(x.ExternalNewsUrl)
                                                         && y.Company.Equals(x.Company)
                                                         && y.AntonIntranetSource.Equals(x.AntonIntranetSource))).ToList();

            var recordsToInsert = intranetNewsItems
                .Where(x => !antonGroepNewsFeedItems.Any(y => y.Date.Date == x.Date.Date
                                                         && y.Title.Equals(x.Title)
                                                         && y.Content.Equals(x.Content)
                                                         && y.ImageUrl.Equals(x.ImageUrl)
                                                         && y.ExternalNewsUrl.Equals(x.ExternalNewsUrl)
                                                         && y.Company.Equals(x.Company)
                                                         && y.AntonIntranetSource.Equals(x.AntonIntranetSource))).ToList();

            //rebuildRssFeed = recordsToDelete.Any() || recordsToInsert.Any();

            if(!recordsToDelete.Any() && !recordsToInsert.Any())
            {
                return;
            }

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"{recordsToDelete.Count} records gevonden om te verwijderen.");
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"{recordsToInsert.Count} records gevonden om toe te voegen.");


            DeleteRecords(sdkSessionBisharp, recordsToDelete);
            InsertRecords(sdkSessionBisharp, recordsToInsert);
        }

        private void InsertRecords(SdkSession sdkSessionBisharp, List<BisharpRelationNewsFeed> recordsToInsert)
        {
            if (!recordsToInsert.Any())
            {
                return;
            }

            var rsNewsFeedItems = sdkSessionBisharp.GetRecordsetColumns("U_BISHARPRELATIONNEWSFEED", "", "");
            rsNewsFeedItems.UseDataChanges = false;
            rsNewsFeedItems.UpdateWhenMoveRecord = false;

            foreach (var intranetNewsItemToInsert in recordsToInsert)
            {
                rsNewsFeedItems.AddNew();

                rsNewsFeedItems.SetFieldValue("FK_RELATION", RelationIdAntonGroepInBisharp);
                rsNewsFeedItems.SetFieldValue("DATE", intranetNewsItemToInsert.Date);
                rsNewsFeedItems.SetFieldValue("TITLE", intranetNewsItemToInsert.Title);
                rsNewsFeedItems.SetFieldValue("IMAGEURL", intranetNewsItemToInsert.ImageUrl);
                rsNewsFeedItems.SetFieldValue("EXTERNALNEWSURL", intranetNewsItemToInsert.ExternalNewsUrl);
                rsNewsFeedItems.SetFieldValue("STATE", (int)NewsStatus.Published);
                rsNewsFeedItems.SetFieldValue("CONTENT", intranetNewsItemToInsert.Content);

                rsNewsFeedItems.SetFieldValue("ANTONINTRANETSOURCE", intranetNewsItemToInsert.AntonIntranetSource);
                rsNewsFeedItems.SetFieldValue("ANTONCOMPANY", intranetNewsItemToInsert.Company);
            }

            var updateResult = rsNewsFeedItems.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Bisharp", SchedulerLogLevel.Error, $"Aanmaken nieuwe nieuws berichten mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }
        }

        private void DeleteRecords(SdkSession sdkSessionBisharp, List<BisharpRelationNewsFeed> recordsToDelete)
        {
            if(!recordsToDelete.Any())
            {
                return;
            }

            var recordIds = recordsToDelete.Select(x => x.Id).ToList();

            var rsNewsFeedItems = sdkSessionBisharp.GetRecordsetColumns("U_BISHARPRELATIONNEWSFEED", "",
                $"PK_U_BISHARPRELATIONNEWSFEED IN ({string.Join(",", recordIds)})");
            rsNewsFeedItems.UseDataChanges = false;
            rsNewsFeedItems.UpdateWhenMoveRecord = false;

            rsNewsFeedItems.MoveFirst();
            while (!rsNewsFeedItems.EOF)
            {
                rsNewsFeedItems.Delete();
                rsNewsFeedItems.MoveNext();
            }

            rsNewsFeedItems.MoveFirst();
            var updateResult = rsNewsFeedItems.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Bisharp", SchedulerLogLevel.Error, $"Verwijderen RSS feed nieuws berichten mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }
        }

        private void SetOlderNewsFeedItemsArchived(SdkSession sdkSessionBisharp, List<BisharpRelationNewsFeed> antonGroepNewsFeedItems, int getIntranetNewsItemsDaysBack)
        {
            var oldNewsItems = antonGroepNewsFeedItems
                .Where(x => x.Date.Date < DateTime.Now.AddDays(getIntranetNewsItemsDaysBack * -1).Date && x.NewsStatus == NewsStatus.Published)
                .ToList();

            if(!oldNewsItems.Any())
            {
                return;
            }

            var oldNewsItemIds = oldNewsItems.Select(x => x.Id).ToList();

            var rsNewsFeedItems = sdkSessionBisharp.GetRecordsetColumns("U_BISHARPRELATIONNEWSFEED", "",
                $"PK_U_BISHARPRELATIONNEWSFEED IN ({string.Join(",", oldNewsItemIds)})");
            rsNewsFeedItems.UseDataChanges = false;
            rsNewsFeedItems.UpdateWhenMoveRecord = false;

            rsNewsFeedItems.MoveFirst();
            while(!rsNewsFeedItems.EOF)
            {
                rsNewsFeedItems.SetFieldValue("STATE", (int)NewsStatus.Archive);
                rsNewsFeedItems.MoveNext();
            }

            rsNewsFeedItems.MoveFirst();
            var updateResult = rsNewsFeedItems.Update2();

            if(updateResult.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, "Bisharp", SchedulerLogLevel.Error, $"Archiveren oude RSS feed nieuws berichten mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }
        }

        private List<BisharpRelationNewsFeed> GetAntonGroepNewsFeedItems(SdkSession sdkSessionBisharp)
        {
            return sdkSessionBisharp.GetRecordsetColumns("U_BISHARPRELATIONNEWSFEED", "", 
                $"FK_RELATION = {RelationIdAntonGroepInBisharp} AND ANTONINTRANETSOURCE <> ''")
                .DataTable.AsEnumerable().Select(x => new BisharpRelationNewsFeed()
                {
                    Id = (int)x["PK_U_BISHARPRELATIONNEWSFEED"],
                    RelationId = (int)x["FK_RELATION"],
                    Date = (DateTime)x["DATE"],
                    Title = x["TITLE"].ToString(),
                    Content = (string)x["PLAINTEXT_CONTENT"],
                    ImageUrl = (string)x["IMAGEURL"],
                    NewsStatus = (NewsStatus)x["STATE"],
                    ExternalNewsUrl = (string)x["EXTERNALNEWSURL"],
                    ExternalId = (int)x["EXTERNALID"],
                    AntonIntranetSource = (string)x["ANTONINTRANETSOURCE"],
                    Company = (string)x["ANTONCOMPANY"],
                }).ToList();
        }

        private SdkSession LoginIntoBisharp()
        {
            var configuration = new SdkConfiguration()
            {
                CompanyName = "Bisharp",
                UserName = _sdkSession.Configuration.UserName,
                Password = _sdkSession.Configuration.Password,
                PersistSession = _sdkSession.Configuration.PersistSession,
                RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
            };

            var sdkSessionBisharp = new SdkSession(configuration) { AutoLogin = false };

            try
            {
                _ridderLogger.Log(SchedulerTask, "Bisharp", SchedulerLogLevel.Info, $"Inloggen in Bisharp...");

                sdkSessionBisharp.Login();

                _ridderLogger.Log(SchedulerTask, "Bisharp", SchedulerLogLevel.Info, $"Ingelogd in Bisharp");
            }
            catch (Exception e)
            {
                _ridderLogger.Log(SchedulerTask, "Bisharp", SchedulerLogLevel.Error, $"Inloggen in Bisharp mislukt, oorzaak: {e}.");
                return null;
            }

            return sdkSessionBisharp;
        }
        
        private List<BisharpRelationNewsFeed> GetAllIntranetItems(int getIntranetNewsItemsDaysBack, string token, ref string errorMessage)
        {
            var result = new List<BisharpRelationNewsFeed>();

            result.AddRange(GetIntranetNewItems(getIntranetNewsItemsDaysBack, token, ref errorMessage));
            //result.AddRange(GetIntranetProjectItems(UrlIntranetProjects, getIntranetNewsItemsDaysBack));

            return result;
        }

        private List<BisharpRelationNewsFeed> GetIntranetNewItems(int getIntranetNewsItemsDaysBack, string token, ref string errorMessage)
        {
            var url = "https://intranet.antongroep.nl/wp-json/wp/v2/posts";
            var apiResults = new List<Post>();
            var perPage = 50;
            var counter = 0;
            var allNewsItemsCollected = false;
            //var fields = "id,date,modified,excerpt,title,link,categories,company";  //Nu we de url link uit de embed info halen, halen we maar gewoon alles op
            var dateFrom = DateTime.Now.Date.AddDays(getIntranetNewsItemsDaysBack * -1);
            var sDateFrom = $"{dateFrom:yyyy-MM-dd}T00:00:00";

            while (!allNewsItemsCollected)
            {
                var skip = counter * perPage;

                var urlWithParameters = new Uri($"{url}?_embed&offset={skip}&per_page={perPage}&after={sDateFrom}");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = client.GetAsync(urlWithParameters).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                {
                    errorMessage = $"Verbinden Dreamlab API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}";
                    return new List<BisharpRelationNewsFeed>();
                }

                try
                {
                    var json = response.Content.ReadAsStringAsync().Result;
                    var deserialized = JsonConvert.DeserializeObject<List<object>>(json);

                    foreach (var item in deserialized)
                    {
                        if (item is JObject obj)
                        {
                            var post = obj.ToObject<Post>();

                            if (post.Id != 0 && !string.IsNullOrEmpty(post.Title?.Rendered))
                            {
                                apiResults.Add(post);
                            }
                        }
                    }

                    if (deserialized.Count < perPage)
                    {
                        allNewsItemsCollected = true;
                    }
                }
                catch (Exception e)
                {
                    errorMessage = $"Converteren API response naar RootObject mislukt, oorzaak: {e}";
                    return new List<BisharpRelationNewsFeed>();
                }

                counter++;
            }

            var result = new List<BisharpRelationNewsFeed>();
            
            foreach (var apiResult in apiResults)
            {
                result.Add(new BisharpRelationNewsFeed()
                {
                    Date = apiResult.Date,
                    Title = apiResult.Title?.Rendered.Replace("&amp;", "&") ?? string.Empty,
                    ImageUrl = apiResult.EmbedInfo?.FeaturedMedia.FirstOrDefault()?.source_url ?? string.Empty,
                    ExternalNewsUrl = apiResult.Link,
                    Content = apiResult.Excerpt?.Rendered ?? string.Empty,
                    AntonIntranetSource = "Nieuws",
                    Company = apiResult.Company.FirstOrDefault() ?? string.Empty,
                    ExternalId = apiResult.Id,
                });
            }

            return result;
        }

        /*
        private List<BisharpRelationNewsFeed> GetIntranetProjectItems(string url, int getIntranetNewsItemsDaysBack)
        {
            using (var client = new HttpClient())
            {
                Stream content;
                try
                {
                    content = client.GetStreamAsync(url).Result;
                }
                catch (Exception e)
                {
                    _ridderLogger.Log(SchedulerTask, DefaultCompany, SchedulerLogLevel.Error,
                        $"Ophalen intranet nieuws feed is mislukt, url '{url}' niet bereikbaar");

                    return null;
                }

                XmlSerializer serializer = new XmlSerializer(typeof(XmlIntranetProjects));
                var myDocument = (XmlIntranetProjects)serializer.Deserialize(content);

                foreach (var item in myDocument.IntranetProjectsItems)
                {
                    if (DateTime.TryParse(item.Date, out DateTime date))
                    {
                        item.dDate = date;
                    }
                }

                var intranetNewsItems = myDocument.IntranetProjectsItems
                    .Where(x => x.Publicerennaarinterneomgevingen.Contains("narrowcasting") && x.dDate.HasValue && x.dDate.Value.Date >= DateTime.Now.AddDays(getIntranetNewsItemsDaysBack * -1).Date)
                    .ToList();

                return intranetNewsItems.Select(x => new BisharpRelationNewsFeed()
                {
                    Date = x.dDate.Value,
                    Title = x.Titel,
                    ImageUrl = x.Hoofdafbeelding,
                    ExternalNewsUrl = x.Permalink,
                    Content = x.Projectomschrijvingopnarrowcasting,
                    AntonIntranetSource = "Projecten",
                }).ToList();
            }
        }*/

        private int? GetAppSettingDaysBack()
        {
            var sDays = ConfigurationManager.AppSettings["GetIntranetNewsItemsDaysBack"].ToString();

            if(!int.TryParse(sDays, out int iDays))
            {
                _ridderLogger.Log(SchedulerTask, DefaultCompany, SchedulerLogLevel.Error, $"Converteren App settings GetIntranetNewsItemsDaysBack mislukt.");
                return null;
            }

            return iDays;
        }
        
    }
}
