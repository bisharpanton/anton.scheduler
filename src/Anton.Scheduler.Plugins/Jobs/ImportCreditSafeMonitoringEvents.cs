﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.CreditSafeModels;
using Anton.Scheduler.Plugins.FrameworkModels;
using Anton.Scheduler.Plugins.Models;
using Anton.Scheduler.Plugins.TokenModels;
using Anton.Scheduler.Plugins.ViewModels;
using Newtonsoft.Json;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Scheduler.Jobs;
using Ridder.Scheduler.Logging;

namespace Anton.Scheduler.Plugins.Jobs
{
    class ImportCreditSafeMonitoringEvents : RidderJob
    {
        public SdkSession _sdkSession { get; set; }
        private readonly RidderLogger  _ridderLogger;
        private const string SchedulerTask = "ImportCreditSafeMonitoringEvents";

        public ImportCreditSafeMonitoringEvents(SdkSession sdkSession) : base(sdkSession)
        {
            _sdkSession = sdkSession;
            _ridderLogger = new RidderLogger(sdkSession);
        }

        protected override void ExecuteImpl(IJobExecutionContext context)
        {
            //DB
            //26-5-2020
            //Er gebeuren voornamelijk 2 dingen:
            //1. Haal de relaties op waar we momenteel zaken mee doen en vergelijk deze met de huidige CreditSafe portfolio
            //2. Haal de wijzigingen van de afgelopen 24 uur op en importeer deze in Ridder
            //3. Zoek relaties op die vandaag geüpdatet zijn, controleer of deze relaties ook bestaan in andere bedrijven en update die dan

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start scheduler plugin Credit Safe Monitoring events");

            var portfolioId = _sdkSession.Find<CrmSetting>().First().CREDITSAFEPORTFOLIOID ?? 0;

            if (portfolioId == 0)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error, "Geen portfolio id gevonden in CRM instellingen.");

                return;
            }

            var token = new Token(SdkSession).GetOrCreateNewToken();

            if (!token.Result)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error, token.Message);
                return;
            }

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.Token);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            if (!CompareRidderRelationsAndCreditSafePortfolio(client, portfolioId).Result)
            {
                return;
            }

            ImportMonitoringEvents(client, portfolioId);
            
            CopyUpdatedCreditSafeInfoToOtherCompanies();

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Einde scheduler plugin Credit Safe Monitoring events");
        }

        private void CopyUpdatedCreditSafeInfoToOtherCompanies()
        {
            //Als een relatie niet in de portfolio zit en er wordt een offerte op deze relatie uitgebracht, dan wordt de CreditSafe info 
            //opnieuw opgehaald. Deze bijgewerkte info kopiëren we hier naar andere bedrijven.

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start kopiëren updated CreditSafe info naar andere bedrijven.");

            var allRelationsUpdatedToday = _sdkSession.Find<AllRelation>($"LASTCREDITSAFEREQUEST > '{DateTime.Now.AddDays(-1):yyyyMMdd hh:mm:ss}'").ToList();

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"{allRelationsUpdatedToday.Count} relaties gevonden waarbij vandaag de CreditSafe info is bijgewerkt.");

            if (!allRelationsUpdatedToday.Any())
            {
                return;
            }

            var allRelationIds = allRelationsUpdatedToday.Select(x => x.Id).ToList();
            var allCreditSafeIds = allRelationsUpdatedToday.Select(x => x.CREDITSAFEID).Distinct().ToList();

            var allRelationsToUpdateInOtherCompanies =
                _sdkSession.Find<AllRelation>(x =>
                    !allRelationIds.Contains(x.Id) && allCreditSafeIds.Contains(x.CREDITSAFEID)).ToList();

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{allRelationsToUpdateInOtherCompanies.Count} relaties gevonden in de andere bedrijven om bij te werken.");

            if (!allRelationsToUpdateInOtherCompanies.Any())
            {
                return;
            }

            UpdateRelationsInRidderIQ(allRelationsUpdatedToday, allRelationsToUpdateInOtherCompanies);
        }

        private void UpdateRelationsInRidderIQ(List<AllRelation> allRelationsUpdatedToday,
            List<AllRelation> allRelationsToUpdateInOtherCompanies)
        {
            var allRelationsToUpdateAIO = allRelationsToUpdateInOtherCompanies
                .Where(x => string.Equals(x.ABBREVIATIONCOMPANY, "AIO")).ToList();

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{allRelationsToUpdateAIO.Count} relaties gevonden in Anton Groep om bij te werken.");

            UpdateRelationsInRidderIqCore(_sdkSession, allRelationsUpdatedToday, allRelationsToUpdateAIO);
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{allRelationsToUpdateAIO.Count} relaties in Anton Groep bijgewerkt in Ridder iQ.");


            
            var companies = allRelationsToUpdateInOtherCompanies.Where(x => !x.ABBREVIATIONCOMPANY.Equals("AIO"))
                .Select(x => x.COMPANY).Distinct().ToList();

            if(!companies.Any())
            {
                return;
            }

            foreach (var company in companies)
            {
                var allRelationsToUpdateThisCompany = allRelationsToUpdateInOtherCompanies
                    .Where(x => string.Equals(x.COMPANY, company)).ToList();
                
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                    $"{allRelationsToUpdateThisCompany.Count} relaties gevonden in {company} om bij te werken.");


                var configuration = new SdkConfiguration()
                {
                    CompanyName = company,
                    UserName = _sdkSession.Configuration.UserName,
                    Password = _sdkSession.Configuration.Password,
                    PersistSession = _sdkSession.Configuration.PersistSession,
                    RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
                };

                var sdkSessionCompany = new SdkSession(configuration);

                UpdateRelationsInRidderIqCore(sdkSessionCompany, allRelationsUpdatedToday, allRelationsToUpdateThisCompany);
                _ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Info,
                    $"{allRelationsToUpdateThisCompany.Count} relaties in {company} bijgewerkt.");


                sdkSessionCompany.Logout();
            }
        }

        private void UpdateRelationsInRidderIqCore(SdkSession sdkSession, List<AllRelation> allRelationsUpdatedToday, List<AllRelation> allRelationsToUpdate)
        {
            if (!allRelationsToUpdate.Any())
            {
                return;
            }

            var rsRelation = sdkSession.GetRecordsetColumns("R_RELATION", "",
                $"PK_R_RELATION IN ({string.Join(",", allRelationsToUpdate.Select(x => x.RELATIONID.Value))})");
            rsRelation.UseDataChanges = true;
            rsRelation.UpdateWhenMoveRecord = false;

            rsRelation.MoveFirst();

            while (!rsRelation.EOF)
            {
                var relationOtherCompany = allRelationsUpdatedToday.First(x =>
                    x.CREDITSAFEID.Equals(rsRelation.GetField("CREDITSAFEID").Value));

                var createdAddress = CreateNewAddress(sdkSession, relationOtherCompany.STREET, relationOtherCompany.HOUSENUMBER2,
                    relationOtherCompany.ADDITIONHOUSENUMBER, relationOtherCompany.ZIPCODE, relationOtherCompany.CITY,
                    relationOtherCompany.COUNTRYCODE);

                rsRelation.Fields["FK_VISITINGADDRESS"].Value = createdAddress == 0
                    ? (object)DBNull.Value
                    : createdAddress;

                if (relationOtherCompany.COUNTRYCODE != "NL")
                {
                    //Bij Anton wordt de BTW-bedrijfsgroep leeg gemaakt indien het bezoekadres buiten NL is. Zet hem dummy op 'EU'.
                    var vatCompanyGroupEu = GetVatCompanyGroup(sdkSession, "EU");

                    if (vatCompanyGroupEu == 0)
                    {
                        rsRelation.MoveNext();
                        continue;
                    }

                    rsRelation.Fields["FK_VATCOMPANYGROUP"].Value = vatCompanyGroupEu;
                }

                if (!string.IsNullOrEmpty(relationOtherCompany.POSTADDRESS_COUNTRYCODE))
                {
                    var createdAddress2 = CreateNewAddress(sdkSession, relationOtherCompany.POSTADDRESS_STREET,
                        relationOtherCompany.POSTADDRESS_HOUSENUMBER2,
                        relationOtherCompany.POSTADDRESS_ADDITIONHOUSENUMBER, relationOtherCompany.POSTADDRESS_ZIPCODE,
                        relationOtherCompany.POSTADDRESS_CITY, relationOtherCompany.POSTADDRESS_COUNTRYCODE);

                    rsRelation.Fields["FK_POSTALADDRESS"].Value = createdAddress2 == 0
                        ? (object)DBNull.Value
                        : createdAddress2;
                }

                rsRelation.Fields["SALESCREDITLIMIT"].Value = relationOtherCompany.SALESCREDITLIMIT;
                rsRelation.Fields["SALESCREDITLIMITSCORE"].Value = relationOtherCompany.SALESCREDITLIMITSCORE;

                if (!string.IsNullOrEmpty(relationOtherCompany.LEGALFORMDESCRIPTION))
                {
                    rsRelation.Fields["FK_LEGALFORM"].Value = GetOrCreate(sdkSession, "R_LEGALFORM", "DESCRIPTION",
                        relationOtherCompany.LEGALFORMDESCRIPTION);
                }

                rsRelation.Fields["NUMBEROFEMPLOYEES"].Value = relationOtherCompany.NUMBEROFEMPLOYEES;

                if (!string.IsNullOrEmpty(relationOtherCompany.RELATIONFINANCIALSTATE))
                {
                    rsRelation.Fields["FK_RELATIONFINANCIALSTATE"].Value = GetOrCreate(sdkSession, "R_RELATIONFINANCIALSTATE",
                        "DESCRIPTION",
                        relationOtherCompany.RELATIONFINANCIALSTATE);
                }

                var sbiMainActivity =
                    GetOrCreateSbiMainActivity(sdkSession, relationOtherCompany.SBICODE, relationOtherCompany.SBIDESCRIPTION);
                rsRelation.Fields["FK_SBIMAINACTIVITY"].Value = sbiMainActivity == 0
                    ? (object)DBNull.Value
                    : sbiMainActivity;


                rsRelation.MoveNext();
            }

            var updateResult = rsRelation.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, sdkSession.Configuration.CompanyName, SchedulerLogLevel.Error,
                    $"Het aanmaken van een nieuwe relatie bij het kopiëren vanuit een ander bedrijf is mislukt, oorzaak: {updateResult.First(x => x.HasError).Messages.First().Message}");

                return;
            }
        }

        private int GetOrCreate(SdkSession sdkSession, string tableName, string columnName, string companyDescription)
        {
            var rs = sdkSession.GetRecordsetColumns(tableName, "", "");

            if (rs.DataTable.AsEnumerable().Any(x => x.Field<string>(columnName).Equals(companyDescription, StringComparison.OrdinalIgnoreCase)))
            {
                return rs.DataTable.AsEnumerable()
                    .First(x => x.Field<string>(columnName).Equals(companyDescription, StringComparison.OrdinalIgnoreCase))
                    .Field<int>($"PK_{tableName}");
            }
            else
            {
                var newCode = $"{rs.RecordCount + 1:00}"; 
            
                rs.AddNew();
                rs.Fields["CODE"].Value = newCode;
                rs.Fields["DESCRIPTION"].Value = companyDescription;
                rs.Update();

                return (int)rs.Fields[$"PK_{tableName}"].Value;
            }
        }

        private int GetOrCreateSbiMainActivity(SdkSession sdkSession, string sbiCode, string sbiDescription)
        {
            if (string.IsNullOrEmpty(sbiCode))
            {
                return 0;
            }

            var rsSbiActivities = sdkSession.GetRecordsetColumns("C_SBIACTIVITIES", "",
                $"CODE = '{sbiCode}'");

            if (rsSbiActivities.RecordCount > 0)
            {
                rsSbiActivities.MoveFirst();
            }
            else
            {
                rsSbiActivities.AddNew();
                rsSbiActivities.Fields["CODE"].Value = sbiCode;
                rsSbiActivities.Fields["DESCRIPTION"].Value = sbiDescription;
                rsSbiActivities.Update();
            }

            return (int)rsSbiActivities.Fields["PK_C_SBIACTIVITIES"].Value;
        }

        private int CreateNewAddress(SdkSession sdkSession, string street, string houseNumber2, string additionHouseNumber, string zipCode, string city, string countryCode)
        {
            if (string.IsNullOrEmpty(countryCode))
            {
                return 0;
            }

            var rsAddress = sdkSession.GetRecordsetColumns("R_ADDRESS", "",
                "PK_R_ADDRESS = -1");
            rsAddress.AddNew();
            rsAddress.Fields["STREET"].Value = street;
            rsAddress.Fields["HOUSENUMBER2"].Value = houseNumber2;
            rsAddress.Fields["ADDITIONHOUSENUMBER"].Value = additionHouseNumber;
            rsAddress.Fields["ZIPCODE"].Value = zipCode;
            rsAddress.Fields["CITY"].Value = city;
            rsAddress.Fields["FK_COUNTRY"].Value = GetOrCreateCountry(sdkSession, countryCode);
            rsAddress.Update();

            return (int)rsAddress.Fields["PK_R_ADDRESS"].Value;
        }

        private int GetVatCompanyGroup(SdkSession sdkSession, string code)
        {
            var rsVatCompanyGroup = sdkSession.GetRecordsetColumns("R_VATCOMPANYGROUP", "", 
                $"CODE = '{code}'");

            if (rsVatCompanyGroup.RecordCount > 0)
            {
                rsVatCompanyGroup.MoveFirst();
                return (int)rsVatCompanyGroup.Fields["PK_R_VATCOMPANYGROUP"].Value;
            }

            _ridderLogger.Log(SchedulerTask, sdkSession.Configuration.CompanyName, SchedulerLogLevel.Error,
                "Aanmaken relatie mislukt. Relatie ligt buiten NL, maar er is geen BTW-bedrijfsgroep 'EU' gevonden.");


            return 0;
        }
        private int GetOrCreateCountry(SdkSession sdkSession, string countryCode)
        {
            var rsCountry = sdkSession.GetRecordsetColumns("R_COUNTRY", "", 
                $"CODE = '{countryCode}'");

            if (rsCountry.RecordCount > 0)
            {
                rsCountry.MoveFirst();
                return (int)rsCountry.Fields["PK_R_COUNTRY"].Value;
            }
            else
            {
                rsCountry.AddNew();
                rsCountry.Fields["CODE"].Value = countryCode;
                rsCountry.Fields["NAME"].Value = countryCode;
                rsCountry.Update();

                return (int)rsCountry.Fields["PK_R_COUNTRY"].Value;
            }
        }

        private void ImportMonitoringEvents(HttpClient client, int portfolioId)
        {
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start importeren monitoring events.");


            var creditSafeMonitoringEvents = GetMonitoringEventsFromPortfolio(client, portfolioId);

            if (creditSafeMonitoringEvents.Status == TaskStatus.Faulted || creditSafeMonitoringEvents.Result == null)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                    $"Ophalen CreditSafe Monitoring events mislukt; oorzaak:{creditSafeMonitoringEvents.Exception}");

                return;
            }

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{creditSafeMonitoringEvents.Result.Count} nieuwe monitoring events gevonden op CreditSafe");

            if (!creditSafeMonitoringEvents.Result.Any())
            {
                return;
            }

            var relationsToUpdate = LinkMonitoringEventsToRelationsInRidderIQ(creditSafeMonitoringEvents.Result);

            if (!relationsToUpdate.Any())
            {
                return;
            }

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start aanmaken monitoring events in Ridder iQ.");

            CreateMonitoringEventsInRidderIQ(relationsToUpdate);

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                "Importeren monitoring events afgerond.");
        }

        private List<LinkCompanyMonitoringEvent> LinkMonitoringEventsToRelationsInRidderIQ(
            List<CreditSafeMonitoringEvent> creditSafeMonitoringEvents)
        {
            var result = new List<LinkCompanyMonitoringEvent>();

            var allRelations = SdkSession.Find<AllRelation>().ToList();

            foreach (var creditSafeMonitoringEvent in creditSafeMonitoringEvents)
            {
                var ridderRelations = allRelations.Where(x =>
                    string.Equals(x.CREDITSAFEID, creditSafeMonitoringEvent.Company.Id)).ToList();

                if (!ridderRelations.Any())
                {
                    continue;
                }

                result.AddRange(ridderRelations.Select(x => new LinkCompanyMonitoringEvent()
                {
                    RidderCompanyName = x.COMPANY, CreditSafeMonitoringEvent = creditSafeMonitoringEvent,
                }));
            }

            return result;
        }

        private void CreateMonitoringEventsInRidderIQ(List<LinkCompanyMonitoringEvent> linksCompanyMonitoringEvents)
        {
            //Aanmaken Monitoring Events in Anton Groep
            var linksCompanyMonitoringEventsAIO =
                linksCompanyMonitoringEvents.Where(x => string.Equals(x.RidderCompanyName, "Anton Groep")).ToList();

            if(linksCompanyMonitoringEventsAIO.Any())
            {
                var createdMonitoringEventsAIO = CreateMonitoringEventsCore(_sdkSession, linksCompanyMonitoringEventsAIO);

                if(createdMonitoringEventsAIO.Any())
                {
                    ProcessMonitoringEvents(_sdkSession, createdMonitoringEventsAIO);
                }
            }

            //Aanmaken Monitoring Events in de andere bedrijven
            var companies = linksCompanyMonitoringEvents.Where(x => !string.Equals(x.RidderCompanyName, "Anton Groep"))
                .Select(x => x.RidderCompanyName).Distinct().ToList();

            if(!companies.Any())
            {
                return;
            }

            foreach (var company in companies)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"Start aanmaken monitoring events ({company}).");

                var linksCompanyMonitoringEventsThisCompany = linksCompanyMonitoringEvents.Where(x => string.Equals(x.RidderCompanyName, company)).ToList();
                
                var configuration = new SdkConfiguration()
                {
                    CompanyName = company,
                    UserName = _sdkSession.Configuration.UserName,
                    Password = _sdkSession.Configuration.Password,
                    PersistSession = _sdkSession.Configuration.PersistSession,
                    RidderIQClientPath = _sdkSession.Configuration.RidderIQClientPath,
                };

                var sdkSessionCompany = new SdkSession(configuration);

                var createdMonitoringEvents = CreateMonitoringEventsCore(sdkSessionCompany, linksCompanyMonitoringEventsThisCompany);

                if(createdMonitoringEvents.Any())
                {
                    ProcessMonitoringEvents(sdkSessionCompany, createdMonitoringEvents);
                }

                sdkSessionCompany.Logout();
            }
        }

        private void ProcessMonitoringEvents(SdkSession sdkSessionCompany, List<int> createdMonitoringEvents)
        {
            var wfProcessMonitoringEvent = new Guid("fe644269-82ff-48e7-aecd-db27aabebaef");

            foreach (var createdMonitoringEvent in createdMonitoringEvents)
            {
                var wfResult = sdkSessionCompany.Sdk.ExecuteWorkflowEvent("C_CREDITSAFEMONITORINGEVENTS",
                    createdMonitoringEvent, wfProcessMonitoringEvent, null);

                if (wfResult.HasError)
                {
                    _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Error,
                        $"Verwerken monitoring event met id {createdMonitoringEvent} is mislukt; oorzaak: {wfResult.GetResult()}");
                }
            }

            _ridderLogger.Log(SchedulerTask, sdkSessionCompany.Configuration.CompanyName, SchedulerLogLevel.Info,
                $"{createdMonitoringEvents.Count} monitoring events verwerkt in {sdkSessionCompany.Configuration.CompanyName}");
        }

        private List<int> CreateMonitoringEventsCore(SdkSession sdkSession, List<LinkCompanyMonitoringEvent> linksCompanyMonitoringEvents)
        {
            var uniqueEventIds = linksCompanyMonitoringEvents.Select(x => x.CreditSafeMonitoringEvent.EventId).Distinct().ToList();

            var rsMonitoringEvents = sdkSession.GetRecordsetColumns("C_CREDITSAFEMONITORINGEVENTS", "", 
                $"EVENTID IN ({string.Join(",", uniqueEventIds)})", "");
            rsMonitoringEvents.UseDataChanges = false;
            rsMonitoringEvents.UpdateWhenMoveRecord = false;

            var alreadyExistingEventIds = rsMonitoringEvents.DataTable.AsEnumerable().Select(x => x.Field<int>("EVENTID")).Distinct().ToList();

            foreach (var linkCompanyMonitoringEvents in linksCompanyMonitoringEvents)
            {
                var ridderRelation = sdkSession
                    .Find<Relation>(
                        $"CREDITSAFEID = '{linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.Company.Id}'")
                    .FirstOrDefault();

                if (ridderRelation == null)
                {
                    continue;
                }

                if(linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.EventId != 0 && alreadyExistingEventIds.Contains(linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.EventId))
                {
                    continue;
                }
                
                rsMonitoringEvents.AddNew();
                rsMonitoringEvents.SetFieldValue("FK_RELATION", ridderRelation.Id);
                rsMonitoringEvents.SetFieldValue("EVENTID", linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.EventId);
                rsMonitoringEvents.SetFieldValue("EVENTDATE", linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.EventDate);
                rsMonitoringEvents.SetFieldValue("MONITORINGRULECODE", linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.RuleCode);
                rsMonitoringEvents.SetFieldValue("MONITORINGRULEDESCRIPTION", linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.RuleName);
                rsMonitoringEvents.SetFieldValue("OLDVALUE", linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.OldValue);
                rsMonitoringEvents.SetFieldValue("NEWVALUE", linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.NewValue);

                rsMonitoringEvents.SetFieldValue("EXTERNALKEY", linkCompanyMonitoringEvents.CreditSafeMonitoringEvent.EventId);
            }

            if (rsMonitoringEvents.RecordCount == 0)
            {
                return new List<int>();
            }

            var updateResult = rsMonitoringEvents.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                _ridderLogger.Log(SchedulerTask, sdkSession.Configuration.CompanyName, SchedulerLogLevel.Error,
                    $"Aanmaken monitoring events in Ridder iQ is mislukt; oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }

            var createdMonitoringEvents = updateResult.Select(x => (int)x.PrimaryKey).ToList();

            _ridderLogger.Log(SchedulerTask, sdkSession.Configuration.CompanyName, SchedulerLogLevel.Info,
                $"{createdMonitoringEvents.Count} monitoring events geïmporteerd in {sdkSession.Configuration.CompanyName}");

            return createdMonitoringEvents;
        }

        private async Task<List<CreditSafeMonitoringEvent>> GetMonitoringEventsFromPortfolio(HttpClient client,
            int portfolioId)
        {
            var pageSize = 50;
            var pageNumber = 0;
            var result = new List<CreditSafeMonitoringEvent>();
            var weGotAllMonitoringEventsOfToday = false;

            var intervalDaysToImportMonitoringEvents =
                Convert.ToInt32(ConfigurationManager.AppSettings["IntervalDaysToImportMonitoringEvents"]);

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"Interval ophalen monitoring events:{intervalDaysToImportMonitoringEvents} dagen");

            var startInterval = DateTime.Now.AddDays(intervalDaysToImportMonitoringEvents * -1);

            while (!weGotAllMonitoringEventsOfToday)
            {
                //Get monitoring rules from CreditSafe
                var url = new Uri(
                    $"https://connect.creditsafe.com/v1/monitoring/portfolios/{portfolioId}/notificationEvents?sortBy=eventDate&sortDir=desc&page={pageNumber}&pageSize={pageSize}");
            
                var response = await client.GetStringAsync(url);

                if (response == null || string.IsNullOrEmpty(response))
                {
                    _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                        $"Ophalen CreditSafe monitoring events mislukt.");


                    return null;
                }

                var rootObjectMonitoringEvents =  JsonConvert.DeserializeObject<RootObjectMonitoringEvents>(response);
                var creditSafeMonitoringEvents = rootObjectMonitoringEvents.MonitoringEvents.ToList();

                if (!creditSafeMonitoringEvents.Any())
                {
                    break; //Er zijn geen monitoring events gevonden.
                }

                result.AddRange(creditSafeMonitoringEvents
                    .Where(x => x.CreatedDate > startInterval).ToList());

                var lastCreatedDate = creditSafeMonitoringEvents.Max(x => x.CreatedDate);

                if (lastCreatedDate < startInterval)
                {
                    weGotAllMonitoringEventsOfToday = true;
                }

                if (weGotAllMonitoringEventsOfToday)
                {
                    break;
                }

                pageNumber++;
            }

            return result;
        }

        private async Task<bool> CompareRidderRelationsAndCreditSafePortfolio(HttpClient client, int portfolioId)
        {
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Start bijwerken portfolio.");
            
            var relationsWithOpenSalesActivities = GetRelationsWithOpenSalesActivities();

            if (relationsWithOpenSalesActivities == null)
            {
                return false;
            }

            var resultCompaniesInPortfolio = GetCompaniesFromPortfolio(client, portfolioId);

            if (resultCompaniesInPortfolio.Status == TaskStatus.Faulted || resultCompaniesInPortfolio.Result == null)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                    $"Ophalen CreditSafe portfolio companies mislukt; oorzaak:{resultCompaniesInPortfolio.Exception}");


                return false;
            }

            var companiesInPortfolio = resultCompaniesInPortfolio.Result.ToList();

            var creditSafeIdsRelationsWithOpenSalesActivities =
                relationsWithOpenSalesActivities.Select(x => x.CREDITSAFEID).Distinct().ToList();
            var creditSafeIdsPortfolioRelations = companiesInPortfolio.Select(x => x.Id).Distinct().ToList();

            var creditSafeIdsToDeleteFromPortfolio = creditSafeIdsPortfolioRelations
                .Except(creditSafeIdsRelationsWithOpenSalesActivities).ToList();

            
            await DeleteFromPortfolio(client, creditSafeIdsToDeleteFromPortfolio, companiesInPortfolio, portfolioId);

            var creditSafeIdsToInsertInPortfolio = creditSafeIdsRelationsWithOpenSalesActivities
                .Except(creditSafeIdsPortfolioRelations).ToList();
            
            await InsertIntoPortfolio(client, creditSafeIdsToInsertInPortfolio, relationsWithOpenSalesActivities, portfolioId);


            //De tabel C_CREDITSAFEPORTFOLIORELATIONS moet altijd overeen komen met de CreditSafe portfolio relaties. Uit een steekproef blijkt dit nu niet zo te zijn.
            //Doe hier een extra vergelijk en werk de tabel bij
            
            var allPortfolioRelationCreditSafeIds = _sdkSession.Find<PortfolioRelation>().Select(x => x.CREDITSAFEID).ToList();

            var creditSafeIdsToAddInTable = creditSafeIdsPortfolioRelations.Except(allPortfolioRelationCreditSafeIds).ToList();
            var creditSafeIdsToDeleteInTable = allPortfolioRelationCreditSafeIds.Except(creditSafeIdsPortfolioRelations).ToList();

            if(creditSafeIdsToAddInTable.Any() || creditSafeIdsToDeleteInTable.Any())
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, $"Extra check bijwerken portfolio, toe te voegen aan Ridder tabel: {creditSafeIdsToAddInTable.Count}, te verwijderen uit Ridder tabel: {creditSafeIdsToDeleteInTable.Count}.");

                InsertIntoTableRidderIQPortfolioRelations(creditSafeIdsToAddInTable, relationsWithOpenSalesActivities);
                DeleteFromTableRidderIQPortfolioRelations(creditSafeIdsToDeleteInTable);
            }

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info, "Bijwerken portfolio afgerond.");

            return true;
        }

        private async Task DeleteFromPortfolio(HttpClient client, List<string> creditSafeIdsToDeleteFromPortfolio,
            List<Company> companiesInPortfolio, int portfolioId)
        {
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{creditSafeIdsToDeleteFromPortfolio.Count} relaties gevonden om te verwijderen van de portfolio");


            if (!creditSafeIdsToDeleteFromPortfolio.Any())
            {
                return;
            }

            foreach (var creditSafeId in creditSafeIdsToDeleteFromPortfolio)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                    $"Te verwijderen uit portfolio: {companiesInPortfolio.First(x => string.Equals(x.Id, creditSafeId)).Name}");
            }

            await DeleteCompaniesFromPortfolioOnCreditSafe(client, creditSafeIdsToDeleteFromPortfolio, portfolioId);

            DeleteFromTableRidderIQPortfolioRelations(creditSafeIdsToDeleteFromPortfolio);
        }

        private void DeleteFromTableRidderIQPortfolioRelations(List<string> creditSafeIdsToDeleteFromPortfolio)
        {
            var portfolioRelationsToDelete =
                _sdkSession.Find<PortfolioRelation>(x => creditSafeIdsToDeleteFromPortfolio.Contains(x.CREDITSAFEID))
                    .ToList();

            if(!portfolioRelationsToDelete.Any())
            {
                return;
            }

            _sdkSession.DeleteBulk(portfolioRelationsToDelete);

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{portfolioRelationsToDelete.Count} records verwijderd uit Ridder tabel portfolio relaties");
        }

        private async Task DeleteCompaniesFromPortfolioOnCreditSafe(HttpClient client,
            List<string> creditSafeIdsToDeleteFromPortfolio, int portfolioId)
        {
            foreach (var creditSafeId in creditSafeIdsToDeleteFromPortfolio)
            {
                var urlPath = new Uri(
                    $"https://connect.creditsafe.com/v1/monitoring/portfolios/{portfolioId}/companies/{creditSafeId}");

                var result = await client.DeleteAsync(urlPath);

                if(!result.IsSuccessStatusCode)
                {
                    _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                        $"Verwijderen CreditSafe id {creditSafeId} van portfolio is mislukt, oorzaak: {result.StatusCode}");
                }

                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                    $"{creditSafeId} verwijderd uit CreditSafe portfolio");
            }
        }

        private async Task InsertIntoPortfolio(HttpClient client,
            List<string> creditSafeIdsToInsertInPortfolio,
            List<AllRelation> relationsWithOpenSalesActivities, int portfolioId)
        {
            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{creditSafeIdsToInsertInPortfolio.Count} relaties gevonden om toe te voegen aan de portfolio");

            if (!creditSafeIdsToInsertInPortfolio.Any())
            {
                return;
            }

            foreach (var creditSafeId in creditSafeIdsToInsertInPortfolio)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                    $"Toe te voegen aan portfolio: {relationsWithOpenSalesActivities.First(x => string.Equals(x.CREDITSAFEID, creditSafeId)).NAME}");
            }

            await InsertIntoPortfolioOnCreditSafe(client, creditSafeIdsToInsertInPortfolio, portfolioId);

            InsertIntoTableRidderIQPortfolioRelations(creditSafeIdsToInsertInPortfolio,
                relationsWithOpenSalesActivities);
        }

        private void InsertIntoTableRidderIQPortfolioRelations(List<string> creditSafeIdsToInsertInPortfolio,
            List<AllRelation> relationsWithOpenSalesActivities)
        {
            var portfolioRelationsToInsert = new List<PortfolioRelation>();

            foreach (var creditSafeId in creditSafeIdsToInsertInPortfolio)
            {
                if(!relationsWithOpenSalesActivities.Any(x => string.Equals(x.CREDITSAFEID, creditSafeId)))
                {
                    _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                        $"Invoegen {creditSafeId} in Ridder tabel portfolio relaties geannuleerd. Relatie niet gevonden in 'RelationsWithOpenSalesActivities'.");
                    continue;
                }

                var portfolioRelation = new PortfolioRelation()
                {
                    CREDITSAFEID = creditSafeId,
                    NAME = relationsWithOpenSalesActivities
                        .FirstOrDefault(x => string.Equals(x.CREDITSAFEID, creditSafeId))?.NAME,
                };

                if(string.IsNullOrEmpty(portfolioRelation.NAME))
                {
                    _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                        $"Invoegen {portfolioRelation.CREDITSAFEID} in Ridder tabel portfolio relaties geannuleerd. NAME is leeg.");
                    continue;
                }

                portfolioRelationsToInsert.Add(portfolioRelation);
            }

            if(!portfolioRelationsToInsert.Any())
            {
                return;
            }

            try
            {
                _sdkSession.InsertBulk(portfolioRelationsToInsert, false);
            }
            catch(Exception e)
            {
                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                    $"Toevoegen van {portfolioRelationsToInsert.Count} records in Ridder tabel portfolio relaties is mislukt, oorzaak: {e}");
            }

            _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                $"{portfolioRelationsToInsert.Count} records toegevoegd in Ridder tabel portfolio relaties");
        }

        private async Task InsertIntoPortfolioOnCreditSafe(HttpClient client, List<string> creditSafeIdsToInsertInPortfolio, int portfolioId)
        {
            var urlPath = new Uri(
                $"https://connect.creditsafe.com/v1/monitoring/portfolios/{portfolioId}/companies");

            foreach (var creditSafeId in creditSafeIdsToInsertInPortfolio)
            {
                var addCompanyToPortfolio = new AddCompanyToPortfolio()
                {
                    CreditSafeId = creditSafeId,
                    PersonalReference = "",
                    FreeText = "",
                    PersonalLimit = "100", //Default
                };

                var content = new StringContent(JsonConvert.SerializeObject(addCompanyToPortfolio), Encoding.UTF8, "application/json");
                    
                var result = await client.PostAsync(urlPath, content);

                if(!result.IsSuccessStatusCode)
                {
                    _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Error,
                        $"Toevoegen CreditSafe id {creditSafeId} aan portfolio is mislukt, oorzaak: {result.StatusCode}");
                    continue;
                }

                _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                    $"{creditSafeId} toegevoegd aan CreditSafe portfolio");
            }
        }

        private async Task<List<Company>> GetCompaniesFromPortfolio(HttpClient client, int portfolioId)
        {
            var result = new List<Company>();

            var totalCompaniesPerPage = 500;
            var pageNumber = 0; //Bij ophalen companies van portfolio begint de page bij index 0
            var totalCompaniesFound = int.MaxValue;

            while (totalCompaniesFound > (pageNumber * totalCompaniesPerPage))
            {
                var urlPathPortfolioCompanies = new Uri(
                    $"https://connect.creditsafe.com/v1/monitoring/portfolios/{portfolioId}/companies?page={pageNumber}&pageSize={totalCompaniesPerPage}");

                var response = await client.GetStringAsync(urlPathPortfolioCompanies);

                if (response == null || string.IsNullOrEmpty(response))
                {
                    _ridderLogger.Log(SchedulerTask, "Anton Groep", SchedulerLogLevel.Info,
                        $"Ophalen portfolio relaties mislukt.");

                    return null;
                }

                var rootObjectCompanyInfo =  JsonConvert.DeserializeObject<RootObjectCompanies>(response);

                if (pageNumber == 1)
                {
                    totalCompaniesFound = rootObjectCompanyInfo.TotalCount;
                }

                result.AddRange(rootObjectCompanyInfo.Companies);

                pageNumber++;
            }

            return result;
        }

        private List<AllRelation> GetRelationsWithOpenSalesActivities()
        {
            return _sdkSession.Find<AllRelation>(x => x.OPENSALES > 0 && !string.IsNullOrEmpty(x.CREDITSAFEID)).ToList();
        }
    }
}
