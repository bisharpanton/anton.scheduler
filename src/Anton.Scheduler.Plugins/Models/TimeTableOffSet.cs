﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.Models
{
    partial class TimeTableOffSet
    {
        public TimeTable TimeTable { get; set; }
        public Employee Employee { get; set; }
    }
}
