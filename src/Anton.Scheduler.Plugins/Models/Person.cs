﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.Models
{
    partial class Person
    {
        public string RecordLabel => $"{FIRSTNAME} {$"{NAMEPREFIX} {LASTNAME}".Trim()}";
    }
}
