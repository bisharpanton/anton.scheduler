﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.Models
{
    partial class AllSafetyReport
    {
        [Ignore]
        public bool SuccesfulCopied { get; set; }
        [Ignore]
        public List<Document> Documents { get; set; }
    }
}
