namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum DefaultTOIL : int
    {
        
        /// <summary>
        /// Never
        /// </summary>
        [Description("Never")]
        Never = 1,
        
        /// <summary>
        /// OneOnOne
        /// </summary>
        [Description("OneOnOne")]
        OneOnOne = 2,
        
        /// <summary>
        /// ByTimecode
        /// </summary>
        [Description("ByTimecode")]
        ByTimecode = 3,
        
        /// <summary>
        /// ByTOILMarkup
        /// </summary>
        [Description("ByTOILMarkup")]
        ByTOILMarkup = 4,
    }
}
