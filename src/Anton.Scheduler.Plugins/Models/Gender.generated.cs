namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum Gender : int
    {
        
        /// <summary>
        /// Man
        /// </summary>
        [Description("Man")]
        Man = 1,
        
        /// <summary>
        /// Vrouw
        /// </summary>
        [Description("Vrouw")]
        Vrouw = 2,
        
        /// <summary>
        /// Onbekend
        /// </summary>
        [Description("Onbekend")]
        Onbekend = 3,
    }
}
