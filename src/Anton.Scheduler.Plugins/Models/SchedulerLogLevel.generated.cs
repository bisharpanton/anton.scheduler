namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum SchedulerLogLevel : int
    {
        
        /// <summary>
        /// Debug
        /// </summary>
        [Description("Debug")]
        Debug = 1,
        
        /// <summary>
        /// Info
        /// </summary>
        [Description("Info")]
        Info = 2,
        
        /// <summary>
        /// Warn
        /// </summary>
        [Description("Warn")]
        Warn = 3,
        
        /// <summary>
        /// Error
        /// </summary>
        [Description("Error")]
        Error = 4,
        
        /// <summary>
        /// Fatal
        /// </summary>
        [Description("Fatal")]
        Fatal = 5,
        
        /// <summary>
        /// Off
        /// </summary>
        [Description("Off")]
        Off = 6,
    }
}
