﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.Models
{
    partial class TimeTable
    {
        public List<TimeTableWorkday> TimeTableWorkdays { get; set; }
    }
}
