﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.Models
{
    partial class ProjectTime
    {
        [Ignore]
        public string EmployeeCode { get; set; }
        [Ignore]
        public string AfasAbsenceCode { get; set; }
        [Ignore]
        public string UniqueKey { get; set; }
        [Ignore]
        public string GroupByEmployeeDateWorkactivity => $"{FK_EMPLOYEE}_{DATE.Date:yyyyMMdd}_{FK_WORKACTIVITY}";
        [Ignore]
        public string GroupByEmployeeDate => $"{FK_EMPLOYEE}_{DATE.Date:yyyyMMdd}";
        [Ignore]
        public int TimeEmployeeInMinutes => Convert.ToInt32(Math.Round(TimeSpan.FromTicks(TIMEEMPLOYEE).TotalMinutes));
        [Ignore]
        public int TimeTvtInMinutes => Convert.ToInt32(Math.Round(TimeSpan.FromTicks(TIMEOFINLIEU ?? 0).TotalMinutes));
        [Ignore]
        public int TimeBuildUpDiscontinuityInMinutes => Convert.ToInt32(Math.Round(TimeSpan.FromTicks(BUILDUPDISCONTINUITY ?? 0).TotalMinutes));
    }
}
