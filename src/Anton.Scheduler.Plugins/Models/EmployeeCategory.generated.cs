namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum EmployeeCategory : int
    {
        
        /// <summary>
        /// Algemeen
        /// </summary>
        [Description("Algemeen")]
        Algemeen = 1,
        
        /// <summary>
        /// Indirect
        /// </summary>
        [Description("Indirect")]
        Indirect = 2,
        
        /// <summary>
        /// Indirect AIO
        /// </summary>
        [Description("Indirect AIO")]
        Indirect_AIO = 3,
        
        /// <summary>
        /// ZZP'er
        /// </summary>
        [Description("ZZP\'er")]
        ZZP_er = 4,
        
        /// <summary>
        /// Uitzendkracht
        /// </summary>
        [Description("Uitzendkracht")]
        Uitzendkracht = 5,
        
        /// <summary>
        /// Oproepkracht
        /// </summary>
        [Description("Oproepkracht")]
        Oproepkracht = 6,
    }
}
