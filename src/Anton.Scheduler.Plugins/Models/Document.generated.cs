//  <auto-generated />
namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_DOCUMENT
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATEAPPROVAL, DATECHANGED, DATECREATED, DATERECEPTION, DATEREVISION, DESCRIPTION, DOCUMENTDATA, DOCUMENTHASHCODE, DOCUMENTLOCATION
    ///  	DUMMYREFRESHDATECHANGED, EXTENSION, EXTERNALKEY, EXTERNALNUMBER, FK_AUTHOR, FK_R_DOCUMENT, FK_WORKFLOWSTATE, FORMAT, GELDIGHEIDSDUUR, MEMO
    ///  	NAME, ORIGINALLOCATION, PK_R_DOCUMENT, PLAINTEXT_MEMO, RECORDLINK, REVISION, SHAREPOINTID, STORAGESYSTEM, STORAGETYPE, USERCHANGED
    ///  	VIEW, WORKINSTRUCTION
    ///  
    ///  	Filter used: FK_TABLEINFO = 'ca9d5d63-291c-4908-b5d8-af06c705595b'
    ///  	Included columns: 
    ///  	Excluded columns: STORAGETYPE
    ///  </remarks>
    [Table("R_DOCUMENT")]
    public partial class Document
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATEAPPROVAL;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private System.Nullable<System.DateTime> _DATERECEPTION;
        
        private System.Nullable<System.DateTime> _DATEREVISION;
        
        private string _DESCRIPTION;
        
        private byte[] _DOCUMENTDATA;
        
        private string _DOCUMENTHASHCODE;
        
        private string _DOCUMENTLOCATION;
        
        private bool _DUMMYREFRESHDATECHANGED;
        
        private string _EXTENSION;
        
        private string _EXTERNALKEY;
        
        private string _EXTERNALNUMBER;
        
        private System.Nullable<int> _FK_AUTHOR;
        
        private int _FK_R_DOCUMENT;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private string _FORMAT;
        
        private System.Nullable<System.DateTime> _GELDIGHEIDSDUUR;
        
        private string _MEMO;
        
        private string _NAME;
        
        private string _ORIGINALLOCATION;
        
        private int _id;
        
        private string _PLAINTEXT_MEMO;
        
        private string _RECORDLINK;
        
        private string _REVISION;
        
        private string _SHAREPOINTID;
        
        private string _STORAGESYSTEM;
        
        private string _USERCHANGED;
        
        private string _VIEW;
        
        private bool _WORKINSTRUCTION;
        
        /// <summary>
        /// 	Created by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Date approval
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATEAPPROVAL")]
        public virtual System.Nullable<System.DateTime> DATEAPPROVAL
        {
            get
            {
                return this._DATEAPPROVAL;
            }
            set
            {
                this._DATEAPPROVAL = value;
            }
        }
        
        /// <summary>
        /// 	Date changed
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Date created
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Date reception
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATERECEPTION")]
        public virtual System.Nullable<System.DateTime> DATERECEPTION
        {
            get
            {
                return this._DATERECEPTION;
            }
            set
            {
                this._DATERECEPTION = value;
            }
        }
        
        /// <summary>
        /// 	Date revision
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATEREVISION")]
        public virtual System.Nullable<System.DateTime> DATEREVISION
        {
            get
            {
                return this._DATEREVISION;
            }
            set
            {
                this._DATEREVISION = value;
            }
        }
        
        /// <summary>
        /// 	Omschrijving
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 255
        /// </remarks>
        [Column("DESCRIPTION")]
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Documentgegevens
        /// </summary>
        /// <remarks>
        /// 	DataType: Blob
        /// 	Size: 0
        /// </remarks>
        [Column("DOCUMENTDATA")]
        public virtual byte[] DOCUMENTDATA
        {
            get
            {
                return this._DOCUMENTDATA;
            }
            set
            {
                this._DOCUMENTDATA = value;
            }
        }
        
        /// <summary>
        /// 	Document hashcode
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("DOCUMENTHASHCODE")]
        public virtual string DOCUMENTHASHCODE
        {
            get
            {
                if (string.IsNullOrEmpty(this._DOCUMENTHASHCODE))
                {
                    return "";
                }
                return this._DOCUMENTHASHCODE;
            }
            set
            {
                this._DOCUMENTHASHCODE = value;
            }
        }
        
        /// <summary>
        /// 	Documentlocatie
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 1024
        /// </remarks>
        [Column("DOCUMENTLOCATION")]
        public virtual string DOCUMENTLOCATION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DOCUMENTLOCATION))
                {
                    return "";
                }
                return this._DOCUMENTLOCATION;
            }
            set
            {
                this._DOCUMENTLOCATION = value;
            }
        }
        
        /// <summary>
        /// 	Dummy refresh date changed
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("DUMMYREFRESHDATECHANGED")]
        public virtual bool DUMMYREFRESHDATECHANGED
        {
            get
            {
                return this._DUMMYREFRESHDATECHANGED;
            }
            set
            {
                this._DUMMYREFRESHDATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Extension
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 255
        /// </remarks>
        [Column("EXTENSION")]
        public virtual string EXTENSION
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTENSION))
                {
                    return "";
                }
                return this._EXTENSION;
            }
            set
            {
                this._EXTENSION = value;
            }
        }
        
        /// <summary>
        /// 	External key
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        [Column("EXTERNALKEY")]
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	External number
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 80
        /// </remarks>
        [Column("EXTERNALNUMBER")]
        public virtual string EXTERNALNUMBER
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALNUMBER))
                {
                    return "";
                }
                return this._EXTERNALNUMBER;
            }
            set
            {
                this._EXTERNALNUMBER = value;
            }
        }
        
        /// <summary>
        /// 	Author
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_DOCUMENT
        /// </remarks>
        [Column("FK_AUTHOR")]
        public virtual System.Nullable<int> FK_AUTHOR
        {
            get
            {
                return this._FK_AUTHOR;
            }
            set
            {
                this._FK_AUTHOR = value;
            }
        }
        
        /// <summary>
        /// 	FK_R_DOCUMENT
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_DOCUMENT
        /// </remarks>
        [Column("FK_R_DOCUMENT")]
        public virtual int FK_R_DOCUMENT
        {
            get
            {
                return this._FK_R_DOCUMENT;
            }
            set
            {
                this._FK_R_DOCUMENT = value;
            }
        }
        
        /// <summary>
        /// 	Workflow state
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 0
        /// 	Table: R_DOCUMENT
        /// </remarks>
        [Column("FK_WORKFLOWSTATE")]
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Format
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 25
        /// </remarks>
        [Column("FORMAT")]
        public virtual string FORMAT
        {
            get
            {
                if (string.IsNullOrEmpty(this._FORMAT))
                {
                    return "";
                }
                return this._FORMAT;
            }
            set
            {
                this._FORMAT = value;
            }
        }
        
        /// <summary>
        /// 	Geldigheidsduur
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("GELDIGHEIDSDUUR")]
        public virtual System.Nullable<System.DateTime> GELDIGHEIDSDUUR
        {
            get
            {
                return this._GELDIGHEIDSDUUR;
            }
            set
            {
                this._GELDIGHEIDSDUUR = value;
            }
        }
        
        /// <summary>
        /// 	Memo
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("MEMO")]
        public virtual string MEMO
        {
            get
            {
                if (string.IsNullOrEmpty(this._MEMO))
                {
                    return "";
                }
                return this._MEMO;
            }
            set
            {
                this._MEMO = value;
            }
        }
        
        /// <summary>
        /// 	Name
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 255
        /// </remarks>
        [Column("NAME")]
        public virtual string NAME
        {
            get
            {
                if (string.IsNullOrEmpty(this._NAME))
                {
                    return "";
                }
                return this._NAME;
            }
            set
            {
                this._NAME = value;
            }
        }
        
        /// <summary>
        /// 	Oorspronkelijke locatie
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 1024
        /// </remarks>
        [Column("ORIGINALLOCATION")]
        public virtual string ORIGINALLOCATION
        {
            get
            {
                if (string.IsNullOrEmpty(this._ORIGINALLOCATION))
                {
                    return "";
                }
                return this._ORIGINALLOCATION;
            }
            set
            {
                this._ORIGINALLOCATION = value;
            }
        }
        
        /// <summary>
        /// 	Documents id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_DOCUMENT")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("PLAINTEXT_MEMO")]
        public virtual string PLAINTEXT_MEMO
        {
            get
            {
                if (string.IsNullOrEmpty(this._PLAINTEXT_MEMO))
                {
                    return "";
                }
                return this._PLAINTEXT_MEMO;
            }
            set
            {
                this._PLAINTEXT_MEMO = value;
            }
        }
        
        /// <summary>
        /// 	Record link
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("RECORDLINK")]
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Revision
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 20
        /// </remarks>
        [Column("REVISION")]
        public virtual string REVISION
        {
            get
            {
                if (string.IsNullOrEmpty(this._REVISION))
                {
                    return "";
                }
                return this._REVISION;
            }
            set
            {
                this._REVISION = value;
            }
        }
        
        /// <summary>
        /// 	SharePoint id
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("SHAREPOINTID")]
        public virtual string SHAREPOINTID
        {
            get
            {
                if (string.IsNullOrEmpty(this._SHAREPOINTID))
                {
                    return "";
                }
                return this._SHAREPOINTID;
            }
            set
            {
                this._SHAREPOINTID = value;
            }
        }
        
        /// <summary>
        /// 	Opslagsysteem
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("STORAGESYSTEM")]
        public virtual string STORAGESYSTEM
        {
            get
            {
                if (string.IsNullOrEmpty(this._STORAGESYSTEM))
                {
                    return "";
                }
                return this._STORAGESYSTEM;
            }
            set
            {
                this._STORAGESYSTEM = value;
            }
        }
        
        /// <summary>
        /// 	Changed by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("USERCHANGED")]
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
        
        /// <summary>
        /// 	View
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 25
        /// </remarks>
        [Column("VIEW")]
        public virtual string VIEW
        {
            get
            {
                if (string.IsNullOrEmpty(this._VIEW))
                {
                    return "";
                }
                return this._VIEW;
            }
            set
            {
                this._VIEW = value;
            }
        }
        
        /// <summary>
        /// 	Work instruction
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("WORKINSTRUCTION")]
        public virtual bool WORKINSTRUCTION
        {
            get
            {
                return this._WORKINSTRUCTION;
            }
            set
            {
                this._WORKINSTRUCTION = value;
            }
        }
    }
}
