﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.Models
{
    partial class EmployeePrivateData
    {
        [ForeignKey("FK_ADDRESS"), Include]
        public Address Address { get; set; }
    }
}
