//  <auto-generated />
namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: C_VEILIGHEIDSMELDINGENDOCUMENT
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, EXTERNALKEY, FK_DOCUMENT, FK_VEILIGHEIDSMELDINGEN, FK_WORKFLOWSTATE, PK_C_VEILIGHEIDSMELDINGENDOCUMENT, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '296e23f9-18ed-4845-afac-896bcca23fbc'
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("C_VEILIGHEIDSMELDINGENDOCUMENT")]
    public partial class SafetyReportDocument
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private string _EXTERNALKEY;
        
        private int _FK_DOCUMENT;
        
        private int _FK_VEILIGHEIDSMELDINGEN;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private int _id;
        
        private string _RECORDLINK;
        
        private string _USERCHANGED;
        
        /// <summary>
        /// 	Aangemaakt door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Datum gewijzigd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Datum aangemaakt
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Externe sleutel
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        [Column("EXTERNALKEY")]
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	Document
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGENDOCUMENT
        /// </remarks>
        [Column("FK_DOCUMENT")]
        public virtual int FK_DOCUMENT
        {
            get
            {
                return this._FK_DOCUMENT;
            }
            set
            {
                this._FK_DOCUMENT = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsmelding
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGENDOCUMENT
        /// </remarks>
        [Column("FK_VEILIGHEIDSMELDINGEN")]
        public virtual int FK_VEILIGHEIDSMELDINGEN
        {
            get
            {
                return this._FK_VEILIGHEIDSMELDINGEN;
            }
            set
            {
                this._FK_VEILIGHEIDSMELDINGEN = value;
            }
        }
        
        /// <summary>
        /// 	Workflowstatus
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGENDOCUMENT
        /// </remarks>
        [Column("FK_WORKFLOWSTATE")]
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Documenten van Veiligheidsmelding ID
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_C_VEILIGHEIDSMELDINGENDOCUMENT")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Record link
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("RECORDLINK")]
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Gewijzigd door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("USERCHANGED")]
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
    }
}
