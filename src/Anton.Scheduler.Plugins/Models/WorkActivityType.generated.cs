namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum WorkActivityType : int
    {
        
        /// <summary>
        /// Begin werktijd
        /// </summary>
        [Description("Begin werktijd")]
        Begin_werktijd = 1,
        
        /// <summary>
        /// Einde werktijd
        /// </summary>
        [Description("Einde werktijd")]
        Einde_werktijd = 2,
        
        /// <summary>
        /// Doorgaan laatste taak
        /// </summary>
        [Description("Doorgaan laatste taak")]
        Doorgaan_laatste_taak = 3,
        
        /// <summary>
        /// Parttime afwezig
        /// </summary>
        [Description("Parttime afwezig")]
        Parttime_afwezig = 4,
        
        /// <summary>
        /// Pauze
        /// </summary>
        [Description("Pauze")]
        Pauze = 5,
        
        /// <summary>
        /// Ziekte
        /// </summary>
        [Description("Ziekte")]
        Ziekte = 6,
        
        /// <summary>
        /// Tijd voor tijd
        /// </summary>
        [Description("Tijd voor tijd")]
        Tijd_voor_tijd = 7,
        
        /// <summary>
        /// A.D.V.
        /// </summary>
        [Description("A.D.V.")]
        A_D_V_ = 8,
        
        /// <summary>
        /// Nat. feestdagen
        /// </summary>
        [Description("Nat. feestdagen")]
        Nat__feestdagen = 9,
        
        /// <summary>
        /// Vakantie-,  snipperdagen
        /// </summary>
        [Description("Vakantie-,  snipperdagen")]
        Vakantie____snipperdagen = 10,
        
        /// <summary>
        /// Reisuren
        /// </summary>
        [Description("Reisuren")]
        Reisuren = 11,
        
        /// <summary>
        /// Betaald verzuim
        /// </summary>
        [Description("Betaald verzuim")]
        Betaald_verzuim = 12,
        
        /// <summary>
        /// Onwettig verzuim
        /// </summary>
        [Description("Onwettig verzuim")]
        Onwettig_verzuim = 13,
        
        /// <summary>
        /// Onbetaald verzuim
        /// </summary>
        [Description("Onbetaald verzuim")]
        Onbetaald_verzuim = 14,
        
        /// <summary>
        /// Indirecte uren
        /// </summary>
        [Description("Indirecte uren")]
        Indirecte_uren = 15,
        
        /// <summary>
        /// Directe uren
        /// </summary>
        [Description("Directe uren")]
        Directe_uren = 16,
    }
}
