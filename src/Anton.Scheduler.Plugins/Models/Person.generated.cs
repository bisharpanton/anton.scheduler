//  <auto-generated />
namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_PERSON
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	ANONYMOUS, CELLPHONE, CREATOR, DATECHANGED, DATECREATED, DATEOFBIRTH, DATEPICTURECHANGED, DECEASED, EMAIL, EXTERNALKEY
    ///  	FAX, FIRSTNAME, FK_ADDRESS, FK_NATIONALITEIT, FK_R_PERSON, FK_TITLE, FK_WORKFLOWSTATE, GENDER, INITIALS, LASTNAME
    ///  	NAMEPREFIX, PHONE1, PHONE2, PICTURE, PK_R_PERSON, RECORDLINK, SALUTATION, SPEEDDIAL, THUMB_PICTURE, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = 'd71a577f-74f8-467b-b4e1-96db0fb6bbc0'
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_PERSON")]
    public partial class Person
    {
        
        private bool _ANONYMOUS;
        
        private string _CELLPHONE;
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private System.Nullable<System.DateTime> _DATEOFBIRTH;
        
        private System.Nullable<System.DateTime> _DATEPICTURECHANGED;
        
        private bool _DECEASED;
        
        private string _EMAIL;
        
        private string _EXTERNALKEY;
        
        private string _FAX;
        
        private string _FIRSTNAME;
        
        private System.Nullable<int> _FK_ADDRESS;
        
        private System.Nullable<int> _FK_NATIONALITEIT;
        
        private int _FK_R_PERSON;
        
        private System.Nullable<int> _FK_TITLE;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private Gender _GENDER;
        
        private string _INITIALS;
        
        private string _LASTNAME;
        
        private string _NAMEPREFIX;
        
        private string _PHONE1;
        
        private string _PHONE2;
        
        private byte[] _PICTURE;
        
        private int _id;
        
        private string _RECORDLINK;
        
        private string _SALUTATION;
        
        private string _SPEEDDIAL;
        
        private byte[] _THUMB_PICTURE;
        
        private string _USERCHANGED;
        
        /// <summary>
        /// 	Anonymous
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("ANONYMOUS")]
        public virtual bool ANONYMOUS
        {
            get
            {
                return this._ANONYMOUS;
            }
            set
            {
                this._ANONYMOUS = value;
            }
        }
        
        /// <summary>
        /// 	(!)Cell phone
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CELLPHONE")]
        public virtual string CELLPHONE
        {
            get
            {
                if (string.IsNullOrEmpty(this._CELLPHONE))
                {
                    return "";
                }
                return this._CELLPHONE;
            }
            set
            {
                this._CELLPHONE = value;
            }
        }
        
        /// <summary>
        /// 	Created by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Date changed
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Date created
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	(!)Date of birth
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATEOFBIRTH")]
        public virtual System.Nullable<System.DateTime> DATEOFBIRTH
        {
            get
            {
                return this._DATEOFBIRTH;
            }
            set
            {
                this._DATEOFBIRTH = value;
            }
        }
        
        /// <summary>
        /// 	Datum foto gewijzigd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATEPICTURECHANGED")]
        public virtual System.Nullable<System.DateTime> DATEPICTURECHANGED
        {
            get
            {
                return this._DATEPICTURECHANGED;
            }
            set
            {
                this._DATEPICTURECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	(!)Deceased
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("DECEASED")]
        public virtual bool DECEASED
        {
            get
            {
                return this._DECEASED;
            }
            set
            {
                this._DECEASED = value;
            }
        }
        
        /// <summary>
        /// 	(!)Email
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 255
        /// </remarks>
        [Column("EMAIL")]
        public virtual string EMAIL
        {
            get
            {
                if (string.IsNullOrEmpty(this._EMAIL))
                {
                    return "";
                }
                return this._EMAIL;
            }
            set
            {
                this._EMAIL = value;
            }
        }
        
        /// <summary>
        /// 	External key
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        [Column("EXTERNALKEY")]
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	(!)Fax
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("FAX")]
        public virtual string FAX
        {
            get
            {
                if (string.IsNullOrEmpty(this._FAX))
                {
                    return "";
                }
                return this._FAX;
            }
            set
            {
                this._FAX = value;
            }
        }
        
        /// <summary>
        /// 	First name
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 127
        /// </remarks>
        [Column("FIRSTNAME")]
        public virtual string FIRSTNAME
        {
            get
            {
                if (string.IsNullOrEmpty(this._FIRSTNAME))
                {
                    return "";
                }
                return this._FIRSTNAME;
            }
            set
            {
                this._FIRSTNAME = value;
            }
        }
        
        /// <summary>
        /// 	(!)Address
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PERSON
        /// </remarks>
        [Column("FK_ADDRESS")]
        public virtual System.Nullable<int> FK_ADDRESS
        {
            get
            {
                return this._FK_ADDRESS;
            }
            set
            {
                this._FK_ADDRESS = value;
            }
        }
        
        /// <summary>
        /// 	Nationaliteit
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PERSON
        /// </remarks>
        [Column("FK_NATIONALITEIT")]
        public virtual System.Nullable<int> FK_NATIONALITEIT
        {
            get
            {
                return this._FK_NATIONALITEIT;
            }
            set
            {
                this._FK_NATIONALITEIT = value;
            }
        }
        
        /// <summary>
        /// 	FK_R_PERSON
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PERSON
        /// </remarks>
        [Column("FK_R_PERSON")]
        public virtual int FK_R_PERSON
        {
            get
            {
                return this._FK_R_PERSON;
            }
            set
            {
                this._FK_R_PERSON = value;
            }
        }
        
        /// <summary>
        /// 	Title
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PERSON
        /// </remarks>
        [Column("FK_TITLE")]
        public virtual System.Nullable<int> FK_TITLE
        {
            get
            {
                return this._FK_TITLE;
            }
            set
            {
                this._FK_TITLE = value;
            }
        }
        
        /// <summary>
        /// 	Workflow state
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 0
        /// 	Table: R_PERSON
        /// </remarks>
        [Column("FK_WORKFLOWSTATE")]
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Gender
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [Column("GENDER")]
        public virtual Gender GENDER
        {
            get
            {
                return this._GENDER;
            }
            set
            {
                this._GENDER = value;
            }
        }
        
        /// <summary>
        /// 	Initials
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("INITIALS")]
        public virtual string INITIALS
        {
            get
            {
                if (string.IsNullOrEmpty(this._INITIALS))
                {
                    return "";
                }
                return this._INITIALS;
            }
            set
            {
                this._INITIALS = value;
            }
        }
        
        /// <summary>
        /// 	Last name
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 127
        /// </remarks>
        [Column("LASTNAME")]
        public virtual string LASTNAME
        {
            get
            {
                if (string.IsNullOrEmpty(this._LASTNAME))
                {
                    return "";
                }
                return this._LASTNAME;
            }
            set
            {
                this._LASTNAME = value;
            }
        }
        
        /// <summary>
        /// 	Name prefix
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("NAMEPREFIX")]
        public virtual string NAMEPREFIX
        {
            get
            {
                if (string.IsNullOrEmpty(this._NAMEPREFIX))
                {
                    return "";
                }
                return this._NAMEPREFIX;
            }
            set
            {
                this._NAMEPREFIX = value;
            }
        }
        
        /// <summary>
        /// 	(!)Phone 1
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("PHONE1")]
        public virtual string PHONE1
        {
            get
            {
                if (string.IsNullOrEmpty(this._PHONE1))
                {
                    return "";
                }
                return this._PHONE1;
            }
            set
            {
                this._PHONE1 = value;
            }
        }
        
        /// <summary>
        /// 	(!)Phone 2
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("PHONE2")]
        public virtual string PHONE2
        {
            get
            {
                if (string.IsNullOrEmpty(this._PHONE2))
                {
                    return "";
                }
                return this._PHONE2;
            }
            set
            {
                this._PHONE2 = value;
            }
        }
        
        /// <summary>
        /// 	(!)Picture
        /// </summary>
        /// <remarks>
        /// 	DataType: Image
        /// 	Size: 0
        /// </remarks>
        [Column("PICTURE")]
        public virtual byte[] PICTURE
        {
            get
            {
                return this._PICTURE;
            }
            set
            {
                this._PICTURE = value;
            }
        }
        
        /// <summary>
        /// 	Persons id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_PERSON")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Record link
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("RECORDLINK")]
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Salutation
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("SALUTATION")]
        public virtual string SALUTATION
        {
            get
            {
                if (string.IsNullOrEmpty(this._SALUTATION))
                {
                    return "";
                }
                return this._SALUTATION;
            }
            set
            {
                this._SALUTATION = value;
            }
        }
        
        /// <summary>
        /// 	(!)Speed dial
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("SPEEDDIAL")]
        public virtual string SPEEDDIAL
        {
            get
            {
                if (string.IsNullOrEmpty(this._SPEEDDIAL))
                {
                    return "";
                }
                return this._SPEEDDIAL;
            }
            set
            {
                this._SPEEDDIAL = value;
            }
        }
        
        /// <summary>
        /// 	(!)Picture
        /// </summary>
        /// <remarks>
        /// 	DataType: Image
        /// 	Size: 0
        /// </remarks>
        [Column("THUMB_PICTURE")]
        public virtual byte[] THUMB_PICTURE
        {
            get
            {
                return this._THUMB_PICTURE;
            }
            set
            {
                this._THUMB_PICTURE = value;
            }
        }
        
        /// <summary>
        /// 	Changed by
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("USERCHANGED")]
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
    }
}
