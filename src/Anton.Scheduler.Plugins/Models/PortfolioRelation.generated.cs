//  <auto-generated />
namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: C_CREDITSAFEPORTFOLIORELATIONS
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, CREDITSAFEID, DATECHANGED, DATECREATED, EXTERNALKEY, FK_WORKFLOWSTATE, NAME, PK_C_CREDITSAFEPORTFOLIORELATIONS, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '680f8a03-5b13-408f-be37-c8700ba2acc1'
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("C_CREDITSAFEPORTFOLIORELATIONS")]
    public partial class PortfolioRelation
    {
        
        private string _CREATOR;
        
        private string _CREDITSAFEID;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private string _EXTERNALKEY;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private string _NAME;
        
        private int _id;
        
        private string _RECORDLINK;
        
        private string _USERCHANGED;
        
        /// <summary>
        /// 	Aangemaakt door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("CREATOR")]
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	CreditSafe id
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 250
        /// </remarks>
        [Column("CREDITSAFEID")]
        public virtual string CREDITSAFEID
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREDITSAFEID))
                {
                    return "";
                }
                return this._CREDITSAFEID;
            }
            set
            {
                this._CREDITSAFEID = value;
            }
        }
        
        /// <summary>
        /// 	Datum gewijzigd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECHANGED")]
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Datum aangemaakt
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATECREATED")]
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Externe sleutel
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        [Column("EXTERNALKEY")]
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	Workflowstatus
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: C_CREDITSAFEPORTFOLIORELATIONS
        /// </remarks>
        [Column("FK_WORKFLOWSTATE")]
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Naam
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 250
        /// </remarks>
        [Column("NAME")]
        public virtual string NAME
        {
            get
            {
                if (string.IsNullOrEmpty(this._NAME))
                {
                    return "";
                }
                return this._NAME;
            }
            set
            {
                this._NAME = value;
            }
        }
        
        /// <summary>
        /// 	CreditSafe portfolio relaties id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_C_CREDITSAFEPORTFOLIORELATIONS")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Recordlink
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("RECORDLINK")]
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Gewijzigd door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("USERCHANGED")]
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
    }
}
