﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.Models
{
    partial class Employee
    {
        [ForeignKey("FK_PERSON"), Include]
        public Person Person { get; set; }

        [ForeignKey("FK_POSITION"), Include]
        public Function Function { get; set; }

        public EmployeeAvgData EmployeeAvgData { get; set; }

        public EmployeePrivateData EmployeePrivateData { get; set; }

        [ForeignKey("FK_CAO"), Include]
        public Cao Cao { get; set; }
    }
}
