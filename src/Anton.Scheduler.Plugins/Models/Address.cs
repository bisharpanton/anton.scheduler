﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.Models
{
    partial class Address
    {
        [ForeignKey("FK_COUNTRY"), Include]
        public Country Country { get; set; }
    }
}
