﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.Models
{
    partial class Workday
    {
        public double DefaultHoursInHours => TimeSpan.FromTicks(DEFAULTHOURS).TotalHours;
        public List<WorkdayOvertime> WorkdayOverTimes { get; set; }
    }
}
