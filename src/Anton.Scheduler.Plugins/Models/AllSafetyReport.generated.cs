//  <auto-generated />
namespace Anton.Scheduler.Plugins.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: C_ALLSAFETYREPORTS
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	ABBREVIATIONCOMPANY, BRONMELDING, CAUSE, COMPANY, COPIEDTOINTERCOMPANYRELATION, DATUMMELDING, DESCRIPTION, EXTERNALKEY, EXTERNALREPORTER, FK_WORKFLOWSTATE
    ///  	INTERCOMPANYRELATION, LONGTERMMEASURES, MEASURES, MELDER, MELDING, MEMO, PK_C_ALLSAFETYREPORTS, RESPONSLINKEDBACK, SAFETYREPORTID, SOORTMELDING
    ///  
    ///  	Filter used: FK_TABLEINFO = '44f2218e-ada6-4084-b1e3-4f45c944496f'
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("C_ALLSAFETYREPORTS")]
    public partial class AllSafetyReport
    {
        
        private string _ABBREVIATIONCOMPANY;
        
        private string _BRONMELDING;
        
        private string _CAUSE;
        
        private string _COMPANY;
        
        private bool _COPIEDTOINTERCOMPANYRELATION;
        
        private System.Nullable<System.DateTime> _DATUMMELDING;
        
        private string _DESCRIPTION;
        
        private string _EXTERNALKEY;
        
        private string _EXTERNALREPORTER;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private IntercompanyRelation _INTERCOMPANYRELATION;
        
        private string _LONGTERMMEASURES;
        
        private string _MEASURES;
        
        private string _MELDER;
        
        private Incidentmelding _MELDING;
        
        private string _MEMO;
        
        private System.Guid _id;
        
        private bool _RESPONSLINKEDBACK;
        
        private System.Nullable<int> _SAFETYREPORTID;
        
        private string _SOORTMELDING;
        
        /// <summary>
        /// 	Abbrevation company
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("ABBREVIATIONCOMPANY")]
        public virtual string ABBREVIATIONCOMPANY
        {
            get
            {
                if (string.IsNullOrEmpty(this._ABBREVIATIONCOMPANY))
                {
                    return "";
                }
                return this._ABBREVIATIONCOMPANY;
            }
            set
            {
                this._ABBREVIATIONCOMPANY = value;
            }
        }
        
        /// <summary>
        /// 	Bron melding
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 125
        /// </remarks>
        [Column("BRONMELDING")]
        public virtual string BRONMELDING
        {
            get
            {
                if (string.IsNullOrEmpty(this._BRONMELDING))
                {
                    return "";
                }
                return this._BRONMELDING;
            }
            set
            {
                this._BRONMELDING = value;
            }
        }
        
        /// <summary>
        /// 	Oorzaak
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("CAUSE")]
        public virtual string CAUSE
        {
            get
            {
                if (string.IsNullOrEmpty(this._CAUSE))
                {
                    return "";
                }
                return this._CAUSE;
            }
            set
            {
                this._CAUSE = value;
            }
        }
        
        /// <summary>
        /// 	Company
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        [Column("COMPANY")]
        public virtual string COMPANY
        {
            get
            {
                if (string.IsNullOrEmpty(this._COMPANY))
                {
                    return "";
                }
                return this._COMPANY;
            }
            set
            {
                this._COMPANY = value;
            }
        }
        
        /// <summary>
        /// 	Gekopieerd naar intercompany relatie
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("COPIEDTOINTERCOMPANYRELATION")]
        public virtual bool COPIEDTOINTERCOMPANYRELATION
        {
            get
            {
                return this._COPIEDTOINTERCOMPANYRELATION;
            }
            set
            {
                this._COPIEDTOINTERCOMPANYRELATION = value;
            }
        }
        
        /// <summary>
        /// 	Datum melding
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        [Column("DATUMMELDING")]
        public virtual System.Nullable<System.DateTime> DATUMMELDING
        {
            get
            {
                return this._DATUMMELDING;
            }
            set
            {
                this._DATUMMELDING = value;
            }
        }
        
        /// <summary>
        /// 	Description
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 125
        /// </remarks>
        [Column("DESCRIPTION")]
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	External key
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        [Column("EXTERNALKEY")]
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	Externe melder
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 250
        /// </remarks>
        [Column("EXTERNALREPORTER")]
        public virtual string EXTERNALREPORTER
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALREPORTER))
                {
                    return "";
                }
                return this._EXTERNALREPORTER;
            }
            set
            {
                this._EXTERNALREPORTER = value;
            }
        }
        
        /// <summary>
        /// 	Workflowstate
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: C_ALLSAFETYREPORTS
        /// </remarks>
        [Column("FK_WORKFLOWSTATE")]
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Intercompany relatie
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [Column("INTERCOMPANYRELATION")]
        public virtual IntercompanyRelation INTERCOMPANYRELATION
        {
            get
            {
                return this._INTERCOMPANYRELATION;
            }
            set
            {
                this._INTERCOMPANYRELATION = value;
            }
        }
        
        /// <summary>
        /// 	Lange termijn maatregelen
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("LONGTERMMEASURES")]
        public virtual string LONGTERMMEASURES
        {
            get
            {
                if (string.IsNullOrEmpty(this._LONGTERMMEASURES))
                {
                    return "";
                }
                return this._LONGTERMMEASURES;
            }
            set
            {
                this._LONGTERMMEASURES = value;
            }
        }
        
        /// <summary>
        /// 	Genomen maatregelen
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("MEASURES")]
        public virtual string MEASURES
        {
            get
            {
                if (string.IsNullOrEmpty(this._MEASURES))
                {
                    return "";
                }
                return this._MEASURES;
            }
            set
            {
                this._MEASURES = value;
            }
        }
        
        /// <summary>
        /// 	Melder
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 153
        /// </remarks>
        [Column("MELDER")]
        public virtual string MELDER
        {
            get
            {
                if (string.IsNullOrEmpty(this._MELDER))
                {
                    return "";
                }
                return this._MELDER;
            }
            set
            {
                this._MELDER = value;
            }
        }
        
        /// <summary>
        /// 	Melding
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [Column("MELDING")]
        public virtual Incidentmelding MELDING
        {
            get
            {
                return this._MELDING;
            }
            set
            {
                this._MELDING = value;
            }
        }
        
        /// <summary>
        /// 	Memo
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        [Column("MEMO")]
        public virtual string MEMO
        {
            get
            {
                if (string.IsNullOrEmpty(this._MEMO))
                {
                    return "";
                }
                return this._MEMO;
            }
            set
            {
                this._MEMO = value;
            }
        }
        
        /// <summary>
        /// 	Alle Anton veiligheidsmeldingen
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 0
        /// </remarks>
        [PrimaryKey("PK_C_ALLSAFETYREPORTS")]
        public virtual System.Guid Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Respons terug gekoppeld
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("RESPONSLINKEDBACK")]
        public virtual bool RESPONSLINKEDBACK
        {
            get
            {
                return this._RESPONSLINKEDBACK;
            }
            set
            {
                this._RESPONSLINKEDBACK = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsmeldingen id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [Column("SAFETYREPORTID")]
        public virtual System.Nullable<int> SAFETYREPORTID
        {
            get
            {
                return this._SAFETYREPORTID;
            }
            set
            {
                this._SAFETYREPORTID = value;
            }
        }
        
        /// <summary>
        /// 	Soort melding
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 125
        /// </remarks>
        [Column("SOORTMELDING")]
        public virtual string SOORTMELDING
        {
            get
            {
                if (string.IsNullOrEmpty(this._SOORTMELDING))
                {
                    return "";
                }
                return this._SOORTMELDING;
            }
            set
            {
                this._SOORTMELDING = value;
            }
        }
    }
}
