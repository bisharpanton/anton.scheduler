﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.CreditSafeModels
{
    class CompanyMonitoring
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("safeNumber")]
        public string SafeNumber { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }
        [JsonProperty("portfolioId")]
        public int PortfolioId { get; set; }
        [JsonProperty("portfolioName")]
        public string PortfolioName { get; set; }
    }
}
