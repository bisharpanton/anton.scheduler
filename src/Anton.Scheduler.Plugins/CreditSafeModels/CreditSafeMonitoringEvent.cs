﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.CreditSafeModels
{
    class CreditSafeMonitoringEvent
    {
        [JsonProperty("company")]
        public CompanyMonitoring Company { get; set; }
        [JsonProperty("eventId")]
        public int EventId { get; set; }
        [JsonProperty("eventDate")]
        public DateTime EventDate { get; set; }
        [JsonProperty("newValue")]
        public string NewValue { get; set; }
        [JsonProperty("oldValue")]
        public string OldValue { get; set; }
        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("notificationEventId")]
        public int NotificationEventId { get; set; }
        [JsonProperty("ruleCode")]
        public int RuleCode { get; set; }
        [JsonProperty("ruleName")]
        public string RuleName { get; set; }
        [JsonProperty("localEventCode")]
        public string LocalEventCode { get; set; }  
    }
}
