﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.CreditSafeModels
{
    class Company
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("safeNumber")]
        public string SafeNumber { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }
        [JsonProperty("portfolioId")]
        public int PortfolioId { get; set; }
        [JsonProperty("creditLimit")]
        public int? CreditLimit { get; set; }
        [JsonProperty("dateLastEvent")]
        public DateTime DateLastEvent { get; set; }
        [JsonProperty("freeText")]
        public string FreeText { get; set; }
        [JsonProperty("personalLimit")]
        public string PersonalLimit { get; set; }
        [JsonProperty("personalReference")]
        public string PersonalReference { get; set; }
        [JsonProperty("ratingCommon")]
        public string RatingCommon { get; set; }
        [JsonProperty("ratingLocal")]
        public string RatingLocal { get; set; }
        [JsonProperty("companyStatus")]
        public string CompanyStatus { get; set; }
    }
}
