﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.CreditSafeModels
{
    class RootObjectCompanies
    {
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("paging")]
        public Paging Paging { get; set; }
        [JsonProperty("data")]
        public List<Company> Companies { get; set; }
    }
}
