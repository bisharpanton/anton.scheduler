﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.CreditSafeModels
{
    class AddCompanyToPortfolio
    {
        [JsonProperty("id")]
        public string CreditSafeId { get; set; }
        [JsonProperty("personalReference")]
        public string PersonalReference { get; set; }
        [JsonProperty("freeText")]
        public string FreeText { get; set; }
        [JsonProperty("personalLimit")]
        public string PersonalLimit { get; set; }
    }
}
