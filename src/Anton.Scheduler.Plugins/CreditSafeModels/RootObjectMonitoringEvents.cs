﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.CreditSafeModels
{
    class RootObjectMonitoringEvents
    {
        [JsonProperty("correlationId")]
        public Guid CorrelationId { get; set; }

        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("data")]
        public CreditSafeMonitoringEvent[] MonitoringEvents { get; set; }
        [JsonProperty("paging")]
        public Paging Paging { get; set; }
    }
}
