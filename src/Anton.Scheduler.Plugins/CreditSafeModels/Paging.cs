﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.CreditSafeModels
{
    class Paging
    {
        [JsonProperty("size")]
        public int Size { get; set; }
        [JsonProperty("prev")]
        public object Previous { get; set; }
        [JsonProperty("next")]
        public object Next { get; set; }
        [JsonProperty("last")]
        public double Last { get; set; }
    }
}
