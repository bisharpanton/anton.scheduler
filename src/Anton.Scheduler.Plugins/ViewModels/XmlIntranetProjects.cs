﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.ViewModels
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false, ElementName = "data")]
    public class XmlIntranetProjects
    {
        private IntranetProjectsItem[] IntranetProjectsItemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("post")]
        public IntranetProjectsItem[] IntranetProjectsItems
        {
            get
            {
                return this.IntranetProjectsItemsField;
            }
            set
            {
                this.IntranetProjectsItemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class IntranetProjectsItem
    {

        private ushort idField;

        private string titleField;

        private object contentField;

        private object excerptField;

        private string dateField;

        private string postTypeField;

        private string permalinkField;

        private string statusField;

        private byte authorIDField;

        private string authorUsernameField;

        private string authorEmailField;

        private string authorFirstNameField;

        private string authorLastNameField;

        private string slugField;

        private object formatField;

        private object templateField;

        private byte parentField;

        private byte parentSlugField;

        private byte orderField;

        private string commentStatusField;

        private string pingStatusField;

        private string postModifiedDateField;

        private string werkmaatschappijField;

        private string titelField;

        private string publicerennaarcontainer1Field;

        private string publicerennaarcontainer2Field;

        private string publicerennaarinterneomgevingenField;

        private string projectomschrijvingopgeselecteerdewebsitesField;

        private string projectomschrijvingophetintranetField;

        private string projectomschrijvingopnarrowcastingField;

        private string samenvattingSEOField;

        private string toepassingDLPSprojectField;

        private string provincieDLPSprojectField;

        private string plaatsDLPSprojectField;

        private string productlijnDLPSprojectField;

        private string lichttransmissieDLPSprojectField;

        private string folieDLPSprojectField;

        private string typehekDLPSprojectField;

        private string hoofdafbeeldingField;

        private string extraafbeelding1Field;

        private string extraafbeelding2Field;

        private string extraafbeelding3Field;

        private string extraafbeelding4Field;

        private string extraafbeelding5Field;

        private string extraafbeelding6Field;

        private object extraafbeelding7Field;

        private object extraafbeelding8Field;

        private object extraafbeelding9Field;

        private string beschrijvinghoofdafbeeldingField;

        private string beschrijvingextraafbeelding1Field;

        private string beschrijvingextraafbeelding2Field;

        private string beschrijvingextraafbeelding3Field;

        private string beschrijvingextraafbeelding4Field;

        private string beschrijvingextraafbeelding5Field;

        private string beschrijvingextraafbeelding6Field;

        private object beschrijvingextraafbeelding7Field;

        private object beschrijvingextraafbeelding8Field;

        private object beschrijvingextraafbeelding9Field;

        /// <remarks/>
        public ushort ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public object Content
        {
            get
            {
                return this.contentField;
            }
            set
            {
                this.contentField = value;
            }
        }

        /// <remarks/>
        public object Excerpt
        {
            get
            {
                return this.excerptField;
            }
            set
            {
                this.excerptField = value;
            }
        }

        /// <remarks/>
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public string PostType
        {
            get
            {
                return this.postTypeField;
            }
            set
            {
                this.postTypeField = value;
            }
        }

        /// <remarks/>
        public string Permalink
        {
            get
            {
                return this.permalinkField;
            }
            set
            {
                this.permalinkField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public byte AuthorID
        {
            get
            {
                return this.authorIDField;
            }
            set
            {
                this.authorIDField = value;
            }
        }

        /// <remarks/>
        public string AuthorUsername
        {
            get
            {
                return this.authorUsernameField;
            }
            set
            {
                this.authorUsernameField = value;
            }
        }

        /// <remarks/>
        public string AuthorEmail
        {
            get
            {
                return this.authorEmailField;
            }
            set
            {
                this.authorEmailField = value;
            }
        }

        /// <remarks/>
        public string AuthorFirstName
        {
            get
            {
                return this.authorFirstNameField;
            }
            set
            {
                this.authorFirstNameField = value;
            }
        }

        /// <remarks/>
        public string AuthorLastName
        {
            get
            {
                return this.authorLastNameField;
            }
            set
            {
                this.authorLastNameField = value;
            }
        }

        /// <remarks/>
        public string Slug
        {
            get
            {
                return this.slugField;
            }
            set
            {
                this.slugField = value;
            }
        }

        /// <remarks/>
        public object Format
        {
            get
            {
                return this.formatField;
            }
            set
            {
                this.formatField = value;
            }
        }

        /// <remarks/>
        public object Template
        {
            get
            {
                return this.templateField;
            }
            set
            {
                this.templateField = value;
            }
        }

        /// <remarks/>
        public byte Parent
        {
            get
            {
                return this.parentField;
            }
            set
            {
                this.parentField = value;
            }
        }

        /// <remarks/>
        public byte ParentSlug
        {
            get
            {
                return this.parentSlugField;
            }
            set
            {
                this.parentSlugField = value;
            }
        }

        /// <remarks/>
        public byte Order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }

        /// <remarks/>
        public string CommentStatus
        {
            get
            {
                return this.commentStatusField;
            }
            set
            {
                this.commentStatusField = value;
            }
        }

        /// <remarks/>
        public string PingStatus
        {
            get
            {
                return this.pingStatusField;
            }
            set
            {
                this.pingStatusField = value;
            }
        }

        /// <remarks/>
        public string PostModifiedDate
        {
            get
            {
                return this.postModifiedDateField;
            }
            set
            {
                this.postModifiedDateField = value;
            }
        }

        /// <remarks/>
        public string Werkmaatschappij
        {
            get
            {
                return this.werkmaatschappijField;
            }
            set
            {
                this.werkmaatschappijField = value;
            }
        }

        /// <remarks/>
        public string Titel
        {
            get
            {
                return this.titelField;
            }
            set
            {
                this.titelField = value;
            }
        }

        /// <remarks/>
        public string Publicerennaarcontainer1
        {
            get
            {
                return this.publicerennaarcontainer1Field;
            }
            set
            {
                this.publicerennaarcontainer1Field = value;
            }
        }

        /// <remarks/>
        public string Publicerennaarcontainer2
        {
            get
            {
                return this.publicerennaarcontainer2Field;
            }
            set
            {
                this.publicerennaarcontainer2Field = value;
            }
        }

        /// <remarks/>
        public string Publicerennaarinterneomgevingen
        {
            get
            {
                return this.publicerennaarinterneomgevingenField;
            }
            set
            {
                this.publicerennaarinterneomgevingenField = value;
            }
        }

        /// <remarks/>
        public string Projectomschrijvingopgeselecteerdewebsites
        {
            get
            {
                return this.projectomschrijvingopgeselecteerdewebsitesField;
            }
            set
            {
                this.projectomschrijvingopgeselecteerdewebsitesField = value;
            }
        }

        /// <remarks/>
        public string Projectomschrijvingophetintranet
        {
            get
            {
                return this.projectomschrijvingophetintranetField;
            }
            set
            {
                this.projectomschrijvingophetintranetField = value;
            }
        }

        /// <remarks/>
        public string Projectomschrijvingopnarrowcasting
        {
            get
            {
                return this.projectomschrijvingopnarrowcastingField;
            }
            set
            {
                this.projectomschrijvingopnarrowcastingField = value;
            }
        }

        /// <remarks/>
        public string SamenvattingSEO
        {
            get
            {
                return this.samenvattingSEOField;
            }
            set
            {
                this.samenvattingSEOField = value;
            }
        }

        /// <remarks/>
        public string ToepassingDLPSproject
        {
            get
            {
                return this.toepassingDLPSprojectField;
            }
            set
            {
                this.toepassingDLPSprojectField = value;
            }
        }

        /// <remarks/>
        public string ProvincieDLPSproject
        {
            get
            {
                return this.provincieDLPSprojectField;
            }
            set
            {
                this.provincieDLPSprojectField = value;
            }
        }

        /// <remarks/>
        public string PlaatsDLPSproject
        {
            get
            {
                return this.plaatsDLPSprojectField;
            }
            set
            {
                this.plaatsDLPSprojectField = value;
            }
        }

        /// <remarks/>
        public string ProductlijnDLPSproject
        {
            get
            {
                return this.productlijnDLPSprojectField;
            }
            set
            {
                this.productlijnDLPSprojectField = value;
            }
        }

        /// <remarks/>
        public string LichttransmissieDLPSproject
        {
            get
            {
                return this.lichttransmissieDLPSprojectField;
            }
            set
            {
                this.lichttransmissieDLPSprojectField = value;
            }
        }

        /// <remarks/>
        public string FolieDLPSproject
        {
            get
            {
                return this.folieDLPSprojectField;
            }
            set
            {
                this.folieDLPSprojectField = value;
            }
        }

        /// <remarks/>
        public string TypehekDLPSproject
        {
            get
            {
                return this.typehekDLPSprojectField;
            }
            set
            {
                this.typehekDLPSprojectField = value;
            }
        }

        /// <remarks/>
        public string Hoofdafbeelding
        {
            get
            {
                return this.hoofdafbeeldingField;
            }
            set
            {
                this.hoofdafbeeldingField = value;
            }
        }

        /// <remarks/>
        public string Extraafbeelding1
        {
            get
            {
                return this.extraafbeelding1Field;
            }
            set
            {
                this.extraafbeelding1Field = value;
            }
        }

        /// <remarks/>
        public string Extraafbeelding2
        {
            get
            {
                return this.extraafbeelding2Field;
            }
            set
            {
                this.extraafbeelding2Field = value;
            }
        }

        /// <remarks/>
        public string Extraafbeelding3
        {
            get
            {
                return this.extraafbeelding3Field;
            }
            set
            {
                this.extraafbeelding3Field = value;
            }
        }

        /// <remarks/>
        public string Extraafbeelding4
        {
            get
            {
                return this.extraafbeelding4Field;
            }
            set
            {
                this.extraafbeelding4Field = value;
            }
        }

        /// <remarks/>
        public string Extraafbeelding5
        {
            get
            {
                return this.extraafbeelding5Field;
            }
            set
            {
                this.extraafbeelding5Field = value;
            }
        }

        /// <remarks/>
        public string Extraafbeelding6
        {
            get
            {
                return this.extraafbeelding6Field;
            }
            set
            {
                this.extraafbeelding6Field = value;
            }
        }

        /// <remarks/>
        public object Extraafbeelding7
        {
            get
            {
                return this.extraafbeelding7Field;
            }
            set
            {
                this.extraafbeelding7Field = value;
            }
        }

        /// <remarks/>
        public object Extraafbeelding8
        {
            get
            {
                return this.extraafbeelding8Field;
            }
            set
            {
                this.extraafbeelding8Field = value;
            }
        }

        /// <remarks/>
        public object Extraafbeelding9
        {
            get
            {
                return this.extraafbeelding9Field;
            }
            set
            {
                this.extraafbeelding9Field = value;
            }
        }

        /// <remarks/>
        public string Beschrijvinghoofdafbeelding
        {
            get
            {
                return this.beschrijvinghoofdafbeeldingField;
            }
            set
            {
                this.beschrijvinghoofdafbeeldingField = value;
            }
        }

        /// <remarks/>
        public string Beschrijvingextraafbeelding1
        {
            get
            {
                return this.beschrijvingextraafbeelding1Field;
            }
            set
            {
                this.beschrijvingextraafbeelding1Field = value;
            }
        }

        /// <remarks/>
        public string Beschrijvingextraafbeelding2
        {
            get
            {
                return this.beschrijvingextraafbeelding2Field;
            }
            set
            {
                this.beschrijvingextraafbeelding2Field = value;
            }
        }

        /// <remarks/>
        public string Beschrijvingextraafbeelding3
        {
            get
            {
                return this.beschrijvingextraafbeelding3Field;
            }
            set
            {
                this.beschrijvingextraafbeelding3Field = value;
            }
        }

        /// <remarks/>
        public string Beschrijvingextraafbeelding4
        {
            get
            {
                return this.beschrijvingextraafbeelding4Field;
            }
            set
            {
                this.beschrijvingextraafbeelding4Field = value;
            }
        }

        /// <remarks/>
        public string Beschrijvingextraafbeelding5
        {
            get
            {
                return this.beschrijvingextraafbeelding5Field;
            }
            set
            {
                this.beschrijvingextraafbeelding5Field = value;
            }
        }

        /// <remarks/>
        public string Beschrijvingextraafbeelding6
        {
            get
            {
                return this.beschrijvingextraafbeelding6Field;
            }
            set
            {
                this.beschrijvingextraafbeelding6Field = value;
            }
        }

        /// <remarks/>
        public object Beschrijvingextraafbeelding7
        {
            get
            {
                return this.beschrijvingextraafbeelding7Field;
            }
            set
            {
                this.beschrijvingextraafbeelding7Field = value;
            }
        }

        /// <remarks/>
        public object Beschrijvingextraafbeelding8
        {
            get
            {
                return this.beschrijvingextraafbeelding8Field;
            }
            set
            {
                this.beschrijvingextraafbeelding8Field = value;
            }
        }

        /// <remarks/>
        public object Beschrijvingextraafbeelding9
        {
            get
            {
                return this.beschrijvingextraafbeelding9Field;
            }
            set
            {
                this.beschrijvingextraafbeelding9Field = value;
            }
        }
    }
}
