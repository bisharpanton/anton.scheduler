﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.ViewModels
{
    //TODO: In mijn Anton Groep testomgeving bestond deze tabel nog niet. Anders hadden we deze ook via de models.tt kunnen aanmaken
    class BisharpRelationNewsFeed
    {
        public int Id { get; set; }
        public int RelationId { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
        public int ExternalId { get; set; }
        public string ExternalNewsUrl { get; set; }
        public NewsStatus NewsStatus { get; set; }
        public string AntonIntranetSource { get; set; }
        public string Company { get; set; }
    }

    public enum NewsStatus
    {
        New = 1,
        Published = 2,
        Archive = 3,
    }
}
