﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.CreditSafeModels;

namespace Anton.Scheduler.Plugins.ViewModels
{
    class LinkCompanyMonitoringEvent
    {
        public string RidderCompanyName { get; set; }
        public CreditSafeMonitoringEvent CreditSafeMonitoringEvent { get; set; }
    }
}
