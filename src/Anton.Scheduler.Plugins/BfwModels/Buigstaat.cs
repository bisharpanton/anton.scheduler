﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.Models;

namespace Anton.Scheduler.Plugins.BfwModels
{
    internal class Buigstaat
    {
        public int Id { get; set; }

        public int BfwWerkNr { get; set; }
        public Order RidderOrder { get; set; }

        public int Buigstaatnummer { get; set; }

        public string BfwFase { get; set; }
        public int RidderFaseId { get; set; }
        public Fase RidderFase { get; set; }

        public string Element { get; set; }
        public string Description { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string DrawingNumber { get; set; }
        public int BfwShipmentId { get; set; }

        public int BfwInvoiceId { get; set; }
        public bool BfwInvoiceComplete { get; set; }
        public DateTime? BfwInvoiceDate { get; set; }

        public string UniqueKey { get; set; }
    }
}
