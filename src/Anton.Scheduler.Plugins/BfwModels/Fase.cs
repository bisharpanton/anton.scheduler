﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.Models;

namespace Anton.Scheduler.Plugins.BfwModels
{
    internal class Fase
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public int RidderOrderId { get; set; }
        public int BfwWerkNr { get; set; }
        public Order RidderOrder { get; set; }

        public string UniqueKey { get; set; }
    }
}
