﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.BfwModels
{
    internal class ShippingHeader
    {
        public int ShipmentId { get; set; }
        public int WerkNr { get; set; }
        public DateTime Leverdatum { get; set; }
        public DateTime Boekdatum { get; set; }
        public string ExternalId { get; set; }

        public List<StockTransactions> StockTransactions { get; set; }

        public int FoundRidderOrderId { get; set; }
    }
}
