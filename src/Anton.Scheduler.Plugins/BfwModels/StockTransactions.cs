﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.BfwModels
{
    internal class StockTransactions
    {
        public string CodeMaterial { get; set; }
        public int GlobalId { get; set; }
        public int TransactionType { get; set; }
        public DateTime TransactionDate { get; set; }
        public long Amount { get; set; }

        public BfwItem Item { get; set; }
    }
}
