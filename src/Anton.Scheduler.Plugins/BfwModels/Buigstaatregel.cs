﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.Models;

namespace Anton.Scheduler.Plugins.BfwModels
{
    internal class Buigstaatregel
    {
        public int Id { get; set; }

        public Buigstaat Buigstaat { get; set; }
        public int LineNumber { get; set; }

        public string BfwCodeMat { get; set; }
        public int RidderItemId { get; set; }
        public Item RidderItem { get; set; }
        public BfwItem BfwItem { get; set; }

        public int Quantity { get; set; }
        public int BfwKnipMin { get; set; }
        public int BfwKnipMax { get; set; }
        public double Diameter { get; set; }
        public int Length { get; set; }
        public string Text { get; set; }
        public double Weight { get; set; }
        public double TotalWeight { get; set; }
        public int Width { get; set; }

        public string UniqueKey { get; set; }
    }
}
