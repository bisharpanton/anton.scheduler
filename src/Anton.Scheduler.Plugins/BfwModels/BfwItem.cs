﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.BfwModels
{
    internal class BfwItem
    {
        public string CodeMaterial { get; set; }
        public string ExternalId { get; set; }
    }
}
