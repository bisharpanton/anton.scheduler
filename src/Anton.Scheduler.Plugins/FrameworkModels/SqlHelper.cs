﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anton.Scheduler.Plugins.FrameworkModels
{
    public static class SqlHelper
    {
        internal static List<string> DetermineRidderAdministrationsWithAfasCompanyName(RidderLogger ridderLogger, string sqlServerInstanceName)
        {
            var allSqlDatabases = GetAllSqlDatabases(sqlServerInstanceName);

            var ridderCompaniesWithAfasCompanyNumber = new List<string>();

            foreach (var sqlDatabase in allSqlDatabases)
            {
                if (IsRidderAdministration(sqlServerInstanceName, sqlDatabase))
                {
                    var afasCompanyName = DetermineAfasCompanyInfoFromCrmSettings(sqlServerInstanceName, sqlDatabase);

                    if (string.IsNullOrEmpty(afasCompanyName))
                    {
                        continue;
                    }

                    ridderCompaniesWithAfasCompanyNumber.Add(sqlDatabase);
                }
            }

            return ridderCompaniesWithAfasCompanyNumber;
        }

        private static string DetermineAfasCompanyInfoFromCrmSettings(string sqlServerInstanceName, string sqlDatabase)
        {
            if (!ColumnAfasCompanyNumberExistsAtCrmSettings(sqlServerInstanceName, sqlDatabase))
            {
                return string.Empty;
            }

            var result = string.Empty;
            var connectionString = BuildUpConnectionString(sqlServerInstanceName, sqlDatabase);
            var queryString = $"SELECT AFASCOMPANYNAME AS 'AFASCOMPANYNAME' FROM X_R_CRMSETTINGS;";

            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                var reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        result = reader["AFASCOMPANYNAME"].ToString();
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return result;
        }

        private static bool ColumnAfasCompanyNumberExistsAtCrmSettings(string sqlServerInstanceName, string sqlDatabase)
        {
            var result = false;
            var connectionString = BuildUpConnectionString(sqlServerInstanceName, sqlDatabase);
            var pkMTableInfoCrmSettings = "F12F8694-BA9A-4BE2-A59E-0D6B028439CB";

            var queryString = $"SELECT COUNT(PK_M_COLUMNINFO) AS 'COLUMNAFASCOMPANYNUMBEREXISTS' FROM M_COLUMNINFO WHERE COLUMNNAME = 'AFASCOMPANYNAME' AND FK_TABLEINFO = '{pkMTableInfoCrmSettings}';";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                //command.Parameters.AddWithValue("@databaseName", sqlDatabase);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        if ($"{reader["COLUMNAFASCOMPANYNUMBEREXISTS"]}" != "0")
                        {
                            result = true;
                        }
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return result;
        }

        private static List<string> GetAllSqlDatabases(string sqlServerInstanceName)
        {
            var result = new List<string>();
            var connectionString = BuildUpConnectionString(sqlServerInstanceName, "master");
            var queryString = "SELECT [NAME] FROM sys.databases WHERE state_desc = 'ONLINE' ORDER BY [NAME];";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        result.Add($"{reader["NAME"]}");
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return result;
        }

        private static bool IsRidderAdministration(string sqlServerInstanceName, string sqlDatabase)
        {
            var result = false;
            var connectionString = BuildUpConnectionString(sqlServerInstanceName, "master");

            var queryString = $"SELECT COUNT([NAME]) AS 'RIDDEREXTENDEDPROPERTY' FROM [{sqlDatabase}].sys.extended_properties WHERE [NAME] = 'RidderCompanyName' AND [VALUE] != '';";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                //command.Parameters.AddWithValue("@databaseName", sqlDatabase);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    while (reader.Read())
                    {
                        if ($"{reader["RIDDEREXTENDEDPROPERTY"]}" != "0")
                        {
                            result = true;
                        }
                    }
                }
                finally
                {
                    // Always call Close when done reading.
                    reader.Close();
                }
            }

            return result;
        }

        private static string BuildUpConnectionString(string sqlServerInstanceName, string databaseName)
        {
            return $"Server={sqlServerInstanceName};Database={databaseName};User Id=bisharp;Password=Welkom@bisharp;;Trusted_Connection=True;MultipleActiveResultSets=true";
        }
    }
}
