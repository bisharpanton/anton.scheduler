﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ADODB;
using Anton.Scheduler.Plugins.AfasModels;
using Anton.Scheduler.Plugins.Models;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.FrameworkModels
{
    public static class RidderHelper
    {
        public static int WorkactivityTypeDiscontinuity = 8; //Adv is hernoemd

        public static void LinkEmployeeAvgAndPrivateData(SdkSession sdkSession, List<Employee> employeesRidder)
        {
            var employeeIds = employeesRidder.Select(x => x.Id).ToList();

            var employeesAvgData = sdkSession.Find<EmployeeAvgData>(x => employeeIds.Contains(x.FK_EMPLOYEE)).ToList();
            var employeesPrivateData = sdkSession.Find<EmployeePrivateData>(x => employeeIds.Contains(x.FK_EMPLOYEE)).ToList();

            foreach (var employee in employeesRidder)
            {
                employee.EmployeeAvgData = employeesAvgData.FirstOrDefault(x => x.FK_EMPLOYEE == employee.Id);
                employee.EmployeePrivateData = employeesPrivateData.FirstOrDefault(x => x.FK_EMPLOYEE == employee.Id);
            }
        }

        public static void CreateEmployeesInRidder(RidderLogger ridderLogger, string schedulerTask, string company, SdkSession sdkSession,
            List<AfasEmployee> employeesAfasToCreate, string defaultWageGroup, string afasStartUrl, string afasToken)
        {
            if (!employeesAfasToCreate.Any())
            {
                return;
            }

            var allFunctions = sdkSession.Find<Function>().ToList();
            var allCountries = sdkSession.Find<Country>().ToList();
            var allCaos = sdkSession.Find<Cao>().ToList();
            var wageGroup = sdkSession.Find<WageGroup>(x => x.CODE.Equals(defaultWageGroup)).FirstOrDefault();

            if(wageGroup == null)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Geen loongroep gevonden met code '{defaultWageGroup}'. Aanmaken werknemers niet mogelijk.");
                return;
            }

            for (var i = 0; i < employeesAfasToCreate.Count; i++)
            {
                var afasEmployee = employeesAfasToCreate[i];

                var rsEmployee = sdkSession.GetRecordsetColumns("R_EMPLOYEE", "", "PK_R_EMPLOYEE IS NULL");
                rsEmployee.UseDataChanges = true;
                rsEmployee.UpdateWhenMoveRecord = false;

                rsEmployee.AddNew();

                rsEmployee.SetFieldValue("CODE", afasEmployee.EmployeeId);

                MapPerson(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, rsEmployee, false);

                MapEmployeeFields(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, rsEmployee, false, allFunctions, allCaos);

                rsEmployee.SetFieldValue("FK_WAGEGROUP", wageGroup.Id);

                var updateResult = rsEmployee.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Aanmaken {afasEmployee.Recordlabel} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
                    continue;
                }

                var image = afasEmployee.ImageDateChanged.HasValue && afasEmployee.ImageId.HasValue
                    ? AfasHelper.GetImage(ridderLogger, afasStartUrl, schedulerTask, company, afasToken, afasEmployee.ImageId.Value)
                    : null;

                MapEmployeeAvgData(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, (int)rsEmployee.GetField("PK_R_EMPLOYEE").Value);
                MapEmployeePrivateData(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, (int)rsEmployee.GetField("PK_R_EMPLOYEE").Value, allCountries, image);

                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Werknemer {afasEmployee.Recordlabel} aangemaakt in Ridder iQ.");
            }
        }


        public static void UpdateEmployeesInRidder(RidderLogger ridderLogger, string schedulerTask, string company, SdkSession sdkSession, List<AfasEmployee> employeesAfasToUpdateInRidder,
            string afasStartUrl, string afasToken)
        {
            if (!employeesAfasToUpdateInRidder.Any())
            {
                return;
            }

            var allFunctions = sdkSession.Find<Function>().ToList();
            var allCountries = sdkSession.Find<Country>().ToList();
            var allCaos = sdkSession.Find<Cao>().ToList();

            foreach (var afasEmployee in employeesAfasToUpdateInRidder)
            {
                var rsEmployee = sdkSession.GetRecordsetColumns("R_EMPLOYEE", "",
                    $"CODE = '{afasEmployee.EmployeeId}'");
                rsEmployee.UseDataChanges = false;
                rsEmployee.UpdateWhenMoveRecord = false;

                rsEmployee.MoveFirst();

                var image = !(afasEmployee.ImageDateChanged ?? DateTime.MinValue).Equals((rsEmployee.GetField("AFASDATEPICTURECHANGED").Value as DateTime?) ?? DateTime.MinValue) && afasEmployee.ImageId.HasValue
                    ? AfasHelper.GetImage(ridderLogger, afasStartUrl, schedulerTask, company, afasToken, afasEmployee.ImageId.Value)
                    : null;

                MapPerson(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, rsEmployee, true);

                MapEmployeeFields(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, rsEmployee, true, allFunctions, allCaos);

                var updateResult = rsEmployee.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Bijwerken {afasEmployee.Recordlabel} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
                    continue;
                }

                MapEmployeeAvgData(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, (int)rsEmployee.GetField("PK_R_EMPLOYEE").Value);
                MapEmployeePrivateData(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, (int)rsEmployee.GetField("PK_R_EMPLOYEE").Value, allCountries, image);

                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Werknemer {afasEmployee.Recordlabel} bijgewerkt.");
            }
        }



        private static void MapEmployeePrivateData(RidderLogger ridderLogger, string schedulerTask, string company, SdkSession sdkSession, AfasEmployee afasEmployee, int employeeId, 
            List<Country> allCountries, byte[] image)
        {
            var rsEmployeePrivateData = sdkSession.GetRecordsetColumns("R_EMPLOYEEPRIVATEDATA", "", $"FK_EMPLOYEE = {employeeId}");

            if (rsEmployeePrivateData.RecordCount == 0)
            {
                rsEmployeePrivateData.AddNew();
                rsEmployeePrivateData.SetFieldValue("FK_EMPLOYEE", employeeId);

                //Maak automatisch het adres aan
                if (!string.IsNullOrEmpty(afasEmployee.Street))
                {
                    var createdAddresId = CreateNewAddres(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, allCountries);

                    if (createdAddresId == 0)
                    {
                        return;
                    }

                    rsEmployeePrivateData.SetFieldValue("FK_ADDRESS", createdAddresId);
                }
            }
            else
            {
                rsEmployeePrivateData.MoveFirst();
            }

            rsEmployeePrivateData.SetFieldValue("DATEOFBIRTH", (object)afasEmployee.DateOfBirth ?? DBNull.Value);
            rsEmployeePrivateData.SetFieldValue("CELLPHONE", (object)afasEmployee.MobilePrivate ?? DBNull.Value);
            rsEmployeePrivateData.SetFieldValue("PHONE1", (object)afasEmployee.PhonePrivate ?? DBNull.Value);
            rsEmployeePrivateData.SetFieldValue("EMAIL", (object)afasEmployee.EmailPrivate ?? DBNull.Value);

            rsEmployeePrivateData.SetFieldValue("DECEASED", afasEmployee.DateDeceased.HasValue);

            if(image != null)
                rsEmployeePrivateData.SetFieldValue("PICTURE", image);

            //Controleer of het adres afwijkt, in dat geval maak een nieuw adres aan en koppel deze
            var addressId = rsEmployeePrivateData.GetField("FK_ADDRESS").Value as int? ?? 0;
            var address = sdkSession.Find<Address>(x => x.Id == addressId).FirstOrDefault();
            if (address == null || !address.STREET.Equals(afasEmployee.Street) ||
                !address.HOUSENUMBER2.Equals(afasEmployee.HouseNumber.ToString()) ||
                !address.ADDITIONHOUSENUMBER.Equals(afasEmployee.AddNumber) ||
                !address.ZIPCODE.Equals(afasEmployee.ZIPCode) || !address.CITY.Equals(afasEmployee.City))
            {
                var createdAddresId = CreateNewAddres(ridderLogger, schedulerTask, company, sdkSession, afasEmployee, allCountries);

                if (createdAddresId == 0)
                {
                    return;
                }

                rsEmployeePrivateData.SetFieldValue("FK_ADDRESS", createdAddresId);
            }

            var updateResult = rsEmployeePrivateData.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Bijwerken werknemer privé gegevens bij werknemer {afasEmployee.Recordlabel} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }
        }

        

        private static int CreateNewAddres(RidderLogger ridderLogger, string schedulerTask, string company, SdkSession sdkSession, AfasEmployee afasEmployee, List<Country> allCountries)
        {
            var rsAddress = sdkSession.GetRecordsetColumns("R_ADDRESS", "", $"PK_R_ADDRESS IS NULL");

            rsAddress.AddNew();
            rsAddress.SetFieldValue("STREET", afasEmployee.Street);
            rsAddress.SetFieldValue("HOUSENUMBER2", afasEmployee.HouseNumber);
            rsAddress.SetFieldValue("ADDITIONHOUSENUMBER", afasEmployee.AddNumber);
            rsAddress.SetFieldValue("ZIPCODE", afasEmployee.ZIPCode);
            rsAddress.SetFieldValue("CITY", afasEmployee.City);

            var country = allCountries.FirstOrDefault(x => x.CODE.Equals(afasEmployee.Country.Trim(), StringComparison.OrdinalIgnoreCase));

            if (country == null)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Aanmaken nieuw adres bij werknemer {afasEmployee.Recordlabel} mislukt, oorzaak: land {afasEmployee.Country} is niet bekend in Ridder.");
                return 0;
            }

            rsAddress.SetFieldValue("FK_COUNTRY", country.Id);

            var updateResult = rsAddress.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Aanmaken nieuw adres bij werknemer {afasEmployee.Recordlabel} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }

            return (int)updateResult.First().PrimaryKey;
        }

        private static void MapEmployeeAvgData(RidderLogger ridderLogger, string schedulerTask, string company, SdkSession sdkSession, AfasEmployee afasEmployee, int employeeId)
        {
            var rsEmployeeAvgData = sdkSession.GetRecordsetColumns("R_EMPLOYEEGDPRSPECIALDATA", "", $"FK_EMPLOYEE = {employeeId}");

            if (rsEmployeeAvgData.RecordCount == 0)
            {
                rsEmployeeAvgData.AddNew();
                rsEmployeeAvgData.SetFieldValue("FK_EMPLOYEE", employeeId);
            }
            else
            {
                rsEmployeeAvgData.MoveFirst();
            }

            rsEmployeeAvgData.SetFieldValue("NATIONALIDNUMBER", afasEmployee.BSN);

            var updateResult = rsEmployeeAvgData.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Bijwerken werknemer AVG data bij werknemer {afasEmployee.Recordlabel} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }
        }

        private static void MapPerson(RidderLogger ridderLogger, string schedulerTask, string company, SdkSession sdkSession, AfasEmployee afasEmployee, ISdkRecordset rsEmployee, bool isUpdate)
        {
            if (isUpdate)
            {
                var personId = (int)rsEmployee.GetField("FK_PERSON").Value;

                var rsPerson = sdkSession.GetRecordsetColumns("R_PERSON", "", $"PK_R_PERSON = {personId}");
                rsPerson.UseDataChanges = false;
                rsPerson.UpdateWhenMoveRecord = false;

                rsPerson.MoveFirst();

                MapPersonFields(rsPerson, afasEmployee);

                var updateResult = rsPerson.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Bijwerken persoon {afasEmployee.Recordlabel} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
                }
            }
            else
            {
                var rsPerson = sdkSession.GetRecordsetColumns("R_PERSON", "", "PK_R_PERSON IS NULL");
                rsPerson.UseDataChanges = true;
                rsPerson.UpdateWhenMoveRecord = false;

                rsPerson.AddNew();

                MapPersonFields(rsPerson, afasEmployee);

                var updateResult = rsPerson.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Aanmaken persoon {afasEmployee.Recordlabel} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
                }

                rsEmployee.SetFieldValue("FK_PERSON", (int)updateResult.First().PrimaryKey);
            }
        }

        private static void MapPersonFields(ISdkRecordset rsPerson, AfasEmployee afasEmployee)
        {
            rsPerson.SetFieldValue("FIRSTNAME", afasEmployee.FirstName);
            rsPerson.SetFieldValue("INITIALS", afasEmployee.Initials);

            rsPerson.SetFieldValue("NAMEPREFIX", afasEmployee.NamePrefix);
            rsPerson.SetFieldValue("LASTNAME", afasEmployee.LastName);

            if (afasEmployee.Gender.Equals("V"))
            {
                rsPerson.SetFieldValue("GENDER", Gender.Vrouw);
            }
            else if (afasEmployee.Gender.Equals("M"))
            {
                rsPerson.SetFieldValue("GENDER", Gender.Man);
            }
            else
            {
                rsPerson.SetFieldValue("GENDER", Gender.Onbekend);
            }
        }

        private static void MapEmployeeFields(RidderLogger ridderLogger, string schedulerTask, string company, SdkSession sdkSession, AfasEmployee afasEmployee,
            ISdkRecordset rsEmployee, bool isUpdate, List<Function> allFunctions, List<Cao> allCaos)
        {
            if(!(rsEmployee.GetField("EMPLOYMENTSTARTDATE").Value as DateTime?).HasValue)
            {
                rsEmployee.SetFieldValue("EMPLOYMENTSTARTDATE", (object)afasEmployee.EmploymentStart ?? DBNull.Value);
            }

            rsEmployee.Fields["EMPLOYMENTTERMINATIONDATE"].Value = afasEmployee.EmploymentEnd.HasValue
                ? afasEmployee.EmploymentEnd.Value.AddDays(1)
                : (object)DBNull.Value;

            rsEmployee.SetFieldValue("EMAIL", afasEmployee.Mail);
            rsEmployee.SetFieldValue("PHONE", afasEmployee.Phone);
            rsEmployee.SetFieldValue("MOBILE", afasEmployee.Mobile);
            rsEmployee.SetFieldValue("UURWERKWEEK", afasEmployee.HourPerWeek);

            rsEmployee.Fields["EINDECONTRACTDATUM"].Value = afasEmployee.ContractEndDate ?? (object)DBNull.Value;

            var afasImageId = afasEmployee.ImageId ?? 0;
            rsEmployee.SetFieldValue("AFASIMAGEID", afasImageId);
            
            if(afasEmployee.ImageDateChanged.HasValue)
            {
                rsEmployee.SetFieldValue("AFASDATEPICTURECHANGED", afasEmployee.ImageDateChanged.Value);
            }
            else
            {
                rsEmployee.SetFieldValue("AFASDATEPICTURECHANGED", DBNull.Value);
            }

            var function = allFunctions.FirstOrDefault(x => x.CODE.Equals(afasEmployee.FunctionId.Trim(), StringComparison.OrdinalIgnoreCase));

            if (function == null)
            {
                var newFuction = new Function() { CODE = afasEmployee.FunctionId, DESCRIPTION = afasEmployee.FunctionDesc };

                try
                {
                    sdkSession.Insert(newFuction);
                }
                catch (Exception e)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error, $"Aanmaken functie '{newFuction.CODE} - {newFuction.DESCRIPTION}' in Ridder iQ mislukt, oorzaak: {e}");
                    return;
                }

                allFunctions.Add(newFuction);

                rsEmployee.SetFieldValue("FK_POSITION", newFuction.Id);
            }
            else
            {
                rsEmployee.SetFieldValue("FK_POSITION", function.Id);
            }

            var cao = allCaos.FirstOrDefault(x => x.AFASCAOCODE.Equals(afasEmployee.CaoCode, StringComparison.OrdinalIgnoreCase));

            if (cao == null)
            {
                var newCao = new Cao() { DESCRIPTION = afasEmployee.CaoDescription, AFASCAOCODE = afasEmployee.CaoCode };

                try
                {
                    sdkSession.Insert(newCao);
                }
                catch (Exception e)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error, $"Aanmaken cao '{newCao.DESCRIPTION}' in Ridder iQ mislukt, oorzaak: {e}");
                    return;
                }

                allCaos.Add(newCao);

                rsEmployee.SetFieldValue("FK_CAO", newCao.Id);
            }
            else
            {
                rsEmployee.SetFieldValue("FK_CAO", cao.Id);
            }
        }

        internal static List<Employee> GetEmployees(SdkSession sdkSession, List<AfasEmployee> employeesAfas)
        {
            var positionIdDirecteur = sdkSession.Find<Function>("DESCRIPTION = 'Directeur'").FirstOrDefault()?.Id ?? 0;

            return sdkSession
                .Find<Employee>($"CODE IN ('{string.Join("','", employeesAfas.Select(x => x.EmployeeId))}') AND FK_POSITION <> {positionIdDirecteur}")
                .ToList();
        }

        internal static List<ProjectTime> GetLeaves(RidderLogger ridderLogger, SdkSession sdkSession, List<Workactivity> workactivitiesWithAfasAbsenceCode, 
            DateTime startInterval, DateTime endInterval, List<Employee> ridderEmployees, long minimalTime)
        {
            //We willen geen uren ophalen van voor 1-1-2022. Dit is de start van live gang met AFAS
            if(startInterval < new DateTime(2022,1,1))
            {
                startInterval = new DateTime(2022, 1, 1);
            }

            if(!ridderEmployees.Any())
            {
                return new List<ProjectTime>();
            }

            var workactivityIds = workactivitiesWithAfasAbsenceCode.Select(x => x.Id).ToList();

            var projectTimes = sdkSession
                .Find<ProjectTime>($"FK_EMPLOYEE IN ({string.Join(",", ridderEmployees.Select(x => x.Id))}) AND FK_WORKACTIVITY IN ({string.Join(",", workactivityIds)}) AND DATE >= '{startInterval:yyyyMMdd}' AND DATE < '{endInterval.AddDays(1):yyyyMMdd}' AND CLOSED = 1 AND (TIMEEMPLOYEE <= {minimalTime * -1} OR TIMEEMPLOYEE >= {minimalTime})")
                .ToList();

            if(!projectTimes.Any())
            {
                return projectTimes;
            }

            var result = new List<ProjectTime>();

            var listProjectTimesGrouped = projectTimes.GroupBy(x => x.GroupByEmployeeDateWorkactivity).ToList();

            foreach (var projectTimesGrouped in listProjectTimesGrouped)
            {
                var newProjectTime = new ProjectTime();

                var workactivity = workactivitiesWithAfasAbsenceCode.First(x => x.Id == projectTimesGrouped.First().FK_WORKACTIVITY);
                var employee = ridderEmployees.First(x => x.Id == projectTimesGrouped.First().FK_EMPLOYEE);

                newProjectTime.FK_EMPLOYEE = projectTimesGrouped.First().FK_EMPLOYEE;
                newProjectTime.DATE = projectTimesGrouped.First().DATE;
                newProjectTime.FK_WORKACTIVITY = projectTimesGrouped.First().FK_WORKACTIVITY;

                //Bij opnemen discontinuiteit nemen we niet de volledige 8 uur op van het saldo
                newProjectTime.TIMEEMPLOYEE = (int)workactivity.WORKACTIVITYTYPE == WorkactivityTypeDiscontinuity
                    ? newProjectTime.TIMEEMPLOYEE = projectTimesGrouped.Sum(x => x.TAKEDISCONTINUITY ?? 0)
                    : newProjectTime.TIMEEMPLOYEE = projectTimesGrouped.Sum(x => x.TIMEEMPLOYEE);

                newProjectTime.TIMEOFINLIEU = projectTimesGrouped.Sum(x => x.TIMEOFINLIEU);
                newProjectTime.BUILDUPDISCONTINUITY = projectTimesGrouped.Sum(x => x.BUILDUPDISCONTINUITY);
                newProjectTime.TAKEDISCONTINUITY = projectTimesGrouped.Sum(x => x.TAKEDISCONTINUITY);

                newProjectTime.EmployeeCode = employee.CODE;
                newProjectTime.AfasAbsenceCode = workactivity.AFASABSENCECODE;

                newProjectTime.UniqueKey = $"{employee.CODE}_{newProjectTime.DATE.Date:yyyyMMdd}_{workactivity.AFASABSENCECODE}_{newProjectTime.TimeEmployeeInMinutes}";

                result.Add(newProjectTime);
            }

            return result;
        }

        internal static List<ProjectTime> GetTvtOverTimes(RidderLogger ridderLogger, SdkSession sdkSession, 
            DateTime startInterval, DateTime endInterval, List<Employee> ridderEmployees)
        {
            //We willen geen uren ophalen van voor 1-1-2022. Dit is de start van live gang met AFAS
            if (startInterval < new DateTime(2022, 1, 1))
            {
                startInterval = new DateTime(2022, 1, 1);
            }

            if (!ridderEmployees.Any())
            {
                return new List<ProjectTime>();
            }

            var projectTimes = sdkSession
                .Find<ProjectTime>($"FK_EMPLOYEE IN ({string.Join(",", ridderEmployees.Select(x => x.Id))}) AND FK_OVERTIMECODE IS NOT NULL AND TIMEOFINLIEU > 0 AND DATE >= '{startInterval:yyyyMMdd}' AND DATE < '{endInterval.AddDays(1):yyyyMMdd}' AND CLOSED = 1")
                .ToList();

            if (!projectTimes.Any())
            {
                return projectTimes;
            }

            
            var result = new List<ProjectTime>();

            //Group projecttimes by employee & date
            var listProjectTimesGrouped = projectTimes.GroupBy(x => x.GroupByEmployeeDate).ToList();

            foreach (var projectTimesGrouped in listProjectTimesGrouped)
            {
                var employee = ridderEmployees.First(x => x.Id == projectTimesGrouped.First().FK_EMPLOYEE);

                result.Add(new ProjectTime()
                {
                    FK_EMPLOYEE = projectTimesGrouped.First().FK_EMPLOYEE,
                    EmployeeCode = employee.CODE,
                    DATE = projectTimesGrouped.First().DATE,
                    TIMEEMPLOYEE = projectTimesGrouped.Sum(x => x.TIMEEMPLOYEE),
                    TIMEOFINLIEU = projectTimesGrouped.Sum(x => x.TIMEOFINLIEU.Value),
                    UniqueKey = $"{employee.CODE}_{projectTimesGrouped.First().DATE.Date:yyyyMMdd}" 
                });
            }


            return result;
        }

        internal static List<ProjectTime> GetDiscontinuiteitOverTimes(RidderLogger ridderLogger, SdkSession sdkSession,
                DateTime startInterval, DateTime endInterval, List<Employee> ridderEmployees)
        {
            //We willen geen uren ophalen van voor 1-1-2022. Dit is de start van live gang met AFAS
            if (startInterval < new DateTime(2022, 1, 1))
            {
                startInterval = new DateTime(2022, 1, 1);
            }

            if (!ridderEmployees.Any())
            {
                return new List<ProjectTime>();
            }

            var projectTimes = sdkSession
                .Find<ProjectTime>($"FK_EMPLOYEE IN ({string.Join(",", ridderEmployees.Select(x => x.Id))}) AND FK_OVERTIMECODE IS NOT NULL AND BUILDUPDISCONTINUITY > 0 AND DATE >= '{startInterval:yyyyMMdd}' AND DATE < '{endInterval.AddDays(1):yyyyMMdd}' AND CLOSED = 1")
                .ToList();

            if (!projectTimes.Any())
            {
                return projectTimes;
            }


            var result = new List<ProjectTime>();

            //Group projecttimes by employee & date
            var listProjectTimesGrouped = projectTimes.GroupBy(x => x.GroupByEmployeeDate).ToList();

            foreach (var projectTimesGrouped in listProjectTimesGrouped)
            {
                var employee = ridderEmployees.First(x => x.Id == projectTimesGrouped.First().FK_EMPLOYEE);

                result.Add(new ProjectTime()
                {
                    FK_EMPLOYEE = projectTimesGrouped.First().FK_EMPLOYEE,
                    EmployeeCode = employee.CODE,
                    DATE = projectTimesGrouped.First().DATE,
                    TIMEEMPLOYEE = projectTimesGrouped.Sum(x => x.TIMEEMPLOYEE),
                    BUILDUPDISCONTINUITY = projectTimesGrouped.Sum(x => x.BUILDUPDISCONTINUITY.Value),
                    UniqueKey = $"{employee.CODE}_{projectTimesGrouped.First().DATE.Date:yyyyMMdd}"
                });
            }


            return result;
        }

        internal static List<TimeTableOffSet> GetActualTimeTableRidder(SdkSession sdkSessionCompany, List<Employee> employees)
        {
            var timeTableOffsets = sdkSessionCompany.Find<TimeTableOffSet>().ToList();
            var timeTables = sdkSessionCompany.Find<TimeTable>().ToList();
            var timeTableWorkdays = sdkSessionCompany.Find<TimeTableWorkday>().ToList();
            var workdays = sdkSessionCompany.Find<Workday>().ToList();
            var workdayOvertimes = sdkSessionCompany.Find<WorkdayOvertime>().ToList();
            var timeCodes = sdkSessionCompany.Find<TimeCode>().ToList();

            workdayOvertimes.ForEach(x => x.TimeCode = timeCodes.First(y => y.Id == x.FK_TIMECODE));
            workdays.ForEach(x => x.WorkdayOverTimes = workdayOvertimes.Where(y => y.FK_WORKDAY == x.Id).ToList());

            timeTableWorkdays.ForEach(x => x.Workday = workdays.First(y => y.Id == x.FK_WORKDAY));
            timeTables.ForEach(x => x.TimeTableWorkdays = timeTableWorkdays.Where(y => y.FK_TIMETABLE == x.Id).ToList());

            foreach (var timeTableOffset in timeTableOffsets)
            {
                timeTableOffset.TimeTable = timeTables.First(y => y.Id == timeTableOffset.FK_TIMETABLE);
                timeTableOffset.Employee = employees.First(y => y.Id == timeTableOffset.FK_EMPLOYEE);
            }


            var result = new List<TimeTableOffSet>();
            var employeeIds = timeTableOffsets.Select(x => x.FK_EMPLOYEE).Distinct().ToList();

            foreach (var employeeId in employeeIds)
            {
                var timeTablesThisEmployee = timeTableOffsets.Where(x => x.FK_EMPLOYEE == employeeId).ToList();

                if(!timeTablesThisEmployee.Any())
                {
                    continue;
                }

                var actualTimeTable = timeTablesThisEmployee.OrderByDescending(x => x.STARTDATE).First();

                result.Add(actualTimeTable);
            }

            return result;
        }

        internal static void CreateNewTimeTables(RidderLogger ridderLogger, string SchedulerTask, string company, SdkSession sdkSessionCompany,
            List<Employee> employeesToUpdateTimeTable, List<AfasTimeTable> timeTablesAfas)
        {
            if(!employeesToUpdateTimeTable.Any())
            {
                return;
            }

            var projectTimes = sdkSessionCompany.GetRecordsetColumns("R_PROJECTTIME", "PK_R_PROJECTTIME, FK_EMPLOYEE, DATE",
                $"FK_EMPLOYEE IN ({string.Join(",", employeesToUpdateTimeTable.Select(x => x.Id))}) AND DATE >= '{DateTime.Now.AddYears(-1):yyyy-MM-dd}'")
                .DataTable.AsEnumerable().ToList();

            var rsTimeTable = sdkSessionCompany.GetRecordsetColumns("R_TIMETABLE", "", $"PK_R_TIMETABLE = -1");
            var timeTableWorkdays = sdkSessionCompany.GetRecordsetColumns("R_TIMETABLEWORKDAY", "", $"PK_R_TIMETABLEWORKDAY = -1");
            var workdays = sdkSessionCompany.GetRecordsetColumns("R_WORKDAY", "", $"PK_R_WORKDAY = -1");


            foreach (var employee in employeesToUpdateTimeTable)
            {
                var projectTimesThisEmployee = projectTimes.Where(x => x.Field<int>("FK_EMPLOYEE") == employee.Id).ToList();
                var projectTimesFromToday = projectTimesThisEmployee.Where(x => x.Field<DateTime>("DATE").Date >= DateTime.Now.Date).ToList();

                if (projectTimesThisEmployee.Any(x => x.Field<DateTime>("DATE").Date > DateTime.Now.Date))
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Werknemer {employee.Person.RecordLabel} heeft al uren geboekt in de toekomst. Rooster bijwerken is (nog) niet mogelijk.");
                    continue;
                }

                var startDateNewTimeTable = !projectTimesThisEmployee.Any() && employee.EMPLOYMENTSTARTDATE.HasValue
                    ? employee.EMPLOYMENTSTARTDATE.Value      //Bij nieuwe werknemers pak datum in dienst als start
                    : (projectTimesThisEmployee.Any(x => x.Field<DateTime>("DATE").Date == DateTime.Now.Date)
                            ? DateTime.Now.Date.AddDays(1)
                            : DateTime.Now.Date);

                var afasTimeTable = timeTablesAfas.FirstOrDefault(x => x.Medewerker.Equals(employee.CODE));

                if(afasTimeTable == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Bij werknemer {employee.Person.RecordLabel} is geen AFAS rooster gevonden bij het verwerken naar Ridder. Dit is gek. Contacteer Bisharp.");
                    continue;
                }

                var timeTable = GetOrCreateTimeTableForThisEmployee(ridderLogger, SchedulerTask, company, sdkSessionCompany, employee, afasTimeTable);

                if(timeTable == null)
                {
                    continue;
                }

                if (timeTable.TimeTableWorkdays.Count == 0)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Bij werknemer {employee.Person.RecordLabel} is rooster {timeTable.DESCRIPTION} gevonden, maar dit rooster heeft geen werkdagen.");
                    continue;
                }

                var startSequenceInTimeTable = timeTable.TimeTableWorkdays.Count == 1
                    ? 0
                    : (startDateNewTimeTable.DayOfWeek == DayOfWeek.Sunday ? 6 : (int)startDateNewTimeTable.DayOfWeek - 1);

                var rsTimeTableOffset = sdkSessionCompany.GetRecordsetColumns("R_TIMETABLEOFFSET", "", $"PK_R_TIMETABLEOFFSET = -1");
                rsTimeTableOffset.UseDataChanges = true;

                rsTimeTableOffset.AddNew();
                rsTimeTableOffset.SetFieldValue("FK_EMPLOYEE", employee.Id);
                rsTimeTableOffset.SetFieldValue("FK_TIMETABLE", timeTable.Id);
                rsTimeTableOffset.SetFieldValue("STARTDATE", startDateNewTimeTable);

                rsTimeTableOffset.SetFieldValue("FK_TIMETABLEWORKDAY", timeTable.TimeTableWorkdays[startSequenceInTimeTable].Id);

                var updateResult = rsTimeTableOffset.Update2();

                if (updateResult.Any(x => x.HasError))
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Opslaan nieuwe roosters bij werknemer {employee.Person.RecordLabel} is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
                }
            }

        }

        private static TimeTable GetOrCreateTimeTableForThisEmployee(RidderLogger ridderLogger, string SchedulerTask, string company, 
            SdkSession sdkSessionCompany, Employee employee, AfasTimeTable afasTimeTable)
        {
            var timeTables = sdkSessionCompany.Find<TimeTable>().ToList();
            var timeTableWorkdays = sdkSessionCompany.Find<TimeTableWorkday>().ToList();
            timeTables.ForEach(x => x.TimeTableWorkdays = timeTableWorkdays.Where(y => y.FK_TIMETABLE == x.Id).ToList());

            if(afasTimeTable.Type_rooster.Equals("Uren per week"))
            {
                var timeTableFlex = timeTables.FirstOrDefault(x => x.CODE.Equals("FLEX"));

                if(timeTableFlex == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Geen FLEX rooster gevonden. Rooster koppelen bij werknemer {employee.Person.RecordLabel} mislukt.");
                    return null;
                }

                return timeTableFlex;
            }

            var workdaysNewTimeTable = DetermineWorkdaysForThisEmployee(ridderLogger, SchedulerTask, company, sdkSessionCompany, employee, afasTimeTable);
            
            if (workdaysNewTimeTable == null)
            {
                return null;
            }

            var existingTimeTableWithSameDays = timeTables.FirstOrDefault(x => x.TimeTableWorkdays.Count == 7 && x.TimeTableWorkdays[0].FK_WORKDAY == workdaysNewTimeTable[0] && 
                                        x.TimeTableWorkdays[1].FK_WORKDAY == workdaysNewTimeTable[1] && x.TimeTableWorkdays[2].FK_WORKDAY == workdaysNewTimeTable[2] && 
                                        x.TimeTableWorkdays[3].FK_WORKDAY == workdaysNewTimeTable[3] && x.TimeTableWorkdays[4].FK_WORKDAY == workdaysNewTimeTable[4] && 
                                        x.TimeTableWorkdays[5].FK_WORKDAY == workdaysNewTimeTable[5] && x.TimeTableWorkdays[6].FK_WORKDAY == workdaysNewTimeTable[6]);

            if (existingTimeTableWithSameDays != null)
            {
                return existingTimeTableWithSameDays;
            }


            var numberOfPartTimeTimetables = sdkSessionCompany.GetRecordsetColumns("R_TIMETABLE", "PK_R_TIMETABLE",
                $"CODE LIKE 'PT%'").RecordCount;

            var sMa = afasTimeTable.Uren_Maandag == null ? "0" : Math.Abs(Math.Round(afasTimeTable.Uren_Maandag.Value) - afasTimeTable.Uren_Maandag.Value) < 0.001 ? $"{afasTimeTable.Uren_Maandag.Value:n0}" : $"{afasTimeTable.Uren_Maandag.Value:n2}";
            var sDi = afasTimeTable.Uren_Dinsdag == null ? "0" : Math.Abs(Math.Round(afasTimeTable.Uren_Dinsdag.Value) - afasTimeTable.Uren_Dinsdag.Value) < 0.001 ? $"{afasTimeTable.Uren_Dinsdag.Value:n0}" : $"{afasTimeTable.Uren_Dinsdag.Value:n2}";
            var sWo = afasTimeTable.Uren_Woensdag == null ? "0" : Math.Abs(Math.Round(afasTimeTable.Uren_Woensdag.Value) - afasTimeTable.Uren_Woensdag.Value) < 0.001 ? $"{afasTimeTable.Uren_Woensdag.Value:n0}" : $"{afasTimeTable.Uren_Woensdag.Value:n2}";
            var sDo = afasTimeTable.Uren_Donderdag == null ? "0" : Math.Abs(Math.Round(afasTimeTable.Uren_Donderdag.Value) - afasTimeTable.Uren_Donderdag.Value) < 0.001 ? $"{afasTimeTable.Uren_Donderdag.Value:n0}" : $"{afasTimeTable.Uren_Donderdag.Value:n2}";
            var sVr = afasTimeTable.Uren_Vrijdag == null ? "0" : Math.Abs(Math.Round(afasTimeTable.Uren_Vrijdag.Value) - afasTimeTable.Uren_Vrijdag.Value) < 0.001 ? $"{afasTimeTable.Uren_Vrijdag.Value:n0}" : $"{afasTimeTable.Uren_Vrijdag.Value:n2}";

            var rsTimeTable = sdkSessionCompany.GetRecordsetColumns("R_TIMETABLE", "", $"PK_R_TIMETABLE = -1");
            rsTimeTable.AddNew();
            rsTimeTable.SetFieldValue("CODE", $"PT-{numberOfPartTimeTimetables + 1}");
            rsTimeTable.SetFieldValue("DESCRIPTION", $"Parttime rooster; ma {sMa}, di {sDi}, wo {sWo}, do {sDo}, vr {sVr}");

            var updateResult = rsTimeTable.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Aanmaken nieuw roosters is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
                return null;
            }

            var createdTimeTableId = (int)updateResult.First().PrimaryKey;

            var rsTimeTableWorkdays = sdkSessionCompany.GetRecordsetColumns("R_TIMETABLEWORKDAY", "", $"PK_R_TIMETABLEWORKDAY = -1");
            rsTimeTableWorkdays.UseDataChanges = false;
            rsTimeTableWorkdays.UpdateWhenMoveRecord = false;

            for (int i = 0; i < 7; i++)
            {
                rsTimeTableWorkdays.AddNew();
                rsTimeTableWorkdays.SetFieldValue("FK_TIMETABLE", createdTimeTableId);
                rsTimeTableWorkdays.SetFieldValue("FK_WORKDAY", workdaysNewTimeTable[i]);
            }

            var updateResult2 = rsTimeTableWorkdays.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Aanmaken rooster werkdagen is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                return null;
            }

            var createdTimeTable = sdkSessionCompany.Find<TimeTable>(x => x.Id == createdTimeTableId).First();
            var createdTimeTableWorkdays = sdkSessionCompany.Find<TimeTableWorkday>(x => x.FK_TIMETABLE == createdTimeTableId).ToList();
            createdTimeTable.TimeTableWorkdays = createdTimeTableWorkdays;

            //Zet rooster klaar voor gebruik
            var wfSetReadyForUse = new Guid("1f434d36-c35c-493f-9d02-337ff49bdcd8");
            var wfResult = sdkSessionCompany.Sdk.ExecuteWorkflowEvent("R_TIMETABLE", createdTimeTableId, wfSetReadyForUse);

            if(wfResult.HasError)
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Rooster klaar zetten voor gebruik is mislukt, oorzaak: {wfResult.GetResult()}");
                return null;
            }

            return createdTimeTable;
        }

        private static int[] DetermineWorkdaysForThisEmployee(RidderLogger ridderLogger, string SchedulerTask, string company,
            SdkSession sdkSessionCompany, Employee employee, AfasTimeTable afasTimeTable)
        {
            var result = new int[7];

            var wfIdGetOrCreateWorkday = ConfigurationManager.AppSettings["WorkflowIdGetOrCreateWorkday"].ToString();

            if (string.IsNullOrEmpty(wfIdGetOrCreateWorkday))
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Geen workflow id GetOrCreateWorkday gevonden in de app settings. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                return null;
            }

            var wfGetOrCreateWorkday = new Guid(wfIdGetOrCreateWorkday);

            //Maandag
            try
            {
                var wfParMa = new Dictionary<string, object>();
                wfParMa.Add("Hours", afasTimeTable.Uren_Maandag ?? 0.0);
                var wfResultMa = sdkSessionCompany.Sdk.ExecuteWorkflowEvent("R_EMPLOYEE", employee.Id, wfGetOrCreateWorkday, wfParMa);

                if (wfResultMa.HasError || !wfResultMa.Messages.Any())
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Opvragen werkdag maandag is mislukt, oorzaak: {wfResultMa.GetResult()}. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[0] = Convert.ToInt32(wfResultMa.Messages.Select(x => Regex.Match(x.Message, @"\d+").Value).First());
            }
            catch (Exception e)
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Uitvoeren workflow bepaling werkdag bij werknemers {employee.Person.RecordLabel} is mislukt, oorzaak {e.Message}.");
                return null;
            }

            //Dinsdag
            try
            {
                var wfParDi = new Dictionary<string, object>();
                wfParDi.Add("Hours", afasTimeTable.Uren_Dinsdag ?? 0.0);
                var wfResultDi = sdkSessionCompany.Sdk.ExecuteWorkflowEvent("R_EMPLOYEE", employee.Id, wfGetOrCreateWorkday, wfParDi);

                if (wfResultDi.HasError || !wfResultDi.Messages.Any())
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Opvragen werkdag dinsdag is mislukt, oorzaak: {wfResultDi.GetResult()}. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[1] = Convert.ToInt32(wfResultDi.Messages.Select(x => Regex.Match(x.Message, @"\d+").Value).First());
            }
            catch (Exception e)
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Uitvoeren workflow bepaling werkdag bij werknemers {employee.Person.RecordLabel} is mislukt, oorzaak {e.Message}.");
                return null;
            }

            //Woensdag
            try
            {
                var wfParWo = new Dictionary<string, object>();
                wfParWo.Add("Hours", afasTimeTable.Uren_Woensdag ?? 0.0);
                var wfResultWo = sdkSessionCompany.Sdk.ExecuteWorkflowEvent("R_EMPLOYEE", employee.Id, wfGetOrCreateWorkday, wfParWo);

                if (wfResultWo.HasError || !wfResultWo.Messages.Any())
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Opvragen werkdag woensdag is mislukt, oorzaak: {wfResultWo.GetResult()}. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[2] = Convert.ToInt32(wfResultWo.Messages.Select(x => Regex.Match(x.Message, @"\d+").Value).First());
            }
            catch (Exception e)
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Uitvoeren workflow bepaling werkdag bij werknemers {employee.Person.RecordLabel} is mislukt, oorzaak {e.Message}.");
                return null;
            }

            //Donderdag
            try
            {
                var wfParDo = new Dictionary<string, object>();
                wfParDo.Add("Hours", afasTimeTable.Uren_Donderdag ?? 0.0);
                var wfResultDo = sdkSessionCompany.Sdk.ExecuteWorkflowEvent("R_EMPLOYEE", employee.Id, wfGetOrCreateWorkday, wfParDo);

                if (wfResultDo.HasError || !wfResultDo.Messages.Any())
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Opvragen werkdag donderdag is mislukt, oorzaak: {wfResultDo.GetResult()}. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[3] = Convert.ToInt32(wfResultDo.Messages.Select(x => Regex.Match(x.Message, @"\d+").Value).First());
            }
            catch (Exception e)
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Uitvoeren workflow bepaling werkdag bij werknemers {employee.Person.RecordLabel} is mislukt, oorzaak {e.Message}.");
                return null;
            }
            //Vrijdag
            try
            {
                var wfParVr = new Dictionary<string, object>();
                wfParVr.Add("Hours", afasTimeTable.Uren_Vrijdag ?? 0.0);
                var wfResultVr = sdkSessionCompany.Sdk.ExecuteWorkflowEvent("R_EMPLOYEE", employee.Id, wfGetOrCreateWorkday, wfParVr);

                if (wfResultVr.HasError || !wfResultVr.Messages.Any())
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Opvragen werkdag vrijdag is mislukt, oorzaak: {wfResultVr.GetResult()}. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[4] = Convert.ToInt32(wfResultVr.Messages.Select(x => Regex.Match(x.Message, @"\d+").Value).First());
            }
            catch (Exception e)
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Uitvoeren workflow bepaling werkdag bij werknemers {employee.Person.RecordLabel} is mislukt, oorzaak {e.Message}.");
                return null;
            }

            var workdays = sdkSessionCompany.Find<Workday>().ToList();

            //Zaterdag & zondag
            if (employee.Cao.DESCRIPTION.Equals("Basis CAO"))
            {
                var workdayWeekendNorm = workdays.FirstOrDefault(x => x.CODE.Equals("WKD NORM"));

                if (workdayWeekendNorm == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Geen werkdag 'WKD NORM' gevonden. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[5] = workdayWeekendNorm.Id;
                result[6] = workdayWeekendNorm.Id;
            }
            else if (employee.Cao.DESCRIPTION.Equals("Metaal"))
            {
                var workdayMetaalZaterdag = workdays.FirstOrDefault(x => x.CODE.Equals("ZATERDAG"));

                if (workdayMetaalZaterdag == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Geen werkdag 'ZATERDAG' gevonden. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                var workdayMetaalZondag = workdays.FirstOrDefault(x => x.CODE.Equals("ZONDAG"));

                if (workdayMetaalZondag == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Geen werkdag 'ZONDAG' gevonden. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[5] = workdayMetaalZaterdag.Id;
                result[6] = workdayMetaalZondag.Id;
            }
            else if (employee.Cao.DESCRIPTION.Equals("Bouw"))
            {
                var workdayBouwZaterdag = workdays.FirstOrDefault(x => x.CODE.Equals("ZATERDAG B"));

                if (workdayBouwZaterdag == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Geen werkdag 'ZATERDAG B' gevonden. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                var workdayBouwZondag = workdays.FirstOrDefault(x => x.CODE.Equals("ZONDAG B"));

                if (workdayBouwZondag == null)
                {
                    ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"Geen werkdag 'ZONDAG B' gevonden. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                    return null;
                }

                result[5] = workdayBouwZaterdag.Id;
                result[6] = workdayBouwZondag.Id;
            }
            else
            {
                ridderLogger.Log(SchedulerTask, company, SchedulerLogLevel.Error, $"CAO '{employee.Cao.DESCRIPTION}' geen bekende CAO binnen de Anton Groep. Rooster aanmaken voor {employee.Person.RecordLabel} is misluk.");
                return null;
            }

            return result;
        }
    }
}
