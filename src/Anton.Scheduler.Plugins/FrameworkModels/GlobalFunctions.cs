﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.Models;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.FrameworkModels
{
    public static class GlobalFunctions
    {
        internal static SdkSession GetSdkSession(RidderLogger ridderLogger, SdkSession defaultSdkSession, string schedulerTask, string company)
        {
            ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Inloggen in {company}...");

            if (company.Equals(defaultSdkSession.Configuration.CompanyName))
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Ingelogd in {company}");
                return defaultSdkSession;
            }

            var otherCompanySdkSession = new SdkSession(new SdkConfiguration()
            {
                CompanyName = company,
                UserName = defaultSdkSession.Configuration.UserName,
                Password = defaultSdkSession.Configuration.Password,
                PersistSession = defaultSdkSession.Configuration.PersistSession,
                RidderIQClientPath = defaultSdkSession.Configuration.RidderIQClientPath,
            })
            { AutoLogin = false };

            try
            {
                otherCompanySdkSession.Login();
            }
            catch (Exception e)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Inloggen {company} mislukt, oorzaak: {e}");
                return null;
            }
            ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Ingelogd in {company}");

            return otherCompanySdkSession;
        }

        public static List<List<T>> SplitList<T>(List<T> list, int batchSize)
        {
            var result = new List<List<T>>();

            for (int i = 0; i < list.Count; i += batchSize)
            {
                result.Add(list.GetRange(i, Math.Min(batchSize, list.Count - i)).ToList());
            }

            return result;
        }
    }
}
