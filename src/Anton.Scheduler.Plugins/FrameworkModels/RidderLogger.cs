﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.Models;
using Ridder.Client.SDK.Extensions;

namespace Anton.Scheduler.Plugins.FrameworkModels
{
    public class RidderLogger
    {
        private readonly SdkSession _sdkSession;

        public RidderLogger(SdkSession sdkSession)
        {
            _sdkSession = sdkSession;
        }

        public void Log(string schedulerTask, string ridderCompany, SchedulerLogLevel logLevel, string logMessage)
        {
            Console.ForegroundColor = logLevel == SchedulerLogLevel.Error ? ConsoleColor.Red : ConsoleColor.Yellow;
            Console.WriteLine(logMessage);
            Console.ForegroundColor = ConsoleColor.Gray; //Back to default

            var logRecord = new LogRecord() 
            {
                TASK = schedulerTask,
                COMPANY = ridderCompany, 
                LOGLEVEL = logLevel, 
                LOG = logMessage,
            };

            _sdkSession.Insert(logRecord);
        }
    }
}
