﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.BfwModels;
using Anton.Scheduler.Plugins.Models;
using Npgsql;

namespace Anton.Scheduler.Plugins.FrameworkModels
{
    public static class BfwHelper
    {
        public static List<int> GetShipmentIds(RidderLogger logger, DateTime dateLastImport, string schedulerTask)
        {
            var query = "SELECT shipmentid, leverdatum " +
                        "FROM shiphdr " +
                        $"WHERE boekdatum >= '{dateLastImport:yyyy-MM-dd}'";
            var result = ExecuteQuery(logger, query, schedulerTask);

            if(result == null)
            {
                return new List<int>();
            }

            return result.Select(x => x.Field<int>("shipmentid")).ToList();
        }

        internal static List<ShippingHeader> GetShippingHeaderInfo(RidderLogger logger, List<int> shipmentsToImport, string schedulerTask)
        {
            var queryGetShipmentHeaders = "SELECT shipmentid, werk_nr, leverdatum, boekdatum, externalid " +
                        "FROM shiphdr " +
                        $"WHERE shipmentid in ({string.Join(",", shipmentsToImport)})";
            var resultGetShipmentHeaders = ExecuteQuery(logger, queryGetShipmentHeaders, schedulerTask);

            if(resultGetShipmentHeaders == null)
            {
                return new List<ShippingHeader>();
            }

            var shippingHeaders = resultGetShipmentHeaders.Select(x => new ShippingHeader()
            {
                ShipmentId = (int)x["shipmentid"],
                WerkNr = (int)x["werk_nr"],
                Leverdatum = (DateTime)x["leverdatum"],
                Boekdatum = (DateTime)x["boekdatum"],
                ExternalId = (string)x["externalid"],
            }).ToList();

            //Als het aantal nul is dan gaat het OHW aanmaken fout. Haal regels met aantal 0 niet op
            var queryGetStockTransactions = "SELECT code_mat, globalid, transtype, transdate, amount " +
                        "FROM stcktrns " +
                        $"WHERE globalid in ({string.Join(",", shipmentsToImport)}) AND transsrc = 7 AND amount <> 0";
            var resultGetStockTransactions = ExecuteQuery(logger, queryGetStockTransactions, schedulerTask);

            if (queryGetStockTransactions == null)
            {
                return new List<ShippingHeader>();
            }

            var stockTransactions = resultGetStockTransactions.Select(x => new StockTransactions()
            {
                CodeMaterial = (string)x["code_mat"],
                GlobalId = (int)x["globalid"],
                TransactionType = Convert.ToInt16(x["transtype"]),
                TransactionDate = (DateTime)x["transdate"],
                Amount = (long)x["amount"],
            }).ToList();

            var queryGetItems = "SELECT code_mat, ext_id " +
                        "FROM vrd_stam " +
                        $"WHERE code_mat in ('{string.Join("','", stockTransactions.Select(x => x.CodeMaterial).Distinct())}')";
            var resultGetItems = ExecuteQuery(logger, queryGetItems, schedulerTask);

            if (resultGetItems == null)
            {
                return new List<ShippingHeader>();
            }

            var items = resultGetItems.Select(x => new BfwModels.BfwItem()
            {
                CodeMaterial = (string)x["code_mat"],
                ExternalId = (string)x["ext_id"],
            }).ToList();


            stockTransactions.ForEach(x => x.Item = items.First(y => y.CodeMaterial.Equals(x.CodeMaterial)));
            shippingHeaders.ForEach(x => x.StockTransactions = stockTransactions.Where(y => y.GlobalId == x.ShipmentId).ToList());


            return shippingHeaders;
        }

        internal static List<Fase> GetFasen(RidderLogger logger, string schedulerTask, List<Order> orders)
        {
            var query =
                "SELECT werk_nr, fase, omschr " +
                "FROM fasering " +
                $"WHERE werk_nr IN ({string.Join(",", orders.Select(x => x.ORDERNUMBER))});";

            var result = ExecuteQuery(logger, query, schedulerTask);

            if (result == null)
            {
                return new List<Fase>();
            }

            return result.Select(x => new Fase()
            {
                BfwWerkNr = Convert.ToInt32(x["werk_nr"]),
                Code = (string)x["fase"],
                Description = (string)x["omschr"],
                UniqueKey = $"{x["werk_nr"]}_{x["fase"]}",
            }).ToList();
        }

        internal static List<Buigstaat> GetBuigstaten(RidderLogger logger, string schedulerTask, List<Order> orders)
        {
            var query =
                "SELECT werk_nr, fase, teken_nr, buigst_nr, vrchtbrfnr, lev_date, omschr, element, invoiceid, invcompl, factdatum " +
                "FROM bgst " +
                $"WHERE werk_nr IN ({string.Join(",", orders.Select(x => x.ORDERNUMBER))});";
                
            var result = ExecuteQuery(logger, query, schedulerTask);

            if (result == null)
            {
                return new List<Buigstaat>();
            }

            return result.Select(x => new Buigstaat()
            {
                BfwWerkNr = Convert.ToInt32(x["werk_nr"]),
                BfwFase = (string)x["fase"],
                DrawingNumber = (string)x["teken_nr"],
                Buigstaatnummer = Convert.ToInt32(x["buigst_nr"]),
                BfwShipmentId = Convert.ToInt32(x["vrchtbrfnr"]),
                DeliveryDate = Convert.ToDateTime(x["lev_date"]),
                Element = (string)x["element"],
                Description = (string)x["omschr"],
                BfwInvoiceId = Convert.ToInt32(x["invoiceid"]),
                BfwInvoiceComplete = Convert.ToInt32(x["invcompl"]) == 1,
                BfwInvoiceDate = Convert.ToDateTime(x["factdatum"]),
                UniqueKey = $"{x["werk_nr"]}_{x["buigst_nr"]}",
            }).ToList();
        }

        internal static List<Buigstaatregel> GetBuigstaatregels(RidderLogger logger, string schedulerTask, List<Order> orders, List<Buigstaat> bfwBuigstaten)
        {
            logger.Log(schedulerTask, "Spanberg", SchedulerLogLevel.Info, $"Start ophalen BFW buigstaatregels.");

            var query =
                "SELECT werk_nr, buigst_nr, rgl_nr, code_mat, aantal, diameter, knipmin, knipmax, lengte, breedte, gewicht, gew_totaal, tekst " +
                "FROM bgstrgl " +
                $"WHERE werk_nr IN ({string.Join(",", orders.Select(x => x.ORDERNUMBER))});";

            var result = ExecuteQuery(logger, query, schedulerTask);

            if (result == null)
            {
                return new List<Buigstaatregel>();
            }

            var buigstaatRegels = result.Select(x => new Buigstaatregel()
            {
                Buigstaat = bfwBuigstaten.First(y => y.UniqueKey.Equals($"{x["werk_nr"]}_{x["buigst_nr"]}")),
                LineNumber = Convert.ToInt32(x["rgl_nr"]),
                BfwCodeMat = (string)x["code_mat"],
                Quantity = Convert.ToInt32(x["aantal"]),
                BfwKnipMin = Convert.ToInt32(x["knipmin"]),
                BfwKnipMax = Convert.ToInt32(x["knipmax"]),
                Diameter = Convert.ToDouble(x["diameter"]),
                Length = Convert.ToInt32(x["lengte"]),
                Text = (string)x["tekst"],
                TotalWeight = Convert.ToDouble(x["gew_totaal"]),
                Weight = Convert.ToDouble(x["gewicht"]),
                Width = Convert.ToInt32(x["breedte"]),
                UniqueKey = $"{x["werk_nr"]}_{x["buigst_nr"]}_{x["rgl_nr"]}",
            }).ToList();

            var queryGetItems =     "SELECT code_mat, ext_id " +
                                    "FROM vrd_stam " +
                                    $"WHERE code_mat in ('{string.Join("','", buigstaatRegels.Select(x => x.BfwCodeMat).Distinct())}')";
            var resultGetItems = ExecuteQuery(logger, queryGetItems, schedulerTask);

            if (resultGetItems == null)
            {
                return new List<Buigstaatregel>();
            }

            var items = resultGetItems.Select(x => new BfwModels.BfwItem()
            {
                CodeMaterial = (string)x["code_mat"],
                ExternalId = (string)x["ext_id"],
            }).ToList();

            buigstaatRegels.ForEach(x => x.BfwItem = items.FirstOrDefault(y => y.CodeMaterial.Equals(x.BfwCodeMat)));

            return buigstaatRegels;
        }

        public static EnumerableRowCollection<DataRow> ExecuteQuery(RidderLogger logger, string query, string schedulerTask)
        {
            var connectionStringBfwDatabase = ConfigurationManager.AppSettings["ConnectionStringBfwDatabase"].ToString();


            var dataSet = new DataSet();

            try
            {
                using (var conn = new NpgsqlConnection(connectionStringBfwDatabase))
                {
                    conn.Open();

                    var cmd = new NpgsqlCommand(query, conn);
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
                    da.Fill(dataSet);
                }
            }
            catch (Exception e)
            {
                logger.Log(schedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Verbinden met BFW is mislukt, oorzaak: {e}");
                return null;
            }

            if (dataSet.Tables.Count == 0)
            {
                logger.Log(schedulerTask, "Spanberg", SchedulerLogLevel.Error, $"Geen info gevonden na uitvoeren query op BFW database");
                return null;
            }

            return dataSet.Tables[0].AsEnumerable();
        }


    }
}
