﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.AfasModels;
using Anton.Scheduler.Plugins.Models;

namespace Anton.Scheduler.Plugins.FrameworkModels
{
    public static class AfasHelper
    {
        public static TimeSpan defaultStartTimeWorkday = new TimeSpan(8, 30, 0);

        public static AfasEmployer GetAfasEmployerId(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasCompanyName, string afasToken)
        {
            var skip = 0;
            var take = 100;

            //We voeren deze url maar 1 keer uit want de kans dat er meer dan 100 bedrijven zijn is nihil.

            var url = new Uri(
                $"{afasStartUrl}/ProfitRestServices/connectors/Profit_Employers?skip={skip}&take={take}&orderbyfieldids=EmployerId");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", afasToken);

            var response = client.GetAsync(url).GetAwaiter().GetResult();

            if (!response.IsSuccessStatusCode)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Verbinden AFAS API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}");

                return null;
            }

            RootObjectGet<AfasEmployer> rootObjectGetEmployers;

            try
            {
                rootObjectGetEmployers = response.Content.ReadAsAsync<RootObjectGet<AfasEmployer>>().Result;
            }
            catch (Exception e)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Converteren API response naar RootObjectGetEmployers mislukt, oorzaak: {e}");

                return null;
            }

            if (rootObjectGetEmployers == null)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Converteren API response naar RootObjectGetEmployers mislukt, oorzaak: rootobject is null");

                return null;
            }

            var afasEmployer = rootObjectGetEmployers.Rows.FirstOrDefault(x => x.Name.Equals(afasCompanyName));

            if (afasEmployer == null)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Geen werkmaatschappij in AFAS gevonden met de naam '{afasCompanyName}'");

                return null;
            }

            return afasEmployer;
        }
        
        public static List<AfasEmployee> GetEmployeesFromAfas(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasOrganisationUnitCodes, string afasToken)
        {
            var result = new List<AfasEmployee>();
            var numberOfEmployeesPerCall = 100;
            var counter = 0;
            var allEmployeesCollected = false;

            while (!allEmployeesCollected)
            {
                var skip = counter * numberOfEmployeesPerCall;

                var filter = BuildUpFilterAfasOrganisationUnitCodes(afasOrganisationUnitCodes);

                if(string.IsNullOrEmpty(filter))
                {
                    return new List<AfasEmployee>();
                }

                var url = new Uri(
                    $"{afasStartUrl}/ProfitRestServices/connectors/Ridder_Profit_Employees?{filter}&Skip={skip}&Take={numberOfEmployeesPerCall}&orderbyfieldids=EmployeeId");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", afasToken);

                var response = client.GetAsync(url).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Verbinden AFAS API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}");

                    return new List<AfasEmployee>();
                }

                RootObjectGet<AfasEmployee> rootObjectGetEmployees;

                try
                {
                    rootObjectGetEmployees = response.Content.ReadAsAsync<RootObjectGet<AfasEmployee>>().Result;
                }
                catch (Exception e)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Converteren API response naar RootObject mislukt, oorzaak: {e}");

                    return new List<AfasEmployee>();
                }

                if (rootObjectGetEmployees == null)
                {
                    return result;
                }

                result.AddRange(rootObjectGetEmployees.Rows);

                if (rootObjectGetEmployees.Rows.Count < numberOfEmployeesPerCall)
                {
                    allEmployeesCollected = true;
                }

                counter++;
            }

            //Filter medewerkers die inmiddels uit dienst zijn eruit. Hier filteren is makkelijker dan in de url meegeven
            result = result.Where(x => !x.EmploymentEnd.HasValue || (x.EmploymentEnd.HasValue && x.EmploymentEnd.Value.Date > DateTime.Now.Date)).ToList();

            return result;
        }

        internal static List<AfasTimeTable> GetTimeTablesFromAfas(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasOrganisationUnitCodes, string afasToken)
        {
            var result = new List<AfasTimeTable>();
            var numberOfTimeTablesPerCall = 100;
            var counter = 0;
            var allRecordsCollected = false;

            while (!allRecordsCollected)
            {
                var skip = counter * numberOfTimeTablesPerCall;

                var filter = BuildUpFilterAfasOrganisationUnitCodes(afasOrganisationUnitCodes);

                if (string.IsNullOrEmpty(filter))
                {
                    return new List<AfasTimeTable>();
                }

                var url = new Uri(
                    $"{afasStartUrl}/ProfitRestServices/connectors/Medewerker_roosters_Bisharp?{filter}&Skip={skip}&Take={numberOfTimeTablesPerCall}");

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", afasToken);

                var response = client.GetAsync(url).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Verbinden AFAS API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}");

                    return new List<AfasTimeTable>();
                }

                RootObjectGet<AfasTimeTable> rootObjectGetTimeTables;

                try
                {
                    rootObjectGetTimeTables = response.Content.ReadAsAsync<RootObjectGet<AfasTimeTable>>().Result;
                }
                catch (Exception e)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                        $"Converteren API response naar RootObject mislukt, oorzaak: {e}");

                    return new List<AfasTimeTable>();
                }

                if (rootObjectGetTimeTables == null)
                {
                    return result;
                }

                result.AddRange(rootObjectGetTimeTables.Rows);

                if (rootObjectGetTimeTables.Rows.Count < numberOfTimeTablesPerCall)
                {
                    allRecordsCollected = true;
                }

                counter++;
            }

            return result;
        }

        internal static List<AfasLeave> GetLeavesFromAfas(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask , string company, string afasOrganisationUnitCodes, 
            string afasToken, List<Workactivity> workactivities, DateTime startInterval, DateTime endInterval, List<AfasEmployee> employeesAfas)
        {
            var result = new List<AfasLeave>();
            var numberOfRecordsPerCall = 100;
            

            var organisationUnitCodes = afasOrganisationUnitCodes.Split(';');

            foreach (var workactivity in workactivities)
            {
                foreach (var organisationUnitCode in organisationUnitCodes)
                {
                    var allLeavesCollected = false;
                    var counter = 0;

                    while (!allLeavesCollected)
                    {
                        var skip = counter * numberOfRecordsPerCall;

                        var filter = $"filterfieldids=OrganisationUnitCode%2CLeaveCode%2CStartDate%2CEndDate&filtervalues={organisationUnitCode.Trim()}%2C{workactivity.AFASABSENCECODE}%2C{startInterval:yyyy-MM-dd}%2000%3A00%2C{endInterval.AddDays(1):yyyy-MM-dd}%2000%3A00&operatortypes=1%2C1%2C2%2C5";

                        var url = new Uri(
                            $"{afasStartUrl}/ProfitRestServices/connectors/Ridder_Profit_Leaves?{filter}&Skip={skip}&Take={numberOfRecordsPerCall}&orderbyfieldids=LeaveId");

                        HttpClient client = new HttpClient();
                        client.DefaultRequestHeaders.Add("Authorization", afasToken);

                        var response = client.GetAsync(url).GetAwaiter().GetResult();

                        if (!response.IsSuccessStatusCode)
                        {
                            ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                                $"Verbinden AFAS API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}");

                            return new List<AfasLeave>();
                        }

                        RootObjectGet<AfasLeave> rootObjectGetLeaves;

                        try
                        {
                            rootObjectGetLeaves = response.Content.ReadAsAsync<RootObjectGet<AfasLeave>>().Result;
                        }
                        catch (Exception e)
                        {
                            ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                                $"Converteren API response naar RootObject mislukt, oorzaak: {e}");

                            return new List<AfasLeave>();
                        }

                        if (rootObjectGetLeaves == null)
                        {
                            return result;
                        }

                        result.AddRange(rootObjectGetLeaves.Rows);

                        if (rootObjectGetLeaves.Rows.Count < numberOfRecordsPerCall)
                        {
                            allLeavesCollected = true;
                        }

                        counter++;
                    }
                }
            }

            //Filter samengestelde boekingen die AFAS zelf aanmaakt (bv Wettelijk 2022 enz) eruit
            result = result.Where(x => x.Samengestelde_verlofboeking == null).ToList();

            //Haal verlofboekingen van mensen uit dienst eruit. 
            result = result.Where(x => employeesAfas.Select(y => y.EmployeeId).Contains(x.EmployeeId)).ToList();

            return result;
        }


        internal static List<AfasLeaveCorrection> GetLeaveCorrectionsFromAfas(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasOrganisationUnitCodes,
                    string afasToken, DateTime startInterval, DateTime endInterval, string afasAbsenceCode, List<AfasEmployee> employeesAfas)
        {
            //We willen geen uren ophalen van voor 1-1-2022. Dit is de start van live gang met AFAS
            if (startInterval < new DateTime(2022, 1, 1))
            {
                startInterval = new DateTime(2022, 1, 1);
            }


            var result = new List<AfasLeaveCorrection>();
            var numberOfRecordsPerCall = 100;

            var organisationUnitCodes = afasOrganisationUnitCodes.Split(';');

            foreach (var organisationUnitCode in organisationUnitCodes)
            {
                var allLeavesCollected = false;
                var counter = 0;

                while (!allLeavesCollected)
                {
                    var skip = counter * numberOfRecordsPerCall;

                    var filter = $"filterfieldids=OrganisationUnitCode%2CCodeTypeVerlof%2CDatum_boeking%2CDatum_boeking&filtervalues={organisationUnitCode.Trim()}%2C{afasAbsenceCode}%2C{startInterval:yyyy-MM-dd}%2000%3A00%2C{endInterval.AddDays(1):yyyy-MM-dd}%2000%3A00&operatortypes=1%2C1%2C2%2C5";

                    var url = new Uri(
                        $"{afasStartUrl}/ProfitRestServices/connectors/Ridder_Verlofcorrecties?{filter}&Skip={skip}&Take={numberOfRecordsPerCall}");

                    HttpClient client = new HttpClient();
                    client.DefaultRequestHeaders.Add("Authorization", afasToken);

                    var response = client.GetAsync(url).GetAwaiter().GetResult();

                    if (!response.IsSuccessStatusCode)
                    {
                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Verbinden AFAS API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}");

                        return new List<AfasLeaveCorrection>();
                    }

                    RootObjectGet<AfasLeaveCorrection> rootObjectGetLeaveCorrections;

                    try
                    {
                        rootObjectGetLeaveCorrections = response.Content.ReadAsAsync<RootObjectGet<AfasLeaveCorrection>>().Result;
                    }
                    catch (Exception e)
                    {
                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Converteren API response naar RootObject mislukt, oorzaak: {e}");

                        return new List<AfasLeaveCorrection>();
                    }

                    if (rootObjectGetLeaveCorrections == null)
                    {
                        return result;
                    }

                    result.AddRange(rootObjectGetLeaveCorrections.Rows);

                    if (rootObjectGetLeaveCorrections.Rows.Count < numberOfRecordsPerCall)
                    {
                        allLeavesCollected = true;
                    }

                    counter++;
                }
            }

            //Filter handmatig aangemaakte AFAS verlofcorrecties eruit
            result = result.Where(x => x.Omschrijving == null).ToList();

            //Haal verlofboekingen van mensen uit dienst eruit. 
            result = result.Where(x => employeesAfas.Select(y => y.EmployeeId).Contains(x.Medewerker)).ToList();

            return result;
        }

        internal static void CreateLeaves(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasToken, List<ProjectTime> ridderLeavesToCreate,
            List<AfasEmployee> afasEmployees)
        {
            if (!ridderLeavesToCreate.Any())
            {
                return;
            }

            var url = new Uri($"{afasStartUrl}/ProfitRestServices/connectors/HrAbsence");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", afasToken);

            foreach (var ridderLeaveToCreate in ridderLeavesToCreate)
            {
                var afasEmployee = afasEmployees.FirstOrDefault(x => x.EmployeeId == ridderLeaveToCreate.EmployeeCode);

                if (afasEmployee == null)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Aanmaken verlofboeking mislukt, geen werknemer gevonden met code '{ridderLeaveToCreate.EmployeeCode}'");
                    continue;
                }

                var afasHrAbsenceBooking = new AfasHrAbsence()
                {
                    EmployeeId = ridderLeaveToCreate.EmployeeCode,
                    TypeAbsence = ridderLeaveToCreate.AfasAbsenceCode,
                    Start = ridderLeaveToCreate.DATE,
                    End = ridderLeaveToCreate.DATE,
                    EmploymentType = afasEmployee.EmploymentTypeId,
                    TimeInMinutes = ridderLeaveToCreate.TimeEmployeeInMinutes,
                    SpecificTimes = true,
                };


                if (afasEmployee.TypeTimeTable.StartsWith("Uren per dag"))
                {
                    afasHrAbsenceBooking.LeaveHoursStartTime = ridderLeaveToCreate.TimeEmployeeInMinutes;
                }

                if(afasEmployee.TypeTimeTable.Equals("Werktijden, wisselende weken"))
                {
                    var end = defaultStartTimeWorkday.Add(TimeSpan.FromMinutes(afasHrAbsenceBooking.TimeInMinutes));

                    afasHrAbsenceBooking.Start = ridderLeaveToCreate.DATE.Add(defaultStartTimeWorkday);
                    afasHrAbsenceBooking.End = ridderLeaveToCreate.DATE.Add(end);
                }
                

                var rootObjectPostHrAbsence = new RootObjectPostHrAbsence()
                {
                    HrAbsence = new RootObjectPostHrAbsence.Hrabsence()
                    {
                        Element = new RootObjectPostHrAbsence.Element() { AfasHrAbsence = afasHrAbsenceBooking }
                    }
                };

                var response = client.PostAsJsonAsync(url, rootObjectPostHrAbsence).Result;

                if (!response.IsSuccessStatusCode)
                {
                    try
                    {
                        var errorResult = response.Content.ReadAsAsync<ErrorResponse>().Result;

                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            errorResult == null
                                ? $"Aanmaken verlofboeking mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}"
                                : $"Aanmaken verlofboeking {afasHrAbsenceBooking.EmployeeId} - {afasHrAbsenceBooking.Start:dd-MM-yyyy} - mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}: {errorResult.ExternalMessage}");
                    }

                    catch (Exception)
                    {
                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Aanmaken verlofboeking mislukt, deserializen object mislukt bij urenboeking {afasHrAbsenceBooking.EmployeeId} - {afasHrAbsenceBooking.Start:dd-MM-yyyy}");
                    }

                    continue;
                }

                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Verlofboeking {afasHrAbsenceBooking.EmployeeId} - {afasHrAbsenceBooking.Start:dd-MM-yyyy} - {afasHrAbsenceBooking.TypeAbsence} toegevoegd.");
            }
        }

        internal static void CreateLeaveCorrections(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasToken, List<ProjectTime> ridderTvtOvertimesToCreate, 
            string afasAbsenceCode, List<AfasEmployee> afasEmployees, bool isDiscontinuity)
        {
            if (!ridderTvtOvertimesToCreate.Any())
            {
                return;
            }

            var url = new Uri($"{afasStartUrl}/ProfitRestServices/connectors/HrAbsCorrection");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", afasToken);

            foreach (var ridderTvtOvertimeToCreate in ridderTvtOvertimesToCreate)
            {
                var afasEmployee = afasEmployees.FirstOrDefault(x => x.EmployeeId == ridderTvtOvertimeToCreate.EmployeeCode);

                if (afasEmployee == null)
                {
                    ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Aanmaken verlofcorrectie mislukt, geen werknemer gevonden met code '{ridderTvtOvertimeToCreate.EmployeeCode}'");
                    continue;
                }

                var hrAbsCorrection = new HrAbsCorrection()
                {
                    EmployeeId = ridderTvtOvertimeToCreate.EmployeeCode,
                    EmploymentType = afasEmployee.EmploymentTypeId,
                    Date = ridderTvtOvertimeToCreate.DATE,
                    TypeAbsence = afasAbsenceCode,
                    TimeInMinutes = isDiscontinuity ? ridderTvtOvertimeToCreate.TimeBuildUpDiscontinuityInMinutes : ridderTvtOvertimeToCreate.TimeTvtInMinutes
                };

                var rootobjectHrAbsenceCorrection = new RootobjectHrAbsenceCorrection()
                {
                    HrAbsCorrection = new Hrabscorrection()
                    {
                        Element = new Element()
                        {
                            Fields = hrAbsCorrection,
                        }
                    }
                };


                var response = client.PostAsJsonAsync(url, rootobjectHrAbsenceCorrection).Result;

                if (!response.IsSuccessStatusCode)
                {
                    try
                    {
                        var errorResult = response.Content.ReadAsAsync<ErrorResponse>().Result;

                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            errorResult == null
                                ? $"Aanmaken verlofcorrectie mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}"
                                : $"Aanmaken verlofcorrectie {hrAbsCorrection.EmployeeId} - {hrAbsCorrection.Date:dd-MM-yyyy} - mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}: {errorResult.ExternalMessage}");
                    }

                    catch (Exception)
                    {
                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Aanmaken verlofcorrectie mislukt, deserializen object mislukt bij urenboeking {hrAbsCorrection.EmployeeId} - {hrAbsCorrection.Date:dd-MM-yyyy}");
                    }

                    continue;
                }

                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"Verlofcorrectie {hrAbsCorrection.EmployeeId} - {hrAbsCorrection.Date:dd-MM-yyyy} - {hrAbsCorrection.TypeAbsence} toegevoegd.");
            }
        }


        internal static void DeleteLeaves(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasToken, List<AfasLeave> afasLeavesToDelete)
        {
            if(!afasLeavesToDelete.Any())
            {
                return;
            }

            var url = new Uri(
                    $"{afasStartUrl}/ProfitRestServices/connectors/HrAbsence");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", afasToken);

            foreach (var afasLeaveToDelete in afasLeavesToDelete)
            {
                var startDateTime = $"{afasLeaveToDelete.StartDate.Year}%2D{afasLeaveToDelete.StartDate.Month:D2}%2D{afasLeaveToDelete.StartDate.Day:D2}T{afasLeaveToDelete.StartDate.Hour:D2}.{afasLeaveToDelete.StartDate.Minute:D2}.{afasLeaveToDelete.StartDate.Second:D2}";
                var response = client.DeleteAsync($"{url}/HrAbsence/EmId,ViAt,EnSe,DaBe/{afasLeaveToDelete.EmployeeId},{afasLeaveToDelete.LeaveCode},{afasLeaveToDelete.EmploymentType},{startDateTime}").Result;

                if (!response.IsSuccessStatusCode)
                {
                    try
                    {
                        var errorResult = response.Content.ReadAsAsync<ErrorResponse>().Result;
                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Verwijderen AFAS verlofboeking {afasLeaveToDelete.StartDate} - {afasLeaveToDelete.EmployeeId} mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}: {errorResult.ExternalMessage}");
                    }
                    catch (Exception)
                    {
                        ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                            $"Verwijderen verlofboeking mislukt, deserializen object mislukt bij urenboeking {afasLeaveToDelete.StartDate} - {afasLeaveToDelete.EmployeeId}");
                    }

                    continue;
                }

                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Info, $"AFAS verlofboeking {afasLeaveToDelete.EmployeeId} - {afasLeaveToDelete.StartDate:dd-MM-yyyy} - {afasLeaveToDelete.LeaveCode} verwijderd.");
            }
        }

        internal static byte[] GetImage(RidderLogger ridderLogger, string afasStartUrl, string schedulerTask, string company, string afasToken, int imageId)
        {
            var url = new Uri(
                $"{afasStartUrl}/ProfitRestServices/ImageConnector/{imageId}");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", afasToken);

            var response = client.GetAsync(url).GetAwaiter().GetResult();

            if (!response.IsSuccessStatusCode)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Verbinden AFAS API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}");

                return null;
            }

            RootObjectGetImage rootObjectGetImage;

            try
            {
                rootObjectGetImage = response.Content.ReadAsAsync<RootObjectGetImage>().Result;
            }
            catch (Exception e)
            {
                ridderLogger.Log(schedulerTask, company, SchedulerLogLevel.Error,
                    $"Converteren API response naar RootObject mislukt, oorzaak: {e}");

                return null;
            }

            if (rootObjectGetImage == null)
            {
                return null;
            }

            return Convert.FromBase64String(rootObjectGetImage.FileData);
        }

        private static string BuildUpFilterAfasOrganisationUnitCodes(string afasOrganisationUnitCodes)
        {
            var codes = afasOrganisationUnitCodes.Split(';');

            if(!codes.Any())
            {
                return string.Empty;
            }

            var result = "filterfieldids=";

            for (int i = 0; i < codes.Count(); i++)
            {
                if (i == 0)
                {
                    result += "OrganisationUnitCode";
                }
                else
                {
                    result += $"%3BOrganisationUnitCode";
                }
            }

            result += "&filtervalues=";

            for (int i = 0; i < codes.Count(); i++)
            {
                if (i == 0)
                {
                    result += codes[i].Trim();
                }
                else
                {
                    result += $"%3B{codes[i].Trim()}";
                }
            }

            result += "&operatortypes=";

            for (int i = 0; i < codes.Count(); i++)
            {
                if (i == 0)
                {
                    result += "1";
                }
                else
                {
                    result += $"%3B1";
                }
            }

            return result;
        }

        
    }
}
