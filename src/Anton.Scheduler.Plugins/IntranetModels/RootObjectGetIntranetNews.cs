﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Anton.Scheduler.Plugins.IntranetModels
{
    public class RootObjectGetIntranetNews
    {
        public List<Post> Posts { get; set; }

        public class Post
        {
            public int Id { get; set; }
            public DateTime Date { get; set; }
            public DateTime Modified { get; set; }
            public string Link { get; set; }
            public Title Title { get; set; }
            public Excerpt Excerpt { get; set; }
            public List<int> Categories { get; set; }
            public List<string> Company { get; set; }
            [JsonProperty("_embedded")]
            public EmbedInfo EmbedInfo { get; set; }
        }

        public class Title
        {
            public string Rendered { get; set; }
        }

        public class Excerpt
        {
            public string Rendered { get; set; }
            public bool Protected { get; set; }
        }

        public class EmbedInfo
        {
            [JsonProperty("wp:featuredmedia")]
            public FeaturedMedia[] FeaturedMedia { get; set; }
        }

        public class FeaturedMedia
        {
            public string source_url { get; set; }
        }

        public class Yoast_Head_Json
        {
            public Og_Image[] og_image { get; set; }
        }

        public class Og_Image
        {
            public int width { get; set; }
            public int height { get; set; }
            public string url { get; set; }
            public string type { get; set; }
        }

    }
}
