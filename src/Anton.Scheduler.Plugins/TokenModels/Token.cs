﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Anton.Scheduler.Plugins.Models;
using Quartz;
using Ridder.Client.SDK.Extensions;
using Ridder.Scheduler.Logging;

namespace Anton.Scheduler.Plugins.TokenModels
{
    class Token
    {
        private const string _connectionString = @"Data Source=SRV-APP03\RIDDERIQ;Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";
        //private const string _connectionString = @"Data Source=LT115\RIDDERIQ;Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";

        public SdkSession _sdkSession { get; set; }

        public string TokenResult { get; set; }

        public Token(SdkSession sdkSession)
        {
            _sdkSession = sdkSession;
        }

        public TokenResponse GetOrCreateNewToken()
        {
            var creditSafeTokenInfo = DetermineCreditSafeTokenInfoFromAntonGroup();

            //Token is 1 uur geldig dus deze is nog bruikbaar
            if (creditSafeTokenInfo.TokenGenerated > DateTime.Now.AddMinutes(-58)) //We houden 2 minuten aan als doorlooptijd
            {
                return new TokenResponse() {Token = creditSafeTokenInfo.Token, Result = true, Message = ""};
            }

            if (string.IsNullOrEmpty(creditSafeTokenInfo.CreditSafeCredentials.UserName) ||
                string.IsNullOrEmpty(creditSafeTokenInfo.CreditSafeCredentials.Password))
            {
                return new TokenResponse()
                {
                    Token = "",
                    Result = false,
                    Message =
                        $"CreditSafe inloggegevens niet correct ingevuld in de CRM instellingen.",
                };
            }

            //Genereer nieuwe token door verbinding te maken met CreditSafe
            var urlPath = new Uri(@"https://connect.creditsafe.com/v1/authenticate");

            HttpClient client = new HttpClient();
            HttpResponseMessage result = client.PostAsJsonAsync(urlPath, creditSafeTokenInfo.CreditSafeCredentials)
                .GetAwaiter().GetResult();

            if (result.StatusCode != HttpStatusCode.OK)
            {
                return new TokenResponse()
                {
                    Token = "",
                    Result = false,
                    Message =
                        $"Verbinding maken met CreditSafe is mislukt; oorzaak: {(int)result.StatusCode}:{result.ReasonPhrase}",
                };
            }

            var newToken = result.Content.ReadAsAsync<TokenResponse>().GetAwaiter().GetResult();
            SaveNewTokenInAntonGroup(newToken.Token);

            return new TokenResponse()
            {
                Token = newToken.Token,
                Result = true,
                Message =
                    $"",
            };
        }

        private void SaveNewTokenInAntonGroup(string newToken)
        {
            var rsCRM = _sdkSession.GetRecordsetColumns("R_CRMSETTINGS", 
                    "CREDITSAFEUSERNAME, CREDITSAFEPASSWORD, CREDITSAFETOKEN, CREDITSAFETOKENGENERATED", "");
                
            rsCRM.MoveFirst();
            rsCRM.Fields["CREDITSAFETOKEN"].Value = newToken;
            rsCRM.Fields["CREDITSAFETOKENGENERATED"].Value = DateTime.Now;
            rsCRM.Update();
        }

        private CreditSafeTokenInfo DetermineCreditSafeTokenInfoFromAntonGroup()
        {
            CreditSafeTokenInfo result = new CreditSafeTokenInfo();

            var crmInfo = _sdkSession.Find<CrmSetting>().First();

            result.CreditSafeCredentials = new CreditSafeCredentials()
            {
                UserName = crmInfo.CREDITSAFEUSERNAME,
                Password = crmInfo.CREDITSAFEPASSWORD,
            };
            
            result.Token = crmInfo.CREDITSAFETOKEN;
            result.TokenGenerated = !crmInfo.CREDITSAFETOKENGENERATED.HasValue
                ? DateTime.MinValue
                : crmInfo.CREDITSAFETOKENGENERATED.Value;

            return result;
        }
    }
}
