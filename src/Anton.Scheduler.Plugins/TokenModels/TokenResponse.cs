﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Anton.Scheduler.Plugins.TokenModels
{
    class TokenResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        public bool Result { get; set; }
        public string Message { get; set; }
    }
}
