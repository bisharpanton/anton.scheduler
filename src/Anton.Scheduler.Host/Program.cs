﻿using System;
using Ridder.Scheduler.Service;

namespace Anton.Scheduler.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            new SchedulerBootstrapper("Ridder.Scheduler.Service", "Ridder iQ Scheduler Service",
                "A general scheduler which handles scheduler plugins to automate processes.").Run();
        }
    }
}
