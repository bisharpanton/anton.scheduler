@echo off
set name=Ridder.Scheduler.Service
set description=Ridder iQ Scheduler Service
set iqserverservice=Ridder.iQ.Service


"%~dp0Anton.Scheduler.Host.exe" install --delayed --localsystem -servicename "%name%" -displayname "%description%" -description "%description%" %*

IF ERRORLEVEL 1 (
    set exit_code=%ERRORLEVEL%
    echo Error calling Anton.Exporter.Host.exe, ERRORLEVEL = %exit_code%
    exit /b %exit_code%
)

sc config "%name%" depend="%iqserverservice%"
