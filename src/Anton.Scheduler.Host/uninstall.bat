@echo off
set name=Ridder.Scheduler.Service

"%~dp0Anton.Scheduler.Host.exe" uninstall -servicename "%name%" %*

IF ERRORLEVEL 1 (
    set exit_code=%ERRORLEVEL%
    echo Error calling Anton.Exporter.Host.exe, ERRORLEVEL = %exit_code%
    exit /b %exit_code%
)
